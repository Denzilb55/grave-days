﻿
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif


#if UNITY_EDITOR
[InitializeOnLoad]
#endif
public class AutosaveOnRun: ScriptableObject
{
	#if UNITY_EDITOR
	static AutosaveOnRun()
	{
		EditorApplication.playmodeStateChanged = () =>
		{
			if(EditorApplication.isPlayingOrWillChangePlaymode && !EditorApplication.isPlaying)
			{
				Debug.Log("Auto-Saving scene before entering Play mode: " + EditorApplication.currentScene);
				
				EditorApplication.SaveScene();
				EditorApplication.SaveAssets();
			}
		};
	}
	#endif
}
