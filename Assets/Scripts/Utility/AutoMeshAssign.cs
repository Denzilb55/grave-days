﻿using UnityEngine;
using System.Collections;

public class AutoMeshAssign : MonoBehaviour {

	// Use this for initialization
	void Start () {
		GetComponent<MeshCollider> ().sharedMesh = GetComponent<MeshFilter> ().sharedMesh;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
