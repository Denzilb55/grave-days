using System;
using UnityEngine;
using GraveDays.Entity.Being;

namespace GraveDays.Items
{
	public class ItemFood: BaseItem
	{

		public int HungerValue {
			get;
			private set;
		}

		public ItemFood(ItemType itemType): base(itemType)
		{
			HungerValue = 16;
		}

		public override float GetWeight()
		{
			return 2;
		}

		public override void Use (HeroBeing user)
		{
			user.Eat (this);
		}

		public override string GetItemText()
		{
			return "Food";
		}

		public override string GetItemTooltip()
		{
			return "Make the hunger go away";
		}
	}
}

