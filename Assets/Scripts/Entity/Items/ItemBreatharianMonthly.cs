using System;
using UnityEngine;
using GraveDays.Entity.Being;

namespace GraveDays.Items
{
	public class ItemBreatharianMonthly: BaseItem
	{

		public ItemBreatharianMonthly(ItemType itemType): base(itemType, ItemRarity.Unique)
		{
			
		}


		public override void Use (HeroBeing user)
		{
			user.movementModule.AddMovementModifier (1.8f);
			user.inventory.RemoveItem (this);
			user.hungerMultiplier -= 0.15f;
			SoundPlayer.instance.PlayForced (Sounds.pageFlip);
		}
		
		public override string GetItemText()
		{
			return "Breatharian Monthly Mag";
		}

		public override string GetItemTooltip()
		{
			return "Aims to teach you to survive without food!";
		}
	}
}


