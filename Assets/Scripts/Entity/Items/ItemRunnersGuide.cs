using System;
using UnityEngine;
using GraveDays.Entity.Being;

namespace GraveDays.Items
{
	public class ItemRunnersGuide: BaseItem
	{

		public ItemRunnersGuide(ItemType itemType): base(itemType, ItemRarity.Unique)
		{
			
		}


		public override void Use (HeroBeing user)
		{
			user.movementModule.AddMovementModifier (1.8f);
			user.inventory.RemoveItem (this);
			SoundPlayer.instance.PlayForced (Sounds.pageFlip);
		}
		
		public override string GetItemText()
		{
			return "Runner's Guide Magazine";
		}


		public override string GetItemTooltip()
		{
			return "How to run guide";
		}
	}
}


