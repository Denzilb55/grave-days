using System;
using System.Collections.Generic;
using GraveDays.Items;
using GraveDays.Items.Guns;
using GraveDays.Entity.Being;

public enum ItemType {Ammo = 0, Food, GunGlock, GunAK, GunShotgun, Bandages, 
						SteelManMagazine, RunnersGuide, HealthyLifeBook, RunningShoes, SneakerShoes,
						BreatharianMonthly, Adrenaline, AntiViral }

public enum EquipType {Weapon = 0, Shoes}

public enum ItemRarity {Common = 0, Rare, Unique}

public static class ItemTypeMethods
{
	public static BaseItem GetItemInstance(this ItemType e)
	{
		return BaseItem.items [(int)e];
	}
	
}

public abstract class BaseItem: IItem
{
	public const int ITEM_COUNT = 14; //based on enum
	public const int EQUIP_COUNT = 3;
	public static BaseItem [] items = new BaseItem[ITEM_COUNT];


	public static List<BaseItem> rareItems = new List<BaseItem> ();
	private ItemRarity rarity;

	//public readonly int itemID;

	
	public static readonly ItemAmmo Ammo = new ItemAmmo(ItemType.Ammo);
	public static readonly ItemFood Food = new ItemFood(ItemType.Food);
	public static readonly ItemBandages Bandages = new ItemBandages(ItemType.Bandages);
	public static readonly ItemSteelManMagazine SteelManMagazine = new ItemSteelManMagazine(ItemType.SteelManMagazine);
	public static readonly ItemRunnersGuide RunnersGuide = new ItemRunnersGuide(ItemType.RunnersGuide);
	public static readonly ItemHealthyLifeMagazine HealthyLifeBook = new ItemHealthyLifeMagazine (ItemType.HealthyLifeBook);
	public static readonly ItemShoes RunningShoes = new ItemShoes (ItemType.RunningShoes, "Running Shoes", 2.5f, 6f, 13f, "Helps you run faster");
	public static readonly ItemShoes SneakerShoes = new ItemShoes (ItemType.SneakerShoes, "Sneaker Shoes", 0.8f, 1.5f, 5f, "Helps you sneak");
	public static readonly ItemBreatharianMonthly BreatharianMonthly = new ItemBreatharianMonthly (ItemType.BreatharianMonthly);
	public static readonly ItemAdrenaline Adrenaline = new ItemAdrenaline ();
	public static readonly ItemAntiViralShot ViralShot = new ItemAntiViralShot ();

	protected ItemType itemType;


	protected BaseItem(ItemType itemType, ItemRarity rarity = ItemRarity.Common)
	{
		this.itemType = itemType;
		items [(int)itemType] = this;
		this.rarity = rarity;
		if (rarity == ItemRarity.Rare || rarity == ItemRarity.Unique) {
			BaseItem.rareItems.Add (this);
		}
	}

	public bool IsRare()
	{
		return rarity == ItemRarity.Rare || rarity == ItemRarity.Unique;
	}

	public bool IsUnique()
	{
		return rarity == ItemRarity.Unique;
	}

	#region IItem implementation

	public virtual int Take (HeroBeing taker, int amount = 1)
	{
		taker.inventory.AddItem (itemType, amount);
		taker.PlayZipSound();
		if (IsRare()) taker.RareItemsFound++;
		
		return amount;
	}

	public abstract string GetItemText ();
	public ItemType GetItemType ()
	{
		return itemType;
	}


	public virtual int GetStackSize()
	{
		return 5;
	}

	public virtual bool IsEquippable()
	{
		return false;
	}


	public virtual void Remove(HeroBeing remover)
	{

	}

	public virtual float GetWeight()
	{
		return 1;
	}

	public abstract void Use (HeroBeing user);
	public abstract string GetItemTooltip();

	#endregion
	
}


