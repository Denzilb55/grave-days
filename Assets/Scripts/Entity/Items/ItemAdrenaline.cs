using System;
using GraveDays.Entity.Being;

namespace GraveDays.Items
{
	public class ItemAdrenaline: BaseItem
	{
		public ItemAdrenaline (): base(ItemType.Adrenaline)
		{
		}

		#region implemented abstract members of BaseItem

		public override string GetItemText ()
		{
			return "Adrenaline";
		}

		public override void Use (GraveDays.Entity.Being.HeroBeing user)
		{
			user.effectsComponent.ApplyAdrenalinEffect ();
			user.inventory.RemoveItem (GetItemType ());
		}

		public override string GetItemTooltip ()
		{
			return "Suppresses flu, cramps and deadly zombie fevers";
		}

		#endregion
	}
}

