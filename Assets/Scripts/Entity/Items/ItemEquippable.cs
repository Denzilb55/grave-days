using System;
using UnityEngine;
using GraveDays.Entity.Being;

namespace GraveDays.Items
{
	public abstract class ItemEquippable: BaseItem
	{
		public ItemEquippable (ItemType itemType, ItemRarity rarity = ItemRarity.Rare) : base (itemType, rarity)
		{

		}


		public abstract void EquipAction (IEquippor equipor);
		public abstract void UnequipAction (IEquippor equipor);

		
		public override bool IsEquippable()
		{
			return true;
		}

		public override void Use (HeroBeing user)
		{
			if (user.equipComponent.IsEquipped(this)) {
				//Unequip (user.equipComponent);
				user.equipComponent.Unequip(this);
			} else {
				//Equip (user.equipComponent);
				user.equipComponent.Equip(this);
			}
		}
		
		public abstract EquipType GetEquipType ();
	}
}

