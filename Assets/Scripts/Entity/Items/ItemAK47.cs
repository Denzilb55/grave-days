using System;
using UnityEngine;
using GraveDays.Animation;

namespace GraveDays.Items.Guns
{
	public class ItemAK47: ItemGun
	{
		public ItemAK47 ( float shootPeriod, string gunText, float spread, float shotDamage, float penetration, float weight):base(ItemType.GunAK, shootPeriod, gunText, spread, shotDamage, penetration, weight)
		{
			
		}
				
		public override AnimationDecorator GetAnimationDecorator()
		{
			return AnimationDecorator.Rifle;
		}
	}
}

