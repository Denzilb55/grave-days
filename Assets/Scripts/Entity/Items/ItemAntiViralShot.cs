using System;

namespace GraveDays.Items
{
	public class ItemAntiViralShot: BaseItem
	{
		public ItemAntiViralShot (): base(ItemType.AntiViral)
		{
		}
		

		
		public override string GetItemText ()
		{
			return "Anti-viral Shot";
		}
		
		public override void Use (GraveDays.Entity.Being.HeroBeing user)
		{
			user.effectsComponent.AddInfected(-20);
			user.inventory.RemoveItem (GetItemType ());
		}
		
		public override string GetItemTooltip ()
		{
			return "Slows the corpsifiction process of being infected";
		}
	
	}
}

