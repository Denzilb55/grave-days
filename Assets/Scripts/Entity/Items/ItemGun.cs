using System;
using System.Collections.Generic;
using UnityEngine;
using GraveDays.Animation;
using GraveDays.Entity.Being;

namespace GraveDays.Items.Guns
{
	public class ItemGun: ItemEquippable
	{
		public float ShootPeriod {
			get;
			private set;
		}

		public float Spread {
			get;
			private set;
		}

		public float ShotDamage {
			get;
			private set;
		}

		public float Penetration {
			get;
			private set;
		}

		public int ID {
			get; 
			private set;
		}

		private float weight;

		public override void Remove(HeroBeing remover)
		{

		}

		public override bool IsEquippable()
		{
			return true;
		}

		public override float GetWeight()
		{
			return weight;
		}

		public override EquipType GetEquipType()
		{
			return EquipType.Weapon;
		}

		public virtual AnimationDecorator GetAnimationDecorator()
		{
			return AnimationDecorator.Glock;
		}

		private string GunText;

		protected ItemGun (ItemType itemType, float shootPeriod, string gunText, float spread, float shotDamage, float penetration, float weight): base(itemType, ItemRarity.Rare)
		{
			this.itemType = itemType;
			ShootPeriod = shootPeriod;
			GunText = gunText;
			Spread = spread;
			ShotDamage = shotDamage;
			Penetration = penetration;
			this.weight = weight;
			ID = assignedID++;
			Guns.Add (this);
		}

		public override int Take (HeroBeing taker, int amount = 1)
		{
			//taker.equipComponent.Equip (this);
			if (IsRare()) taker.RareItemsFound++;
			taker.inventory.AddItem (itemType, amount);
			SoundPlayer.instance.PlayForced (Sounds.gunCocking);
			return amount;
		}

		public override void EquipAction(IEquippor equipper)
		{
			equipper.EquipAction (this);
		}
		
		
		public override void UnequipAction (IEquippor equipper)
		{
			equipper.UnequipAction (this);
		}

		public override string GetItemTooltip()
		{
			return "Zombie repellant!";
		}

		public override string GetItemText ()
		{
			return GunText;
		}

		public virtual void Shoot (GunComponent gunComponent, Vector2 direction, IGunner gunner, InventoryComponent inventory)
		{


			Vector2 fireDirection = ((Vector2)(direction)).Rotate(UnityEngine.Random.Range(-Spread,Spread));
			Vector2 fireForce = fireDirection*1.5f;

		
			if (ManagerScript.networkType == NetworkType.Single)
			{
				BulletBehaviour shotBehaviour = gunComponent.InstantiateBullet(fireForce);
				shotBehaviour.direction = direction;
				gunComponent.AssignBullet(shotBehaviour);
			}
			else {
				Debug.Log("GUNNER " + gunner);
				NetworkScript.instance.FireNetworkBullet(fireForce, gunComponent.transform.position, gunComponent.transform.rotation, gunner);
			}
			
			inventory.RemoveItem(ItemType.Ammo);

			if (gunner != null && ManagerScript.networkType != NetworkType.Single)
			{
				//gunner.OnProjectileFired(fireForce, shotBehaviour.networkView.viewID);
			}
		}



		private static int assignedID;
		public static List<ItemGun> Guns = new List<ItemGun> ();
		public static readonly ItemGun Glock = new ItemGun(ItemType.GunGlock, 0.45f, "Glock 9mm",1f, 36f, 0, 5);
		public static readonly ItemGun AK47 = new ItemAK47 (0.15f, "AK-47",5f, 60f,0.35f, 20);
		public static readonly ItemGun Shotgun = new ItemShotgun (ItemType.GunShotgun,0.8f, "Shotgun",5f, 26f,0.5f, 20);

	}
}



