using System;
using UnityEngine;
using GraveDays.Entity.Being;

namespace GraveDays.Items
{
	public class ItemSteelManMagazine: BaseItem
	{
		public ItemSteelManMagazine(ItemType itemType): base(itemType, ItemRarity.Unique)
		{
			
		}

		public override void Use (HeroBeing user)
		{
			user.healthComponent.MaxLimit = 300;
			user.healthComponent.IncreaseMaxHealth (100);
			user.inventory.RemoveItem (this);
			SoundPlayer.instance.PlayForced (Sounds.pageFlip);
		}
		
		public override string GetItemText()
		{
			return "Steel Man Mag";
		}

		public override string GetItemTooltip()
		{
			return "Makes you stronger";
		}
	}
}


