using System;
using UnityEngine;
using GraveDays.Entity.Being;

namespace GraveDays.Items
{
	public class ItemHealthyLifeMagazine : BaseItem
	{
		public ItemHealthyLifeMagazine(ItemType itemType): base(itemType, ItemRarity.Unique)
		{
			
		}
			
		
		public override void Use (HeroBeing user)
		{
			user.healthComponent.HealthRecovery = 0.1f;
			user.inventory.RemoveItem (this);
			SoundPlayer.instance.PlayForced (Sounds.pageFlip);
		}
		
		public override string GetItemText()
		{
			return "Healthy Life Mag";
		}

		public override string GetItemTooltip()
		{
			return "Some tips for a healthy lifestyle";
		}
	}
}

