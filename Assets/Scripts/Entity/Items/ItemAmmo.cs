using System;
using UnityEngine;
using GraveDays.Entity.Being;

namespace GraveDays.Items
{
	public class ItemAmmo: BaseItem
	{

		public ItemAmmo(ItemType itemType): base(itemType)
		{

		}


		public override int Take (HeroBeing taker, int amount = 1)
		{

				SoundPlayer.instance.PlayForced (Sounds.gunCocking, 0.8f);

				return base.Take(taker, amount);
		}

		public override void Use (HeroBeing user)
		{

		}

		public override float GetWeight()
		{
			return 0.5f;
		}

		public override string GetItemText()
		{
			return "Ammo";
		}

		public override string GetItemTooltip()
		{
			return "Some juice for your gun";
		}
	}
}





