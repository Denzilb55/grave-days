//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.1
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using UnityEngine;

namespace GraveDays.Effects
{
	public class EffectBleeding: BaseEffect
	{
		public float intensity;

		private float bleedChance;

		public EffectBleeding (float intensity)
		{
			this.intensity = intensity;
		}

		public override bool Process(EffectsComponent component)
		{
			component.ApplyDamage (intensity * ManagerScript.instance.CachedTime[4]);
			bleedChance -= ManagerScript.instance.CachedTime[4];
			intensity -= ManagerScript.instance.CachedTime[4] * 0.12f;

			if (intensity <= 0.01f) {
				return true;
			}

			if (UnityEngine.Random.Range (0f, bleedChance) <= intensity *0.65f) {
				DoBleed(component);
			}
			return false;
		}

		void DoBleed(EffectsComponent component)
		{
			bleedChance = 50;
			GameObjectPool.instance.ServeBloodSplatter (component.transform.position);
		}



		public override bool ApplyEffect(EffectsComponent component)
		{
			//component.SetEffect (EffectsType.Bleeding);
			return true;
		}

		public override void RemoveEffect(EffectsComponent component)
		{
		//	DoBleed (component);

		}

		public override void CompoundEffect(float intensity) 
		{
			this.intensity += intensity;
		}

		public override void CompoundEffect(BaseEffect effect) 
		{
			EffectBleeding ef = effect as EffectBleeding;
			intensity += ef.intensity;
		}

		public override EffectsType GetEffectsType()
		{
			return EffectsType.Bleeding;
		}


	}
}


