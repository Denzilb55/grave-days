using System;
using UnityEngine;

namespace GraveDays.Effects
{
	public class EffectInfected: BaseEffect
	{
		private float intensity;
		private float countOut;

		public EffectInfected (float intensity)
		{
			this.intensity = intensity;
			MakeImmune ();
		}

		public void MakeImmune()
		{
			countOut = 60;
		}

		public override EffectsType GetEffectsType()
		{
			return EffectsType.Infected;
		}

		public override bool Process(EffectsComponent component)
		{
			if (countOut <= 0) {

				float random = component.being.GetRandom (intensity + 3000);
			//	Debug.Log ("R"+random);
				if (random > 3000) {
					component.AddEffect (new EffectFever(Mathf.Sqrt(intensity) * 10, component.GetComponent<FatigueComponent>()));
					countOut = 50;
				//	Debug.Log ("ADD FEVER " + intensity);
				}
			} else {
				countOut -= ManagerScript.instance.CachedTime[4];
			//	Debug.Log("COUNT " + countOut);
			}

			//if (component.GetComponent<GraveDays.Entity.Being.HeroBeing>() != null)
			//Debug.Log ("INFECTED " + intensity);
			return false;
		}

		public override void CompoundEffect(float intensity) 
		{
			this.intensity += intensity;

			if (intensity < 5) {
				intensity = 5;
			}
		}


	}
}

