using System;
using UnityEngine;
using GraveDays.Entity.Being;

namespace GraveDays.Effects
{
	public class EffectFever: BaseEffect
	{
		private float intensity;
		private float bleedCompound;
		private FatigueComponent fatigue;

		public EffectFever (float intensity, FatigueComponent fatigue)
		{
			this.intensity = intensity > 250? 250 : intensity;
			this.fatigue = fatigue;
		}

		public override EffectsType GetEffectsType()
		{
			return EffectsType.Fever;
		}

		public override bool Process(EffectsComponent component)
		{
	
			component.ApplyDamage (intensity * 0.002f);
			bleedCompound += intensity * 0.00022f;
		

			float random = 800 - bleedCompound * 500;
			random = random < 0 ? 0 : random;
			float randomVal = component.being.GetRandom (random);
		//	Debug.Log ("BLEED RAND" + randomVal);
			if (randomVal < 1) {
				component.AddEffect (new EffectBleeding(bleedCompound));
			//	Debug.Log("ADD BLEED: " + bleedCompound);
				bleedCompound = 0;
			}

			intensity -= ManagerScript.instance.CachedTime [4]*2;

			if (randomVal < 5) {
				StandardBeing being = component.GetComponent<StandardBeing> ();
				being.Stun (0.25f);
				being.rigidbody2D.AddForce(being.GetRandomInCircleCentered(2.3f));
				being.transform.Rotate (Vector3.forward, being.GetRandom(20f) - 10f);

				if (fatigue != null){
					float infect = (intensity - fatigue.Fatigue) / 16;
					infect = infect < 0? 0:infect;
					component.AddInfected(infect);
				}
			}

			if (fatigue != null) {
				fatigue.feverModifier = intensity/60f;
			}

			//if (component.GetComponent<GraveDays.Entity.Being.HeroBeing>() != null)
			//Debug.Log ("FEVER " + intensity);
			if (intensity <= 0) {
				return true;
			}
			return false;
		}

		public override void CompoundEffect(float intensity) 
		{
			this.intensity += intensity;

			if (this.intensity > 250) {
				this.intensity = 250;
			}
		}

		public override void CompoundEffect(BaseEffect effect) 
		{
			intensity += ((EffectFever)effect).intensity;

			if (this.intensity > 250) {
				this.intensity = 250;
			}
		}

		public override bool ApplyEffect(EffectsComponent component)
		{
			if (fatigue != null) {
				fatigue.Fatigue -= intensity/10f;
			}
			return true;
		}
		
		public override void RemoveEffect(EffectsComponent component)
		{

			if (fatigue != null) {
				fatigue.feverModifier = 0;
			}
		}
	}
}

