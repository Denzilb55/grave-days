using System;

namespace GraveDays.Effects
{
	public abstract class BaseEffect
	{
		public float Intensity { get; private set; }

		public BaseEffect ()
		{
		}

		public virtual bool Process(EffectsComponent component)
		{
			return false;
		}

		public virtual bool ApplyEffect(EffectsComponent component)
		{
			return true;
		}

		public virtual void CompoundEffect(BaseEffect effect) 
		{
			throw new NotImplementedException ("NOT IMPLEMENTED - HIDE METHOD");
		}

		public virtual void CompoundEffect(float intensity) 
		{
			throw new NotImplementedException ("NOT IMPLEMENTED - HIDE METHOD");
		}

	
		public virtual void RemoveEffect(EffectsComponent component)
		{

		}


		public abstract EffectsType GetEffectsType();
	}
}


