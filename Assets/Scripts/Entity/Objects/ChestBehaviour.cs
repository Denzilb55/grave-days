using System;
using UnityEngine;
using System.Collections.Generic;
using GraveDays.Animation;
using GraveDays.Entity.Being;


public class ChestBehaviour: InteractInventoryComponent
{
	public static List<ChestBehaviour> chests = new List<ChestBehaviour>();
	private AnimatorChest animator;

	public ChestBehaviour()
	{

	}

	void Start()
	{
		animator = GetComponent<AnimatorChest> ();
	}

	public override void NotifyClose () 
	{
		animator.CloseChestAnimate ();
	}

	protected override void Initialise()
	{
		base.Initialise ();
		chests.Add (this);
		inventory.maxCarryWeight = 250;
	}



	public override bool Interact(HeroBeing hero)
	{
		if (base.Interact (hero)) {
			SoundPlayer.instance.PlayForced(Sounds.chestOpen);
			animator.OpenChestAnimate();
			return true;
		} else {
			return false;
		}
	}

	protected override string GetInventoryTitle ()
	{
		return "Chest";
	}
	protected override string GetPutTitle ()
	{
		return "Store";
	}
	protected override string GetTakeTitle ()
	{
		return "Take";
	}
	
	public override void TakeItem(HeroBeing interactor, IItem item, int amount = 1)
	{
		/*int itemCount = inventory.GetItemCount (item);
		if (amount > itemCount) {
			amount = itemCount;
		}
		int taken = item.Take (interactor, amount);
		inventory.RemoveItem (item, taken);*/

		InventoryComponent.TransferItem (item, inventory, interactor.inventory, ref amount);
		/*if (ManagerScript.networkType != NetworkType.Single) {
			networkView.RPC ("RPCRemoveItem", RPCMode.Others, (int)item.GetItemType (), amount);
		}*/
	}

	[RPC]
	void RPCAddItem(int itemIndex, int amount)
	{
		inventory.AddItem ((ItemType)itemIndex, amount);
	}
	
	[RPC]
	void RPCRemoveItem(int itemIndex, int amount)
	{
		inventory.RemoveItem ((ItemType)itemIndex, amount);
	}
	
	public override void DepositItem(HeroBeing interactor, IItem item, int amount = 1)
	{
		if (interactor == null) {
			inventory.AddItem(item, amount);
			if (ManagerScript.networkType != NetworkType.Single)
			{
				networkView.RPC ("RPCAddItem", RPCMode.Others, (int)item.GetItemType(), amount);
			}
			return;
		}

		int remains = InventoryComponent.TransferItem (item, interactor.inventory, inventory, ref amount);

		if (remains == 0 && item.IsEquippable()) {
			//interactor.gunComponent.UnequipGun();
			GraveDays.Items.ItemEquippable equipItem = (item as GraveDays.Items.ItemEquippable);
			
			if (interactor.equipComponent.IsEquipped(equipItem))
			{
				interactor.equipComponent.Unequip(equipItem);
			}
		}

		/*if (ManagerScript.networkType != NetworkType.Single) {
			networkView.RPC ("RPCDepositItem", RPCMode.Others, (int)item.GetItemType (), amount);
		}*/

		SoundPlayer.instance.PlayForced (Sounds.chestStore, 0.8f);
	}

}


