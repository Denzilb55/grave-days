﻿using UnityEngine;
using System.Collections;
using System;
using GraveDays.Entity.Being;

public enum PickupType {health = 0, ammo, food}


[Serializable]
public class SaveDataPickup
{
	public PickupType type;
	public int holderID;
	public int value;

	public SerializableVector2 position;
}

public class PickupItem : InteractComponent {

	public PickupType type;
	private int value;
	public IItem item;
	ItemHolder itemHolder;
	 
	public int maxValue;
	public int minValue;

	void Awake()
	{
		Initialise ();
		int maxValHalf = (int)(maxValue*0.5f) + 1;
		int minValHalf = (int)(minValue*0.5f);
		value = UnityEngine.Random.Range(minValHalf, maxValHalf) + UnityEngine.Random.Range(minValHalf, (int)(maxValHalf*1.1f));
		if (value > maxValue) {
			value = maxValue;
		}

		if (value < minValue) {
			value = minValue;
		}

		ManagerScript.instance.pickups.Add(gameObject);
	}



	void Initialise()
	{

		if (type == PickupType.ammo) {
			item = BaseItem.Ammo;
		} else if (type == PickupType.health) {
			item = BaseItem.Bandages;
		} else if (type == PickupType.food) {
			item = BaseItem.Food;
		}
	}

	public SaveDataPickup GetSaveObject()
	{
		SaveDataPickup pickupStats = new SaveDataPickup ();
		if (itemHolder != null) {
			pickupStats.holderID = itemHolder.ID;
			if (itemHolder.ID == 47)
			Debug.Log( " save: " + itemHolder.ID);
		} else {
			pickupStats.holderID = -1;
		}
		pickupStats.value = value;
		pickupStats.type = type;
		pickupStats.position = transform.position;
		return pickupStats;
	}
	
	public void LoadFromSaveObject(SaveDataPickup data)
	{
		value = data.value;
		type = data.type;
		Initialise ();

		if (data.holderID != -1) {
		//	Debug.Log(data.holderID + " : " + ItemHolder.itemHolders.Count);
			itemHolder = ItemHolder.itemHolders [data.holderID];
			itemHolder.ItemAdded(this);
		//	Debug.Log("LOAD:" + data.holderID);
		} 


	}

	public override bool Interact(HeroBeing hero)
	{

		if (base.Interact (hero)) {
			TakeItem(hero);
			return true;
		}

		return false;
	}
	
	public void SetItemHolder(ItemHolder itemHolder)
	{
		this.itemHolder = itemHolder;
	//	Debug.Log ("SET HOLDER " + itemHolder + " " + itemHolder.ID);
	}
	
	void OnTriggerEnter2D(Collider2D other) {
		if (other.CompareTag("Player")) {
			HeroBeing hero = other.GetComponent<HeroBeing>();

			if (hero != null)
			{
				TakeItem(hero);	
			}
		}
	}

	[RPC]
	void RPCRemoveItem()
	{
		if (itemHolder != null) {
			itemHolder.ItemRemoved (this);
		}
		ManagerScript.instance.pickups.Remove (gameObject);
		Network.RemoveRPCs( networkView.viewID);
		Destroy (gameObject);
	}

	public void TakeItem(HeroBeing hero)
	{
		if (item.Take (hero, value) > 0) {
			if (itemHolder != null) {
				itemHolder.ItemRemoved (this);
			}
			ManagerScript.instance.pickups.Remove (gameObject);
			Network.RemoveRPCs( networkView.viewID);

			if (ManagerScript.networkType != NetworkType.Single)
			{
				networkView.RPC("RPCRemoveItem", RPCMode.Others);
			}
			Destroy (gameObject);
		}

	}
}
