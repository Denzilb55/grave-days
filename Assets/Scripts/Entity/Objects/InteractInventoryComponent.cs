using System;
using UnityEngine;
using System.Collections.Generic;

using GraveDays.Entity.Being;

[Serializable]
public class SaveDataContainer
{
	
	public ItemType[] items;
	public int [] num0;
	
	public SaveDataContainer(int count)
	{
		items = new ItemType[count];
		num0 = new int[count];
	}
}

public abstract class InteractInventoryComponent: InteractComponent, IOptioned
{
	public InteractInventoryComponent ()
	{
	}

	protected bool MouseOnGui = false;
	
	public Vector2 scrollPosition1 = Vector2.zero;
	public Vector2 scrollPosition2 = Vector2.zero;
	
	
	public InventoryComponent inventory;
	
	
	/*public void Add(IItem item)
	{
		inventory.AddItem (item);
	}*/
	
	void Awake()
	{
		inventory = GetComponent<InventoryComponent> ();
		if (inventory == null) {
			inventory = gameObject.AddComponent<InventoryComponent>();
		}
		Initialise ();
		//name = "Chest" + chests.Count;
		
	}

	protected virtual void Initialise()
	{

	}

	

	
	public virtual bool IsMouseOver()
	{
		return MouseOnGui;
	}
	
	public virtual void HandleInputs()
	{
		
	}

	protected abstract string GetInventoryTitle ();
	protected abstract string GetPutTitle ();
	protected abstract string GetTakeTitle ();

	public virtual void DrawMenu(){

		int xWidth = 500;
		int yWidth = 300;
		int xStart = (int)(Screen.width / 2f - xWidth/2f);
		int yStart = (int)(Screen.height / 2f - yWidth/2f);
		
		Rect guiRect = new Rect (xStart, yStart, xWidth, yWidth);
		
		//Debug.Log (guiRect.Contains (Event.current.mousePosition) + " " + Event.current.mousePosition);
		if (guiRect.Contains (Event.current.mousePosition)) {
			MouseOnGui = true;
		} else {
			MouseOnGui = false;
		}
		
		GUI.Box (guiRect, GUIContent.none); 
		
		GUIStyle titleStyle = new GUIStyle (GUI.skin.label);
		titleStyle.alignment = TextAnchor.UpperCenter;
		titleStyle.fontSize = 20;
		titleStyle.fontStyle = FontStyle.Bold;
		
		
		GUIStyle subTitleStyle = new GUIStyle (GUI.skin.label);
		subTitleStyle.alignment = TextAnchor.UpperCenter;
		subTitleStyle.fontSize = 16;
		subTitleStyle.fontStyle = FontStyle.Bold;
		
		GUIStyle hintStyle = new GUIStyle (GUI.skin.label);
		hintStyle.alignment = TextAnchor.UpperCenter;
		hintStyle.fontSize = 12;
		hintStyle.fontStyle = FontStyle.Italic;
		
		//TAKE
		
		
		
		//STORE
		int heroFood = GlobalData.instance.player.FoodCount;
		int heroMeds = GlobalData.instance.player.BandagesCount;
		int heroAmmo = GlobalData.instance.player.gunComponent.Ammo;
		
		
		GUILayout.BeginArea (guiRect);
		GUILayout.Label (GetInventoryTitle(), titleStyle);
		DrawSubStuff (subTitleStyle);
		GUILayout.Label ("Hold shift to move in stacks", hintStyle);
		GUILayout.BeginHorizontal ();
		GUILayout.BeginVertical (GUILayout.Width (250));
		GUILayout.Label (GetPutTitle (), subTitleStyle);
		scrollPosition1 = GUILayout.BeginScrollView(scrollPosition1,false,true);
		
		foreach(KeyValuePair<ItemType, int> entry in GlobalData.instance.player.inventory.items)
		{
			BaseItem item = entry.Key.GetItemInstance();
			
			if (GUILayout.Button (new GUIContent(entry.Value+" x "+item.GetItemText(), "Deposit: "+item.GetItemTooltip()))) 
			{
				if (Input.GetKey(KeyCode.LeftShift))
				{
					DepositItem(GlobalData.instance.player, item, item.GetStackSize());
				}
				else {
					DepositItem(GlobalData.instance.player, item);
				}
			}
		}
		
		
		GUILayout.EndScrollView();
		//	GUILayout.Label (GUI.tooltip);
		GUILayout.EndVertical ();
		GUILayout.BeginVertical (GUILayout.Width (250));
		GUILayout.Label (GetTakeTitle (), subTitleStyle);
		scrollPosition2 = GUILayout.BeginScrollView(scrollPosition2,false, true);
		foreach(KeyValuePair<ItemType, int> entry in inventory.items)
		{
			BaseItem item = entry.Key.GetItemInstance();
			
			if (GUILayout.Button (new GUIContent(entry.Value+" x "+item.GetItemText(), "Take: " +item.GetItemTooltip()))) 
			{
				if (Input.GetKey(KeyCode.LeftShift))
				{
					TakeItem(GlobalData.instance.player, item, item.GetStackSize());
				}
				else {
					TakeItem(GlobalData.instance.player, item);
				}
			}
		}
		
		
		GUILayout.EndScrollView();
		
		GUILayout.EndVertical ();
		
		GUILayout.EndHorizontal ();
		GUILayout.Label (GUI.tooltip);
		
		GUILayout.EndArea ();
		//inventory.CleanUp ();
		//	GlobalData.instance.player.inventory.CleanUp ();
	}

	public virtual void DrawSubStuff(GUIStyle subTitleStyle)
	{
		
		
	}
	
	public virtual void TakeItem(HeroBeing interactor, IItem item, int amount = 1)
	{

		
	}
	
	public virtual void DepositItem(HeroBeing interactor, IItem item, int amount = 1)
	{

	}

	public abstract void NotifyClose ();

}


