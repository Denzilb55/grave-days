using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GraveDays.Effects;
using System;
using GraveDays.Quests;
using GraveDays.Animation;
using GraveDays.Entity.Being.AI;
using GraveDays.Entity;

//public enum BehaviourState {idleStand = 0, idleWalk, idleSearch, meleeEngage, avoid, stationaryEngage, follow, holdPosition }


[Serializable]
public class SaveDataZombie
{
	public SaveDataMovementStats movementStats; 
	public SaveDataHealthStats healthStats;
	public float fatigue;

	public SerializableVector2 pos;

	public BehaviourState state;
	public SerializableVector2 lastKnownPosition;
	public SerializableVector2 lastKnownTargetVector;
}

public class ZombieBehaviour : MonoBehaviour, IHealthReactive, IOccupant, ITargetable, ITargeter, IQuestTarget, ILocalAffector {
	public int ThinkID;

	public ITargetable collidedTargetable;
	public ITargetable target {
		get;
		private set;
	}
	private Vector2 moveGoal;
	public float fatigue;
	private bool stunned = false;
	private MovementComponent movement;
	private AnimatorZombie animatorModule;
	private HealthComponent healthComponent;
	public EffectsComponent effectsComponent;
	//private LivingScanner livingScanner;
	//public List<ITargetable> localLiving = new List<ITargetable> ();
	private List<ITargeter> targeterList = new List<ITargeter> ();
	private List<ILocalAffector> localAffected = new List<ILocalAffector> ();

	public Vector2 targetVector;
	private Vector2 lastKnownTargetVector;
	private Vector2 lastKnownTargetPosition;
	//private Vector2 lastKnownTargetDirection;
	private bool hasSraightPath = true;
	
	private AudioSource zombieSource;
	public static int gruntSaturate = 0;
	private const int gruntMax = 5;

	public BehaviourState StateAI { get; private set; }

	private const float exposureOnCall = 3.8f;

	private float searchRange = 5f;
	private float colliderRadius;

	public bool isMoving;

	public const int AVOID_LAYER = (1 << 8) | (1 << 10) | (1 << 13);
	public const int SIGHT_LAYER = (1 << 14);
	
	public bool isRecycling = false;

	public bool alreadyProcessed = false;

	public List<ITargetable> targetList = new List<ITargetable> ();

	public bool debugEnable;

	private bool HasTarget;

	private Vector2 netInterpVec;
	private float interpVal;
	private bool interpEnable;

	private static System.Random CasualRandomer = new System.Random();

	public int ID;
	public static int IDs = 0;

	// Use this for initialization
	void Start () {

	
	}




	void Awake()
	{
		effectsComponent = GetComponent<EffectsComponent> ();
		movement = GetComponent<MovementComponent> ();
		animatorModule = GetComponent<AnimatorZombie> ();
		healthComponent = GetComponent<HealthComponent> ();
		//livingScanner = GetComponentInChildren<LivingScanner> ();
		//localLiving.Add (GlobalData.instance.player);
		gruntSaturate = 0;
		colliderRadius = ((CircleCollider2D)collider2D).radius * 1.2f;
		ID = IDs++;


	}

	void OnEnable()
	{
		//despawned = false;
		stunned = false;
		isRecycling = false;
		float moveSpeed = UnityEngine.Random.Range (12.2f, 13.2f);
		movement.SetBaseSpeed (moveSpeed);
		healthComponent.Initialise (this, UnityEngine.Random.Range (30, 120));
		HasTarget = false;
		StateAI = BehaviourState.idleStand;
		interpEnable = false;
		gameObject.GetComponentInChildren<CollisionTrackerComponent> ().Refresh ();
		targeterList.Clear ();
		//networkView.stateSynchronization = NetworkStateSynchronization.ReliableDeltaCompressed;
	}



	public SaveDataZombie GetSaveObject()
	{
		HandleInvokes ();
		SaveDataZombie zombieSave = new SaveDataZombie ();
		zombieSave.movementStats = movement.GetSaveObject ();
		zombieSave.healthStats = healthComponent.GetSaveObject ();

		zombieSave.fatigue = fatigue;
		
		zombieSave.pos = transform.position;
		zombieSave.lastKnownPosition = lastKnownTargetPosition;
		zombieSave.lastKnownTargetVector = lastKnownTargetVector;
		zombieSave.state = StateAI;
		
		return zombieSave;
	}

	public float GetDotToTarget()
	{
		return 1;
	}

	[RPC] 
	void AssignZombie()
	{
		NetworkViewID viewID = Network.AllocateViewID ();
		Debug.Log ("old " + networkView.viewID + " new " + viewID); 
		networkView.RPC ("NotifyAssignZombie", RPCMode.All, viewID);
	}

	[RPC]
	void NotifyAssignZombie(NetworkViewID viewID)
	{
		Network.RemoveRPCs (networkView.viewID);
		Debug.Log ("NEW VIEW ID " + viewID + " old " + networkView.viewID);
		networkView.viewID = viewID;
		//Network.SetSendingEnabled(0, true);
	}



	private void AssignTarget(ITargetable tar)
	{
		target = tar;
		HasTarget = true;
		tar.AssignTargeter (this);

		/*if (ManagerScript.networkType != NetworkType.Single) {
			if (tar.GetTransform ().networkView.viewID.owner != networkView.viewID.owner) {
				networkView.RPC("AssignZombie",tar.GetTransform ().networkView.viewID.owner);
				Network.RemoveRPCs (networkView.viewID);
				//Network.SetSendingEnabled(0, false);
			}
		}*/
	}

	public void UpdateAnimation()
	{
		animatorModule.UpdateAnimation ();
	}

	public bool CanAttack()
	{
		return !stunned && IsValid ();
	}

	void HandleInvokes()
	{
		if (IsInvoking ("TargetPositionReached")) {
			CancelInvoke("TargetPositionReached");
			TargetPositionReached();
		}
	}
	
	public void LoadFromDataObject(SaveDataZombie data)
	{
		movement.LoadFromSaveObject (data.movementStats);
		healthComponent.LoadFromSaveObject (data.healthStats);

		fatigue = data.fatigue;
		transform.position = data.pos;

		StateAI = data.state;

		if (StateAI == BehaviourState.meleeEngage) {
			AssignTarget(GlobalData.instance.player);
		}

		lastKnownTargetPosition = data.lastKnownPosition;
		lastKnownTargetVector = data.lastKnownTargetVector;
		moveGoal = data.lastKnownPosition;
	}


	#region ITargetable implementation
	public float GetExposure ()
	{
		return 500;
		return ManagerScript.instance.LightExposure/120f + 0.2f;
	}
	public float GetSqrExposure ()
	{
		float exps = GetExposure ();
		return exps * exps;
	}

	public bool IsMoving()
	{
		return (movement.MoveType != MovementType.none); 
	}

	public Transform GetTransform ()
	{
		return transform;
	}
	public void IncreaseExposure (float value)
	{
		
	}
	public void ReduceBaseExposureSightLost (float originalValue)
	{
		
	}

	public HealthComponent GetHealthComponent()
	{
		return healthComponent;
	}
	
	public EffectsComponent GetEffectsComponent()
	{
		return effectsComponent;
	}

	public bool IsValid()
	{
		return !healthComponent.IsDead () && !isRecycling;
	}

	public Vector2 GetMovingVector ()
	{
		return targetVector.normalized;
	}

	public void UnassignTargeter (ITargeter targeter)
	{
		targeterList.Remove (targeter);
	}
	
	#endregion	
	

	public void DoUnspawn()
	{
//		Debug.Log ("DO UNSPAWN " + networkView.viewID); 
		Recycle();
		if (Network.isServer) {
			networkView.RPC("Recycle", RPCMode.Others);
		}
	}


	void OnSerializeNetworkView(BitStream stream, NetworkMessageInfo info) {
	
		if (stream.isWriting)
		{
			float x = transform.position.x;
			float y = transform.position.y;

			stream.Serialize (ref x);
			stream.Serialize (ref y);

			float xVel = targetVector.x;
			float yVel = targetVector.y;

			stream.Serialize (ref isMoving);
			stream.Serialize (ref xVel);
			stream.Serialize (ref yVel);
		}
		else 
		if (stream.isReading)
		{
			float x = 0;
			float y = 0;
			float xVel = 0;
			float yVel = 0;
			
			stream.Serialize(ref x);
			stream.Serialize(ref y);
			
			stream.Serialize(ref isMoving);
			stream.Serialize(ref xVel);
			stream.Serialize(ref yVel);
			netInterpVec = new Vector2(x, y);
			interpVal = 0;
			//transform.position = 
			targetVector = new Vector2(xVel, yVel);

			if (!interpEnable)
			{
				interpEnable = true;
				transform.position = netInterpVec;
			}
		}

	}

	public void ClientUpdate () {
		if (ManagerScript.instance.PAUSED || isRecycling || stunned) {
			return;
		}

		if (isMoving) {
			movement.Move (targetVector);
		} else {
			movement.Stop ();
		}

		float angle = Mathf.Atan2 (targetVector.y, targetVector.x) * Mathf.Rad2Deg;
		transform.eulerAngles = new Vector3 (0, 0, angle -90);
	//	animatorModule.UpdateAnimationState ();

		if (interpEnable && interpVal < 1) {
			transform.position = Vector2.Lerp (transform.position, netInterpVec, interpVal);
			interpVal += 0.25f;
		}

	}

	public void CasualUpdate() 
	{
		if (ManagerScript.instance.PAUSED || stunned || isRecycling) {
			return;
		}

		//if (Time.frameCount % 55 == ThinkID % 55)
		//{
			CasualThink();
		//}
	}

	// Update is called once per frame
	public void ManualUpdate () { 

		if (ManagerScript.instance.PAUSED || stunned || isRecycling) {
			return;
		}

		/*if (alreadyProcessed) {
			Debug.Log ("ZOMBIE MULTIPROCESSED FUCKKKK " + ID);
		}

		alreadyProcessed = true;*/

		effectsComponent.ProcessEffects ();

		if (HasTarget && !RevalidateTarget(target))
		{
			ReleaseTarget();
		}





		Think ();

		bool prev = (StateAI != BehaviourState.idleStand);

		if ((StateAI != BehaviourState.idleWalk && StateAI != BehaviourState.idleStand) && HasTarget  &&  !target.IsValid()) {

			ReleaseTarget();
		}


		if (StateAI == BehaviourState.meleeEngage) {	
			if (hasSraightPath) {
		//		Intercept (2f);
			}

			isMoving = true;
		}
		else if (StateAI == BehaviourState.idleWalk || StateAI == BehaviourState.idleSearch)
		{
			isMoving = true;
		} else {
			isMoving = false;
		}

		if (isMoving) {
			movement.Move (targetVector);
		} else {
			movement.Stop ();
		}

		float angle = Mathf.Atan2 (targetVector.y, targetVector.x) * Mathf.Rad2Deg;
		transform.eulerAngles = new Vector3 (0, 0, angle -90);
		//animatorModule.UpdateAnimationState ();
	}

	void Intercept(float value)
	{

		if (StateAI == BehaviourState.meleeEngage) {
			Vector2 interceptVector = (target.GetMovingVector() * value);
			if (Vector2.Dot(targetVector.normalized, interceptVector.normalized) > - 0.3f)
			{
				targetVector += interceptVector;
			}
		}
	}




	public void TakeDamageCallback (float amount)
	{

		if (StateAI == BehaviourState.idleStand) {
			RandomWalk(Vector2.zero);
			Grunt(GlobalData.instance.zombieGrunts [1]);
		}

	}




	public void OnDied()
	{
		_OnDied ();



		AreaComponentZombie ac = GetComponent<AreaComponentZombie> ();
		foreach (AreaBehaviour a in ac.areas)
		{
			a.ReduceInfestationLevel();
		}


		if (Network.isServer) {
			networkView.RPC("_OnDied", RPCMode.Others);
		}
	}
	

	[RPC]
	void _OnDied()
	{
	
//		Debug.Log ("DIED: " + networkView.viewID + " " + networkView.viewID.owner);
		CancelInvoke ("StunRecover");
		stunned = true;
		ReleaseTarget ();
		ManagerScript.instance.killCount++;
		StopGrunt ();
		Grunt(GlobalData.instance.zombieGrunts [2]);

		if (!IsInvoking ("Recycle")) {
			Invoke ("Recycle", 1.2f);
		}
		
		if (IsInvoking("UnsaturateGrunt"))
		{
			gruntSaturate--;
			CancelInvoke("UnsaturateGrunt");
		}

		QuestData zombieData = new KillTargetQuestData (this);
		ManagerScript.instance.questManager.AdvanceQuest (QuestType.KillTarget, zombieData);  
		ManagerScript.instance.questManager.AdvanceQuest (QuestType.KillZombies);
	}

	public void HitByMelee(float damage, float stun, Vector3 knockback)
	{
		if (Network.isServer || ManagerScript.networkType == NetworkType.Single) {
			healthComponent.TakeDamage (9.5f);
			rigidbody2D.AddForce (knockback);
			Stun (stun);
		} else {
			networkView.RPC("RPCHitByMelee",RPCMode.All, damage, stun, knockback);
		}
	}

	[RPC] 
	void RPCHitByMelee(float damage, float stun, Vector3 knockback)
	{
		if (Network.isServer) {
			healthComponent.TakeDamage (9.5f);
			rigidbody2D.AddForce (knockback);
			Stun (stun);
		} else {
			rigidbody2D.AddForce (knockback);
			Stun (stun);
		}
	}

	public void RPCHitByBullet(float damage)
	{
		networkView.RPC ("_hitByBullet", RPCMode.Server, damage);
	}

	[RPC] void _hitByBullet(float damage)
	{
		healthComponent.TakeDamage (damage);
	}



	public void OnRecycle()
	{
		AreaComponentZombie ac = GetComponent<AreaComponentZombie> ();
		foreach (AreaBehaviour a in ac.areas)
		{
		//TAG	a.zombies.Remove(this);
		}
		
		//TAG ManagerScript.instance.zombies.Remove (this);
		//TAG GameObjectPool.instance.FreeZombie (this);

		//reverse loop because Notify death modifies this targeterList through ReleaseTarget of targeter
		for (int i = targeterList.Count -1; i >= 0; i--) {
			targeterList[i].NotifyDeath(this);
		}

		foreach (var affected in localAffected) {
			affected.UnassignAffected(this);
		}
		//networkView.stateSynchronization = NetworkStateSynchronization.Off;
	}

	//public 

	[RPC]
	public void Recycle()
	{
		if (!isRecycling) {
			isRecycling = true;
		//TAG	ManagerScript.instance.zombiesToRecycle.Add (this);
		}

	}

	#region ITargetable implementation
	public void AssignTargeter (ITargeter targeter)
	{
		targeterList.Add (targeter);
	}
	#endregion

	#region ITargeter implementation

	public void NotifyDeath (ITargetable target)
	{
		if (this.target == target) {
			ReleaseTarget ();
		}
	}

	#endregion

	public void ReleaseTarget()
	{

		if (HasTarget) {
			target.ReduceBaseExposureSightLost (exposureOnCall);
			target.UnassignTargeter(this);
			HasTarget = false;
			StateAI = BehaviourState.idleStand;
			//CancelInvoke ("ReleaseTarget");
		}
	}


	int CornerCastHit(Bounds bounds, Vector2 direction, float distance, int layer)
	{
		Vector2 thisPos = ((CircleCollider2D) collider2D).center + (Vector2)transform.position;
		Vector2 vecToTarget = (direction.normalized)*colliderRadius;
		
		Vector2 [] p = new Vector2[3];
		Vector2 edge1 = vecToTarget.Rotate (90) + thisPos;
		Vector2 edge2 = vecToTarget.Rotate (-90) + thisPos;
		p [0] = (Vector2)transform.position;
		p [1] = edge1;
		p [2] = edge2;



		int c = 0;
		for (int i = 0; i < 3; i++) {
			if (Physics2D.Raycast (p[i], direction, distance, layer))
			{
				c++;
			}
		}


		return c;

	}

	bool IsUnhindered(Bounds bounds, Vector2 targetPos)
	{
		Vector2 thisPos = ((CircleCollider2D) collider2D).center + (Vector2)transform.position;
		Vector2 vecToTarget = ((targetPos - thisPos).normalized)*colliderRadius;

		Vector2 [] p = new Vector2[3];
		Vector2 edge1 = vecToTarget.Rotate (90) + thisPos;
		Vector2 edge2 = vecToTarget.Rotate (-90) + thisPos;
		p [0] = (Vector2)transform.position;
		p [1] = edge1;
		p [2] = edge2;


		int c = 0;
		for (int i = 0; i < 3; i++) {

			Vector2 pointVector = targetPos - p[i];
			
			RaycastHit2D hit = Physics2D.Raycast (p[i], pointVector, pointVector.magnitude, AVOID_LAYER);
			if (hit.transform == null)
			{
				c++;
			}


		}

		return c >= 3;

	}

	bool IsUnhindered(Bounds bounds, Vector2 targetPos, float range)
	{
		Vector2 thisPos = ((CircleCollider2D) collider2D).center + (Vector2)transform.position;
		Vector2 vecToTarget = ((targetPos - thisPos).normalized)*colliderRadius;
		
		Vector2 [] p = new Vector2[3];
		Vector2 edge1 = vecToTarget.Rotate (90) + thisPos;
		Vector2 edge2 = vecToTarget.Rotate (-90) + thisPos;
		p [0] = (Vector2)transform.position;
		p [1] = edge1;
		p [2] = edge2;
		
		int c = 0;
		for (int i = 0; i < 3; i++) {
			
			Vector2 pointVector = targetPos - p[i];
			
			RaycastHit2D hit = Physics2D.Raycast (p[i], pointVector, range, AVOID_LAYER);
			if (hit.transform == null)
			{
				c++;
			}
		}

	//	UnityEngine.Debug.Log (c);
		return c >= 3;
	}

	bool InLineOfSight(Vector2 targetVec)
	{
		RaycastHit2D hit = Physics2D.Raycast (transform.position, targetVec, targetVec.magnitude, SIGHT_LAYER);
		return (hit.transform == null);

		//TAG
		return true;
	}

	void ComputeVectorToTarget()
	{
		if (StateAI == BehaviourState.meleeEngage) {
			targetVector = target.GetTransform().position - transform.position;
		}
		else if (StateAI == BehaviourState.idleWalk) {
			targetVector = moveGoal - (Vector2)transform.position;
		}

	}

	void PathSteer()
	{
		hasSraightPath = IsUnhindered (renderer.bounds, target.GetTransform ().position);

		if (!hasSraightPath) {
			AvoidanceSteer ();

		} else {
			lastKnownTargetVector = targetVector;
			lastKnownTargetPosition = target.GetTransform().position;
			//lastKnownTargetDirection = target.GetMovingVector();
		}
	}

	void AvoidanceSteer()
	{
		int hit = CornerCastHit (renderer.bounds, lastKnownTargetVector, colliderRadius *1.8f, AVOID_LAYER);
		
		int avoidCountOut = 0;
		int angleCount = 0;
		int lastHit = 0;
		
		Vector2 rotateVector = lastKnownTargetVector;
		Vector2 lastRotateVector = rotateVector;
		int dir=0;
		while ((avoidCountOut < 12 && hit != 0)) {
			avoidCountOut++;

			if (avoidCountOut % 2 == 0) {
				angleCount++;
				dir = 1;
			} else {
				dir = -1;
			}
			
			lastRotateVector = rotateVector;
			rotateVector = lastKnownTargetVector.Rotate (dir * angleCount * 21);
			lastHit = hit;
			hit = CornerCastHit (renderer.bounds, rotateVector, colliderRadius *1.8f, AVOID_LAYER);
			
		}

		targetVector = rotateVector;
	}

	public void Think()
	{
		ComputeVectorToTarget ();

		if (StateAI == BehaviourState.meleeEngage)
		{
			PathSteer();
		} else if (StateAI == BehaviourState.idleWalk) {

				CheckMoveGoalReached ();
		

		}
	}

	void DoMoveGoalReached()
	{

		StateAI = BehaviourState.idleStand;
		HasTarget = false;
	}

	void CheckMoveGoalReached()
	{
		if (targetVector.sqrMagnitude < 1) {
			DoMoveGoalReached();
		}
	}

	void RandomWalk (Vector2 bias)
	{

		float xRandom = UnityEngine.Random.Range (-8, 8);
		float yRandom = UnityEngine.Random.Range (-8, 8);

		targetVector = Vector2.ClampMagnitude(new Vector2 (xRandom, yRandom), 8) + bias;

		moveGoal = (targetVector + (Vector2)transform.position);


		if (GlobalData.instance.IsInWorld (moveGoal)) {
			StateAI = BehaviourState.idleWalk;
		} else {
			StateAI = BehaviourState.idleStand;
		}
		HasTarget = false;

	}


	public void Stun(float period)
	{
		stunned = true;
		CancelInvoke ("StunRecover");
		if (!healthComponent.IsDead ()) {
			Invoke ("StunRecover", period);
		}
	}

	public void StunRecover()
	{
		stunned = false;
	}


	bool IdleGeneral(float thinkVal1, float thinkVal2)
	{
		if (SearchForTarget ()) {
			return true;			
		} else {
			if (thinkVal1 < 1) {
				Grunt (3);
			}

			return false;
		}

	}
	

	void CasualThink()
	{
		//float thinkVal1 = UnityEngine.Random.Range (0, 200);
		//float thinkVal2 = UnityEngine.Random.Range (0, 400);

		float thinkVal1 = CasualRandomer.Next (200);
		float thinkVal2 = CasualRandomer.Next (400);

		if (StateAI == BehaviourState.idleStand ) {
			if (!IdleGeneral (thinkVal1, thinkVal2)) {
				if (thinkVal1 < 40) {
					RandomWalk (Vector2.zero);
				}
			}	
		}
		else if (StateAI == BehaviourState.idleWalk)
		{

			if (!IsUnhindered(renderer.bounds, moveGoal, 1f))
			{
				StateAI = BehaviourState.idleStand;
				HasTarget = false;
			}

			IdleGeneral (thinkVal1, thinkVal2);
		} else if (StateAI == BehaviourState.meleeEngage) {
			if (thinkVal1 < 2) {
				AlertGrunt();
			}


			if (StateAI == BehaviourState.meleeEngage && !hasSraightPath && (lastKnownTargetPosition - (Vector2)transform.position).sqrMagnitude < 1f)
			{
	
				Invoke("TargetPositionReached",0.2f);

			}

		} else if (StateAI == BehaviourState.idleSearch) {
			IdleGeneral(thinkVal1, thinkVal2);
		
		}


	}
	
	void TargetPositionReached()
	{

		if (HasTarget) {
			Vector2 targetVec = target.GetTransform ().position - transform.position;
			if (!InLineOfSight (targetVec)) {
				ITargetable temp = target;
				ReleaseTarget ();
				QuickSetTarget (temp, false, targetVec);
			} 
		}

	}



	bool SearchForTarget()
	{
		if (localAffected.Count == 0) {
			return false;
		}
		List<ITargetable> untestedTargets = new List<ITargetable> ();
		
		foreach (ITargetable target in localAffected) {
			untestedTargets.Add(target);
		}
		
		int countOut = 0;
		
		while (countOut++ < 2) {
			int randIndex = UnityEngine.Random.Range(0, untestedTargets.Count);
			
			if (TrySetTarget (untestedTargets[randIndex])) {
				return true;
			}
			else {
				untestedTargets.RemoveAt(randIndex);
				
				if (untestedTargets.Count == 0) {
					return false;
				}
			}
		}
		
		return false;
	}

	
	/*bool SearchForTarget()
	{
		float skipThreshHold = 0.7f;
				
		//foreach (ITargetable target in targetList) {
		for (int i = 0; i < targetList.Count; i++){
			if (TrySetTarget(targetList[i]))
			{
				return true;
			}
		}

		for (int i = 0; i < localLiving.Count; i++){
			if (TrySetTarget (localLiving[i])) {

				if (!targetList.Contains(localLiving[i]))
				{
					targetList.Add(localLiving[i]);
				}
				
				if (UnityEngine.Random.Range(0f, 1f) < skipThreshHold)
				{
					return true;
				}
			}

		}
		
		while (targetList.Count > 0) {
			int randomIndex = UnityEngine.Random.Range(0,targetList.Count);
			
			if (TrySetTarget(targetList[randomIndex]))
			{
				return true;
			}
			else {
				targetList.RemoveAt(randomIndex);
			}
		}
		
		return false;
		
	}*/


	public bool TrySetTarget(ITargetable target)
	{

		if (SetTarget(target)) {
			AlertGrunt();
			return true;
		}
		
		return false;
	}

	void AlertGrunt()
	{
		target.IncreaseExposure(exposureOnCall);
		Grunt (0);
	}

	void StopGrunt()
	{
		if (zombieSource != null) {
			zombieSource.Stop();
		}
	}

	[RPC]
	void GruntSound(int index)
	{
		Grunt (GlobalData.instance.zombieGrunts[index]);
		Debug.Log ("GRUNT served " + networkView.viewID);
	}

	void Grunt(int index)
	{
		Grunt(GlobalData.instance.zombieGrunts [index]);

		if (ManagerScript.networkType != NetworkType.Single) {
			Debug.Log ("GRUNT " + networkView.viewID);
			networkView.RPC ("GruntSound", RPCMode.Others, index); 
		}
	}


	void Grunt(AudioClip grunt)
	{
		if (gruntSaturate != gruntMax) {
			if (zombieSource == null) {
				zombieSource = gameObject.AddComponent<AudioSource> ();
			}

			if (!zombieSource.isPlaying) {
				float volume = SoundPlayer.GetVolumeOverDistance(transform, 120);
				if (volume > 0.005f)
				{
					if (volume > 1)
					{
						volume = 1;
					}
					zombieSource.PlayOneShot (grunt, volume);
					gruntSaturate++;
					Invoke("UnsaturateGrunt",0.6f);
				}

			}
		}


	}

	void UnsaturateGrunt()
	{
		gruntSaturate--;
	}


	bool ValidateTarget(ITargetable target)
	{
		return (target.IsValid() && (target.GetTransform().position - transform.position).sqrMagnitude < 200 * target.GetExposure());
	}

	bool RevalidateTarget(ITargetable target)
	{
		return (target.IsValid() && (target.GetTransform().position - transform.position).sqrMagnitude < 250 * target.GetExposure());
	}

	bool TryFindTarget(ITargetable target)
	{
		return false;
	}

	void TargetRetrack(ITargetable target, float biasStrength, bool biasEnforce)
	{
		if (TryFindTarget(target))
		{
			StateAI = BehaviourState.idleSearch;
			HasTarget = false;

		}
		else 
		{
			if (StateAI != BehaviourState.idleWalk)
			{
				Vector2 tVec = target.GetTransform().position - transform.position;
				Vector2 bias;
				if (biasEnforce)
				{
					bias = tVec.normalized*biasStrength;
				}
				else {
					bias = Vector2.ClampMagnitude(tVec,biasStrength);
				}
				RandomWalk(bias);
				HasTarget = false; // this is necessary
			}
		}
	}

	public bool QuickSetTarget(ITargetable target, bool inLineOfSight, Vector2 targetVec)
	{
		if ( inLineOfSight)
		{
			AssignTarget(target);
			StateAI = BehaviourState.meleeEngage;
			lastKnownTargetVector = targetVec;
			lastKnownTargetPosition = target.GetTransform().position;
			return true;
		}
		else
		{
			TargetRetrack(target,4f,false);
			if (HasTarget) {
				Debug.Log("Retrack 1");
				
			}
			ReleaseTarget();
			if (HasTarget) {
				Debug.Log("Retrack 2");
				
			}

			return false;
		}
	}
	

	public bool SetTarget(ITargetable target)
	{
		if ((!HasTarget || target != this.target) && ValidateTarget(target)) {
			Vector2 targetVec = target.GetTransform ().position - transform.position;

			return QuickSetTarget(target,InLineOfSight(targetVec),targetVec); 

		}

		return false;
	}
	
	#region ILocalAffector implementation
	public void AssignAffected (ILocalAffector affected)
	{
		localAffected.Add (affected);
	}

	public void UnassignAffected(ILocalAffector affected)
	{
		localAffected.Remove (affected);
	}
	#endregion

	#region ILocalAffected implementation

	public void NotifyEmigration (ILocalAffector affector)
	{
		//ITargetable target = affector as ITargetable;

		//if (target != null) {
		localAffected.Remove (affector);
		//}
	}

	#endregion
}
