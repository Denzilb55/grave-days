﻿using UnityEngine;
using System.Collections;
using GraveDays.Animation;

public class BloodBehaviour : MonoBehaviour {
	private AnimatorBloodSplatter animator;
	// Use this for initialization
	void Start () {
		animator = GetComponent<AnimatorBloodSplatter> ();
	}
	
	// Update is called once per frame
	void Update () {
		animator.UpdateAnimation ();
	}
}
