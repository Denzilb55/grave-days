using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using GraveDays.Items.Guns;
using GraveDays.Items;
using GraveDays.Effects;
using GraveDays.Animation;
using GraveDays.Entity.Being;
using GraveDays.Entity;



public class AllyBehaviour : MonoBehaviour, ITargetable, IOccupant, IOptioned, IEquippor, ILocalAffector {

	public Light2D Torch;
	Color torchColor = new Color(231/255f,220/255f, 207/255f,1);
	public Light2D senseLight;
	private bool torchEnabled = false;
	public GameObject GUIMarker;
	public MovementComponent movement;
	public AnimatorDecorated animatorModule;
	public AreaComponentHero areaComponent;
	public GunComponent gunComponent;
	public MeleeComponent meleeComponent;
	public EquipComponent equipComponent;
	//public NetworkPlayer owner
	private Vector2 netInterpVec;
	private float interpVal;
	private bool interpEnable;
	public float exposure;
	public InventoryComponent inventory;

	//halo


	private AudioSource footstepsAudio;
	

	private float defaultSpeed = 14f;

	public bool melee = false;
	

	private bool holdMove;

	private float _torchRadius;
	public float TorchRadius {
		get
		{
			return _torchRadius;
		}
		set
		{
			_torchRadius = value;

			if (torchEnabled)
			{
				Torch.LightRadius = _torchRadius;
			}
		}
	
	}

	public FacingDirection Direction {
		get;
		private set;
	}
	
	public Vector2 FacingVector {
		get;
		private set;
	}

	public Vector2 MovingVector {
		get;
		private set;
	}

	bool isDead;

	//private float xSync;
	//private float ySync;
	//private float 

	void Awake()
	{
		melee = true;
		animatorModule = GetComponent<AnimatorLeader> ();
		movement = GetComponent<MovementComponent> ();
		areaComponent = GetComponent<AreaComponentHero> ();
		movement.SetBaseSpeed (defaultSpeed);
		gunComponent = GetComponent<GunComponent> ();
		equipComponent = GetComponent<EquipComponent> ();
		meleeComponent = GetComponent<MeleeComponent> ();
		footstepsAudio = gameObject.AddComponent<AudioSource> ();
		inventory = GetComponent<InventoryComponent> ();
		if (inventory == null) {
			inventory = gameObject.AddComponent<InventoryComponent>();
		}
		inventory.maxCarryWeight = 100;
		footstepsAudio.volume = 0.6f;


		torchOffFunction ();

	}



	public void AddAndWear(ItemEquippable item, int amount = 1)
	{
		//item.Equip (this);
		equipComponent.Equip (item);
	}

	public void AddToBag(ItemType item, int amount = 1)
	{

	}

	// Use this for initialization
	void Start () {
		footstepsAudio.clip = GlobalData.instance.sounds [(int)Sounds.footstep1];
		Torch.ShadowsImportant = false;
		if (!ManagerScript.doLoad) {
			AddAndWear(ItemGun.Glock);
			//AddAndWear(ItemGun.AK47);
			AddToBag (ItemType.Food, 2);
			AddToBag (ItemType.Ammo, 8);
			AddToBag (ItemType.Bandages, 3);
		}

	
	}

	public void Initialise(PlayerClass playerClass)
	{

		switch (playerClass)
		{
		case PlayerClass.AverageJoe:
			animatorModule = gameObject.AddComponent<AnimatorJoe>();
			break;
		case PlayerClass.AverageJane:
			animatorModule = gameObject.AddComponent<AnimatorJane>();
			break;
		case PlayerClass.Athlete:
			animatorModule = gameObject.AddComponent<AnimatorJane>();
			break;
		case PlayerClass.Leader:
			animatorModule = gameObject.AddComponent<AnimatorLeader>();
			if (!Network.isClient) {
				SurvivorBeing surv = ManagerScript.instance.SpawnRandomSurvivor (transform.position);
				surv.GetComponent<InteractComponentSurvivor> ().TrustRating = 100;
				surv.Follow (this);
				surv.AddAndEquip (ItemGun.Glock);
				surv.AddToBag (ItemType.Ammo, 20);
				surv.AddToBag (ItemType.Bandages, 5);
				surv.AddToBag (ItemType.Food, 8);
			}
			break;
		case PlayerClass.Soldier:
			animatorModule = gameObject.AddComponent<AnimatorSoldier>();
			break;
		}

	}
	
	public void ManualUpdate() {

	}

	public void ClientUpdate()
	{
		if (ManagerScript.instance.PAUSED || isDead) {
			return;
		}



		movement.Move (MovingVector);

		float angle = Mathf.Atan2 (FacingVector.y, FacingVector.x) * Mathf.Rad2Deg;
		transform.eulerAngles = new Vector3 (0, 0, angle -90);
		//animatorModule.UpdateAnimationState ();
		//ManagerScript.instance.networkDebugText = movement.IsSprinting.ToString();

		if (interpEnable && interpVal < 1) {
			transform.position = Vector2.Lerp (transform.position, netInterpVec, interpVal);
			interpVal += 0.25f;
		}

	}

	[RPC]
	void RPCAddItem(int itemIndex, int amount)
	{
		inventory.AddItem ((ItemType)itemIndex, amount);
	}
	
	[RPC]
	void RPCRemoveItem(int itemIndex, int amount)
	{
		inventory.RemoveItem ((ItemType)itemIndex, amount);
	}

	[RPC]
	void RPCFireProjectile(Vector3 fireForce, NetworkViewID viewID)
	{
//		GameObject bullet = gunComponent.InstantiateBullet (fireForce);
	//	bullet.networkView.viewID = viewID;
	}
	
	[RPC]
	void RPCFailFireProjectile()
	{
		gunComponent.EmptyGunSpecifics ();
	}
	
	[RPC]
	void RPCMeleeSwing()
	{
		meleeComponent.FakeAttack ();
	}
	
	[RPC]
	void RPCMeleeHit()
	{
		meleeComponent.FakeAttackHit ();
	}

	void OnDestroy()
	{
		ManagerScript.instance.allies.Remove (this);
	}

	void OnSerializeNetworkView(BitStream stream, NetworkMessageInfo info) {
		
		//Debug.Log("Netread attempt " + networkView.viewID);
		if (stream.isWriting) {
			if (Network.isServer)
			{
				float x = transform.position.x;
				float y = transform.position.y;
				float xVel = MovingVector.x;
				float yVel = MovingVector.y;
				float xFace = FacingVector.x;
				float yFace = FacingVector.y;
				//int movementState = (int)move;

				stream.Serialize(ref x);
				stream.Serialize(ref y);
				stream.Serialize(ref xVel);
				stream.Serialize(ref yVel);
				stream.Serialize(ref xFace);
				stream.Serialize(ref yFace);
				//stream.Serialize(ref movementState);
				stream.Serialize (ref exposure);

			}
		} else 
		if (stream.isReading) {
			//Debug.Log("Netread " + info.networkView + " " + info.networkView.owner + " " + info.timestamp);
			float x = 0;
			float y = 0;
			float xVel = 0;
			float yVel = 0;
			float xFace = 0;
			float yFace = 0;
		//	int movementState = 0;
			
			stream.Serialize(ref x);
			stream.Serialize(ref y);
			stream.Serialize(ref xVel);
			stream.Serialize(ref yVel);
			stream.Serialize(ref xFace);
			stream.Serialize(ref yFace);
			//stream.Serialize(ref movementState);
			stream.Serialize (ref exposure);

			netInterpVec = new Vector2(x, y);
			interpVal = 0;
			MovingVector = new Vector2(xVel, yVel);
			FacingVector = new Vector2(xFace, yFace);

			//float angle = Mathf.Atan2 (FacingVector.y, FacingVector.x) * Mathf.Rad2Deg;
			//Torch.transform.rotation = Quaternion.Euler (new Vector3 (0, 0, angle));

			if (!interpEnable)
			{
				interpEnable = true;
				transform.position = netInterpVec;
			}
		}
		
	}
	
	


	#region IEquippor implementation
	public void EquipAction (ItemShoes shoes)
	{
		movement.AddMovementModifier (shoes.MovementModifier);
	}
	public void UnequipAction (ItemShoes shoes)
	{
		movement.AddMovementModifier (-shoes.MovementModifier);
	}

	public void EquipAction (ItemGun gun)
	{
		melee = false;
	}

	public void UnequipAction (ItemGun gun)
	{
		melee = true;
	}
	#endregion	

	#region ITargetable implementation

	public float GetExposure ()
	{
		return exposure;
	}

	public bool IsMoving()
	{
		return (movement.MoveType != MovementType.none); 
	}

	#region ITargetable implementation
	public void AssignTargeter (ITargeter targeter)
	{
		throw new NotImplementedException ();
	}

	public void UnassignTargeter (ITargeter targeter)
	{
		throw new NotImplementedException ();
	}

	#endregion

	public float GetSqrExposure ()
	{
		return exposure * exposure;
	}

	public Transform GetTransform()
	{
		return transform;
	}

	public void IncreaseExposure(float value)
	{

	}

	public void ReduceBaseExposureSightLost(float originalValue)
	{

	}


	public HealthComponent GetHealthComponent()
	{
		return null;
	}

	public EffectsComponent GetEffectsComponent()
	{
		return null;
	}


	public Vector2 GetMovingVector()
	{
		//if (movement.IsMoving) {
			return FacingVector;
		/*} else
			return Vector2.zero;*/
	}

	public bool IsValid()
	{
		return !isDead;
	}
	#endregion
	

	[RPC]
	void _onDied()
	{
		if (!isDead) {
			isDead = true;
			ManagerScript.instance.living.Remove (gameObject);
			SoundPlayer.instance.Play (Sounds.playerDeath, transform, 8);

			ManagerScript.instance.allies.Remove(this);
			Destroy (gameObject);

			/*Vector3 pos = new Vector3 (transform.position.x, transform.position.y, 0.01f);

			float xOffset = 0;
			float yOffset = 0;
			for (int i = 0; i < 9; i++) {
				GameObjectPool.instance.GetBloodSplatter (pos + Vector3.ClampMagnitude (new Vector3 (xOffset, yOffset), 0.8f));
				xOffset += UnityEngine.Random.Range (-1f, 1f);
				yOffset += UnityEngine.Random.Range (-1f, 1f);
			}*/
		}
	}
		





	private void torchOnFunction()
	{
		Torch.LightColor = torchColor;
		Torch.LightRadius = TorchRadius;
		torchEnabled = true;
	}

	[RPC]
	public void TorchOn()
	{
		torchOnFunction ();

	}

	private void torchOffFunction()
	{
		Torch.LightColor = Color.black;
		Torch.LightRadius = 0.01f;
		torchEnabled = false;
	}

	[RPC]
	public void TorchOff()
	{
		torchOffFunction ();

	}

	void HandleSurfaceType ()
	{
		Ray ray = new Ray (transform.position, Vector3.forward);
		RaycastHit hit;
		if (Physics.Raycast (ray, out hit, 5f)) {
			if (hit.transform.CompareTag ("Earth")) {
				footstepsAudio.clip = GlobalData.instance.sounds [(int)Sounds.earthyFootstep];
			}
			else
				if (hit.transform.CompareTag ("Water")) {
					footstepsAudio.clip = GlobalData.instance.sounds [(int)Sounds.waterFootstep];
				}
				else
					if (hit.transform.CompareTag ("Wood")) {
						footstepsAudio.clip = GlobalData.instance.sounds [(int)Sounds.footstepWood];
					}
					else {
						footstepsAudio.clip = GlobalData.instance.sounds [(int)Sounds.footstep1];
					}
		}
		if (!footstepsAudio.isPlaying) {
			footstepsAudio.Play ();
			CancelInvoke ("StopFootstepAudio");
		}
	}
	

	[RPC]
	public void DoStartSprint()
	{
		movement.Sprint(8f);
		footstepsAudio.volume = 0.85f;
	}

	[RPC]
	public void DoStopSprint()
	{
		movement.StopSprint();
		footstepsAudio.volume = 0.6f;
	}



	void StopFootstepAudio()
	{
		footstepsAudio.Stop();
	}


	//INVENTORY GUI
	public bool MouseOnGui;
	public Vector2 scrollPosition1 = Vector2.zero;

	public void DrawMenu()
	{

	}

	public bool IsMouseOver()
	{
		return MouseOnGui;
	}

	void IOptioned.HandleInputs()
	{

	}

	public void NotifyClose()
	{

	}

	#region ILocalAffector implementation
	public void AssignAffected (ILocalAffector affected)
	{
		throw new NotImplementedException ();
	}
	public void UnassignAffected (ILocalAffector affected)
	{
		throw new NotImplementedException ();
	}
	#endregion

	#region ILocalAffected implementation

	public void NotifyEmigration (ILocalAffector affector)
	{
		throw new NotImplementedException ();
	}

	#endregion
}
