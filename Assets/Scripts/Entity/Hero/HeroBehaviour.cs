using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using GraveDays.Items.Guns;
using GraveDays.Items;
using GraveDays.Effects;
using GraveDays.Animation;
using GraveDays.Entity;


[Serializable]
public class SaveDataHero
{
	public SaveDataMovementStats movementStats; 
	public SaveDataGunStats gunStats;
	public SaveDataExposureStats exposureStats;
	public SaveDataHealthStats healthStats;
	public SaveDataInventory inventorySave;

	public float hunger;
	public float fatigue;
	//public int foodCount;
	//public int medsCount;


	public SerializableVector2 pos;

	public bool torchEnabled;
}

public enum PlayerClass {AverageJoe = 0, AverageJane, Soldier, Athlete, Leader, Farmer}

public class HeroBehaviour : MonoBehaviour, IHealthReactive, ITargetable, IOccupant, IOptioned, IEquippor, IGunner, IMelee, ILocalAffector 
{

	public Light2D Torch;
	Color torchColor = new Color(231/255f,220/255f, 207/255f,1);
	public Light2D senseLight;
	private bool torchEnabled = false;

	public MovementComponent movement;
	public AnimatorDecorated animatorModule;
	public AreaComponentHero areaComponent;
	public GunComponent gunComponent;
	public MeleeComponent meleeComponent;
	public HealthComponent healthComponent;
	public ExposureComponent exposureComponent;
	public InventoryComponent inventory;
	public FatigueComponent fatigueComponent;
	public EquipComponent equipComponent;
	public EffectsComponent effectsComponent;

	private Vector2 netInterpVec;
	private float interpVal;
	public List<ILocalAffector> localAffected = new List<ILocalAffector> ();

	public int RareItemsFound;
	public float Fatigue
	{
		get 
		{
			return fatigueComponent.Fatigue;
		}

		set 
		{
			fatigueComponent.Fatigue = value;
		}
	}

	//halo

	private Color senseColor;
	private const float HURT_PERIOD = 0.25f;
	private float hurtTimer;

	private AudioSource footstepsAudio;


//	public float fatigue = 100;
	public bool PrimaryInputs = true;
	public float Hunger;
	public float hungerMultiplier = 1f;

	public bool ForceFollow;
	private float forceFollowTime;

	private bool scrollReady;

	public int FoodCount
	{
		get {
			return inventory.GetItemCount(ItemType.Food);
		}
	}
	public int BandagesCount
	{
		get {
			return inventory.GetItemCount(ItemType.Bandages);
		}
	}
	
	private float defaultSpeed = 14f;
	private float mouseTurn;

	public bool melee = false;

	private AudioSource zipSource;

	private bool holdMove;

	private float _torchRadius;
	public float TorchRadius {
		get
		{
			return _torchRadius;
		}
		set
		{
			_torchRadius = value;

			if (torchEnabled)
			{
				Torch.LightRadius = _torchRadius;
			}
		}
	
	}

	public FacingDirection Direction {
		get;
		private set;
	}
	
	public Vector2 FacingVector {
		get;
		private set;
	}

	public Vector2 MovingVector {
		get;
		private set;
	}

	public PlayerClass playerClass;



	//private float xSync;
	//private float ySync;
	//private float 

	void Awake()
	{
		melee = true;
		scrollReady = true;
		//animatorModule = GetComponent<AnimatorComponent> ();
		movement = GetComponent<MovementComponent> ();
		areaComponent = GetComponent<AreaComponentHero> ();
		movement.SetBaseSpeed (defaultSpeed);
		gunComponent = GetComponent<GunComponent> ();
		gunComponent.gunner = this;
		inventory = GetComponent<InventoryComponent> ();
		inventory.maxCarryWeight = 100;
		equipComponent = GetComponent<EquipComponent> ();
		meleeComponent = GetComponent<MeleeComponent> ();
		effectsComponent = GetComponent<EffectsComponent> ();
		fatigueComponent = GetComponent<FatigueComponent> ();
		healthComponent = GetComponent<HealthComponent> ();
		healthComponent.Initialise (this, 100f);
		exposureComponent = GetComponent<ExposureComponent> ();
		footstepsAudio = gameObject.AddComponent<AudioSource> ();
		zipSource = gameObject.AddComponent<AudioSource> ();

		footstepsAudio.volume = 0.6f;
		Hunger = 70;
		//gunComponent.EquipGun (ItemGun.Glock);


		torchOffFunction ();

	}



	public void AddAndEquip(ItemEquippable item, int amount = 1)
	{
		equipComponent.Equip (item);
		AddToBag (item.GetItemType(), amount);
	}

	public void AddToBag(ItemType item, int amount = 1)
	{
		inventory.AddItem (item, amount);
		if (ManagerScript.networkType != NetworkType.Single) {
			networkView.RPC("RPCAddItem", RPCMode.Others, (int) item, amount);
		}
	}

	public void RemoveFromBag(ItemType item, int amount = 1)
	{
		inventory.RemoveItem (item, amount);
		if (ManagerScript.networkType != NetworkType.Single) {
			networkView.RPC("RPCRemoveItem", RPCMode.Others, (int) item, amount);
		}
	}

	[RPC]
	void RPCAddItem(int itemIndex, int amount)
	{
		inventory.AddItem ((ItemType)itemIndex, amount);
	}
	
	[RPC]
	void RPCRemoveItem(int itemIndex, int amount)
	{
		inventory.RemoveItem ((ItemType)itemIndex, amount);
	}

	// Use this for initialization
	void Start () {
		footstepsAudio.clip = GlobalData.instance.sounds [(int)Sounds.footstep1];


	}


	void SetAnimatorComponent(PlayerClass playerClass)
	{
		switch (playerClass)
		{
		case PlayerClass.AverageJoe:
			animatorModule = gameObject.AddComponent<AnimatorJoe>();
			break;
		case PlayerClass.AverageJane:
			animatorModule = gameObject.AddComponent<AnimatorJane>();
			break;
		case PlayerClass.Athlete:
			animatorModule = gameObject.AddComponent<AnimatorJane>();
			break;
		case PlayerClass.Leader:
			animatorModule = gameObject.AddComponent<AnimatorLeader>();
			break;
		case PlayerClass.Soldier:
			animatorModule = gameObject.AddComponent<AnimatorSoldier>();
			break;
		}
	}

	public void Initialise(PlayerClass playerClass)
	{
		SetAnimatorComponent (playerClass);
		this.playerClass = playerClass;

		if (!ManagerScript.doLoad) {
			//AddAndEquip(ItemGun.Glock);
			//AddToBag(ItemType.GunAK);
			AddAndEquip(ItemGun.Shotgun);
			AddToBag (ItemType.Food, 3);
			AddToBag (ItemType.Ammo, 25);
			AddToBag (ItemType.Bandages, 4);

			switch (playerClass)
			{
			case PlayerClass.AverageJoe:

				break;
			case PlayerClass.AverageJane:

				break;
			case PlayerClass.Athlete:
				InitialiseAthlete();
				break;
			case PlayerClass.Leader:
				InitialiseLeader();
				break;
			case PlayerClass.Soldier:
				InitialiseSoldier();
				break;
			}
		}


	}

	void InitialiseAthlete()
	{
		movement.SetBaseSpeed (defaultSpeed + 1);
		AddAndEquip ((ItemEquippable)ItemType.RunningShoes.GetItemInstance());
		fatigueComponent.MaxFatigue = 200;
		fatigueComponent.Fatigue = 200;

	}

	void InitialiseSoldier()
	{
		RPCRemoveItem ((int)ItemGun.Glock.GetItemType (), 1);
		AddAndEquip (ItemGun.AK47);
		AddToBag (ItemType.Ammo, 25);
		InvokeRepeating ("SoldierAddAmmo", 15, 15);
	}

	void SoldierAddAmmo()
	{
		if (inventory.GetItemCount (ItemType.Ammo) < 20) {
			RPCAddItem ((int)ItemType.Ammo, 1);
		}
	}
	

	void InitialiseLeader()
	{
		if (!Network.isClient) {
		/*	SurvivorBeing surv = ManagerScript.instance.SpawnRandomSurvivor (transform.position);
			surv.GetComponent<InteractComponentSurvivor> ().TrustRating = 100;
			surv.Follow (this);
			surv.AddAndEquip (ItemGun.Glock);
			surv.AddToBag (ItemType.Ammo, 20);
			surv.AddToBag (ItemType.Bandages, 5);
			surv.AddToBag (ItemType.Food, 8);*/
		}
	}
	


	// Update is called once per frame
	public void ManualUpdate () {
	
		if (ManagerScript.instance.PAUSED || healthComponent.IsDead ()) {
			return;
		}
		animatorModule.UpdateAnimation ();
		//Debug.Log ("Affected " + localAffected.Count);

		effectsComponent.ProcessEffects ();
		HandleInputs ();

		if (ForceFollow) {
			forceFollowTime -= Time.deltaTime;

			if (forceFollowTime <= 0) {
				ForceFollow = false;
			}
		}

		exposureComponent.DecayExposure (movement.MoveType);

		if (fatigueComponent.UpdateFatigue (movement.MoveType, Hunger)) {
			DoStopSprint();
		}

		if (hurtTimer > 0) {
			hurtTimer -= Time.deltaTime;

		} else {
			senseColor = new Color (exposureComponent.Exposure, exposureComponent.Exposure, 1f);
			senseLight.LightColor = senseColor;

			hurtTimer = 0;
		}

		if (Hunger > 0) {
			Hunger -= Time.deltaTime * (0.1f + 0.05f * Fatigue / 100f) * hungerMultiplier;

		}

		if (Hunger <= 0)
		{
			Hunger = 0;
			healthComponent.DecreaseMaxHealth(Time.deltaTime*0.5f);
		}

		/*if (Network.isClient)
		{
			if (ManagerScript.instance.allies.Count > 0)
			{
			Vector3 pos = ManagerScript.instance.allies [0].transform.position;
			MovingVector = (pos - transform.position ).normalized;
			}
		}*/

//		Debug.Log (MovingVector);
		movement.Move (MovingVector);
		if (this.playerClass == PlayerClass.AverageJoe) {
			//float angle = Mathf.Atan2(FacingVector.x,  FacingVector.y);
			//transform.rotation = Quaternion.Euler(new Vector3(0,angle,0));

		//	animatorModule.UpdateAnimationState ();
		} 


	}

	AnimationDecorator GetAnimationDecorator()
	{
		if (gunComponent.GunType == null || melee)
		{
			return AnimationDecorator.Unarmed;
		}
		else {
			return gunComponent.GunType.GetAnimationDecorator();
		}
	}

	/*public void ClientUpdate()
	{
		if (ManagerScript.instance.PAUSED || healthComponent.IsDead ()) {
			return;
		}

		movement.Move (MovingVector);
		//Debug.Log(transform.position);
		//Debug.Log (movement.IsSprinting);
		animatorModule.SetAnimationState (movement.IsWalking);
		//ManagerScript.instance.networkDebugText = movement.IsSprinting.ToString();

		if (interpVal < 1) {
			transform.position = Vector2.Lerp (transform.position, netInterpVec, interpVal);
			interpVal += 0.25f;
		}

	}*/

	void OnSerializeNetworkView(BitStream stream, NetworkMessageInfo info) {
		
	//	Debug.Log("Netwrite attempt " + networkView.viewID);
		if (stream.isWriting) {
			//Debug.Log ("Netwrite " + Network.isClient);
			float x = 0;
			float y = 0;

			float xVel = 0;
			float yVel = 0;

			float xFace = 0;
			float yFace = 0;

			float exposure = GetExposure();

			int movementState = 0;

			x = GlobalData.instance.player.transform.position.x;
			y = GlobalData.instance.player.transform.position.y;

			xVel = GlobalData.instance.player.MovingVector.x;
			yVel = GlobalData.instance.player.MovingVector.y;

			xFace = GlobalData.instance.player.FacingVector.x;
			yFace = GlobalData.instance.player.FacingVector.y;

			//movementState = (int)movement.MoveType;

			stream.Serialize (ref x);
			stream.Serialize (ref y);
			stream.Serialize (ref xVel);
			stream.Serialize (ref yVel);
			stream.Serialize (ref xFace);
			stream.Serialize (ref yFace);
			//stream.Serialize (ref movementState);
			stream.Serialize (ref exposure);
		} 

		
	}
	


	public SaveDataHero GetSaveObject()
	{
		SaveDataHero heroSave = new SaveDataHero ();
		heroSave.movementStats = movement.GetSaveObject ();
		heroSave.gunStats = gunComponent.GetSaveObject ();
		heroSave.exposureStats = exposureComponent.GetSaveObject ();
		heroSave.healthStats = healthComponent.GetSaveObject ();
		heroSave.inventorySave = inventory.GetSaveObject ();

		heroSave.hunger = Hunger;
		heroSave.fatigue = Fatigue;

		heroSave.pos = transform.position;
		heroSave.torchEnabled = torchEnabled;

		return heroSave;
	}

	public void LoadFromDataObject(SaveDataHero data)
	{
		movement.LoadFromSaveObject (data.movementStats);
		gunComponent.LoadFromSaveObject (data.gunStats);
		exposureComponent.LoadFromSaveObject (data.exposureStats);
		healthComponent.LoadFromSaveObject (data.healthStats);
		inventory.LoadFromSaveObject (data.inventorySave);

		Hunger = data.hunger;
		Fatigue = data.fatigue;

		transform.position = data.pos;

		if (data.torchEnabled) {
			torchOnFunction ();
		} else {
			torchOffFunction();
		}
	}

	public void Heal(ItemBandages bandages)
	{
		if (BandagesCount > 0) {
			RemoveFromBag(bandages.GetItemType());
			healthComponent.RecoverHealth(bandages.HealValue);
			SoundPlayer.instance.PlayForced(Sounds.bandAid, 0.72f);

			if (effectsComponent.IsEffected(EffectsType.Bleeding))
			{
				//effectsComponent.ApplyEffect(new EffectBleeding(-1f));
				effectsComponent.ReduceBleed(1);
			}
		}
	}

	public void PlayZipSound()
	{
		SoundPlayer.instance.PlayForced(Sounds.bagZip, 1);
	}

	public void Eat(ItemFood food)
	{

		if (FoodCount > 0) {
			SoundPlayer.instance.PlayForced(Sounds.appleBite, 1);

			RemoveFromBag(food.GetItemType());
			Hunger += food.HungerValue;
			Fatigue += food.HungerValue;

			if (Hunger > 100) {
				Hunger = 100;
			}

			if (Hunger > 50) {
				healthComponent.IncreaseMaxHealth (food.HungerValue * 0.25f);
			}
		}
	}

	#region IEquippor implementation
	public void EquipAction (ItemShoes shoes)
	{
		movement.AddMovementModifier (shoes.MovementModifier);
		exposureComponent.walkingExposure = shoes.WalkingExposure;
		exposureComponent.runningExposure = shoes.RunningExposure;
	}
	public void UnequipAction (ItemShoes shoes)
	{
		movement.AddMovementModifier (-shoes.MovementModifier);
		exposureComponent.walkingExposure = ExposureComponent.MIN_EXPOSURE_WALKING;
		exposureComponent.runningExposure = ExposureComponent.MIN_EXPOSURE_RUNNING;
	}

	public void EquipAction (ItemGun gun)
	{
		melee = false;
	}

	public void UnequipAction (ItemGun gun)
	{
		melee = true;
	}
	#endregion	

	#region ITargetable implementation

	public float GetExposure ()
	{
		return exposureComponent.Exposure + (effectsComponent.IsEffected(EffectsType.Bleeding) ? 10 : 0);
	}

	public bool IsMoving()
	{
		return (movement.MoveType != MovementType.none); 
	}
	


	#region ITargetable implementation

	public void AssignTargeter (ITargeter targeter)
	{
		
	}

	public void UnassignTargeter (ITargeter targeter)
	{
	
	}

	#endregion

	public float GetSqrExposure ()
	{
		return exposureComponent.Exposure * exposureComponent.Exposure;
	}

	public Transform GetTransform()
	{
		return transform;
	}

	public void IncreaseExposure(float value)
	{
		exposureComponent.AddBaseExposure(value);
	}

	public void ReduceBaseExposureSightLost(float originalValue)
	{
		exposureComponent.ReduceBaseExposureSightLost (originalValue);
	}

	public HealthComponent GetHealthComponent()
	{
		return healthComponent;
	}
	
	public EffectsComponent GetEffectsComponent()
	{
		return effectsComponent;
	}

	public Vector2 GetMovingVector()
	{
		//if (movement.IsMoving) {
			return FacingVector;
		/*} else
			return Vector2.zero;*/
	}

	public bool IsValid()
	{
		return !healthComponent.IsDead ();
	}
	#endregion

	#region IHealthReactive implementation
	public void TakeDamageCallback (float amount)
	{
		hurtTimer = HURT_PERIOD;
		senseLight.LightColor = Color.red;
	}

	[RPC]
	void _onDied()
	{
		ManagerScript.instance.living.Remove (gameObject);
		SoundPlayer.instance.PlayForced(Sounds.playerDeath);
		
		Vector3 pos = new Vector3 (transform.position.x, transform.position.y, 0.01f);

		healthComponent.HealthRecovery = 0;
		
		float xOffset = 0;
		float yOffset = 0;
		for (int i = 0; i < 9; i++) {
			GameObjectPool.instance.ServeBloodSplatter(pos + Vector3.ClampMagnitude(new Vector3(xOffset,yOffset),0.8f));
			xOffset += UnityEngine.Random.Range (-1f,1f);
			yOffset += UnityEngine.Random.Range (-1f,1f);
		}
		Destroy (senseLight);
		Destroy (Torch);

		if (ManagerScript.instance.allies.Count > 0) {
			foreach (Transform t in transform) {
				t.parent = ManagerScript.instance.allies[0].transform;
				t.localPosition = new Vector3(0, 0, t.position.z);
			}
			
		} else {
			gameObject.transform.DetachChildren ();
		}
		ManagerScript.instance.GameOver = true;
		Destroy (gameObject);
	}

	public void OnDied()
	{
		_onDied ();
		networkView.RPC ("_onDied", RPCMode.Others);

	}
	#endregion	
	
	

	bool IsFatigued()
	{
		return Fatigue < 1f;
	}


	private void torchOnFunction()
	{
		Torch.LightColor = torchColor;
		Torch.LightRadius = TorchRadius;
		torchEnabled = true;
	}

	[RPC]
	public void TorchOn()
	{
		if (!torchEnabled) {
			exposureComponent.AddLightEmitterExposure (60f);
			torchOnFunction ();
		}

		if (ManagerScript.networkType != NetworkType.Single) {
			networkView.RPC("TorchOn", RPCMode.Others);
		}
	}

	private void torchOffFunction()
	{
		Torch.LightColor = Color.black;
		Torch.LightRadius = 0.01f;
		torchEnabled = false;
	}

	[RPC]
	public void TorchOff()
	{
		if (torchEnabled) {
			exposureComponent.RemoveLightEmitterExposure ();
			torchOffFunction ();
		}

		if (ManagerScript.networkType != NetworkType.Single) {
			networkView.RPC("TorchOff", RPCMode.Others);
		}
	}
	

	void HandleSurfaceType ()
	{
		Ray ray = new Ray (transform.position, Vector3.forward);
		RaycastHit hit;
		if (Physics.Raycast (ray, out hit, 5f)) {
			if (hit.transform.CompareTag ("Earth")) {
				footstepsAudio.clip = GlobalData.instance.sounds [(int)Sounds.earthyFootstep];
			}
			else
				if (hit.transform.CompareTag ("Water")) {
					footstepsAudio.clip = GlobalData.instance.sounds [(int)Sounds.waterFootstep];
				}
				else
					if (hit.transform.CompareTag ("Wood")) {
						footstepsAudio.clip = GlobalData.instance.sounds [(int)Sounds.footstepWood];
					}
					else {
						footstepsAudio.clip = GlobalData.instance.sounds [(int)Sounds.footstep1];
					}
		}
		if (!footstepsAudio.isPlaying) {
			footstepsAudio.Play ();
			CancelInvoke ("StopFootstepAudio");
		}
	}

	void MovementInput ()
	{
		float mouseX = Input.GetAxis ("Mouse X");
		mouseTurn =  (mouseX * -1.35f + mouseTurn *0.82f  );
		mouseTurn = mouseTurn > 15 ? 15 : mouseTurn;
		mouseTurn = mouseTurn < -15 ? -15 : mouseTurn;
		//mousePos.z = 5.23f;
		Vector2 targetPos = Camera.main.WorldToScreenPoint (transform.position);

		//FacingVector = (mousePos - targetPos).normalized;
		FacingVector = transform.up;

		transform.Rotate (Vector3.forward, mouseTurn);
		bool moveForward = Input.GetMouseButton (1);
		bool WASD = Input.GetKey (KeyCode.W) || Input.GetKey (KeyCode.A) || Input.GetKey (KeyCode.S) || Input.GetKey (KeyCode.D);
		if (moveForward || Input.GetKey (KeyCode.Space) || WASD) {
			if (WASD) {
				MovingVector = Vector2.zero;
			}
			if (moveForward) {
				MovingVector = FacingVector;
				movement.SetBaseSpeed (defaultSpeed);
			}
			else {
				if (Input.GetKey (KeyCode.W)) {
					MovingVector += new Vector2 (0, 1);
				}
				if (Input.GetKey (KeyCode.S)) {
					MovingVector += new Vector2 (0, -1);
				}
				if (Input.GetKey (KeyCode.A)) {
					MovingVector += new Vector2 (-1, 0);
				}
				if (Input.GetKey (KeyCode.D)) {
					MovingVector += new Vector2 (1, 0);
				}
				MovingVector.Normalize();
				MovingVector = transform.TransformVector(MovingVector);
				float directionDot = Vector2.Dot (MovingVector, FacingVector) * 0.32f + 0.78f;
				if (directionDot > 1) {
					directionDot = 1;
				}
				else {
					//movement.StopSprint ();
					DoStopSprint();
				}
				movement.SetBaseSpeed (defaultSpeed * directionDot);
			}
			//movement.Move (MovingVector);
			ManagerScript.instance.CloseOpenMenus (false);
			HandleSurfaceType ();
		}
		else {
			if (MovingVector != Vector2.zero)
			{
				MovingVector = Vector2.zero;
				Invoke ("StopFootstepAudio", 0.2f);
			}
		}
	}

	[RPC]
	public void DoStartSprint()
	{
		movement.Sprint(8f);
		footstepsAudio.volume = 0.85f;
		animatorModule.SetAnimSpeedMultiplier (0.76f);
	}

	[RPC]
	public void DoStopSprint()
	{
		movement.StopSprint();
		footstepsAudio.volume = 0.6f;
		animatorModule.SetAnimSpeedMultiplier (1.15f);
	}

	void HandleInputs()
	{
	
		if (Input.GetKey (KeyCode.F5)) {
			Hunger = 100;
			Fatigue = 100;
			healthComponent.RecoverHealth(100);
			//effectsComponent.ApplyEffect(new EffectBleeding(-1000));
			effectsComponent.ReduceBleed(1000);

			AddToBag(ItemType.Ammo,50);
		}


		if (Input.GetKeyDown (KeyCode.Alpha1)) {
			melee = true;
		}

		if (Input.GetKeyDown (KeyCode.Alpha2) && gunComponent.GunType != null) {
			melee = false;
		}

		if (Input.GetKeyDown (KeyCode.Alpha3)) {
			Eat(ItemType.Food.GetItemInstance() as ItemFood);
		}

		if (Input.GetKeyDown (KeyCode.Alpha4)) {
			Heal(ItemType.Bandages.GetItemInstance() as ItemBandages);
		}

		MovementInput ();


		if (PrimaryInputs) {

			if (!movement.IsSprinting && Input.GetMouseButton (0)) {
				if (melee)
				{
					if (meleeComponent.Attack(FacingVector)) {
						animatorModule.DoPunch();
					}
				}
				else {
					gunComponent.Shoot(FacingVector);
					
				}

			}
			else if (Input.GetKeyDown (KeyCode.F)) {
				if (torchEnabled) {
					TorchOff ();
				} else {
					TorchOn ();
				}
			}
			else if (Input.GetKeyUp (KeyCode.I)) {
				ManagerScript.instance.SetOptionActivated(this);
				PrimaryInputs = false;
				Screen.lockCursor = false;
			}
			else if (Input.GetKey (KeyCode.Q)) {
				ForceFollow = true;
				forceFollowTime = 3;
			}
			else if (Input.GetKeyDown (KeyCode.Tab)) {
				ManagerScript.instance.HomeMarker.transform.position = new Vector3(transform.position.x, transform.position.y, -0.2f); 
				ManagerScript.instance.HomeMarker.SetActive(true);
				ManagerScript.instance.HomeMarkerPosition = transform.position;
			}
			
			
			if (Input.GetKey (KeyCode.LeftShift) && !IsFatigued()) {
				if (!movement.IsSprinting)
				{
					DoStartSprint();
					if (ManagerScript.networkType != NetworkType.Single)
					{
						networkView.RPC("DoStartSprint", RPCMode.Others);
					}
				}
			}
			else  {
				if (movement.IsSprinting)
				{
					DoStopSprint();
					if (ManagerScript.networkType != NetworkType.Single)
					{
						networkView.RPC("DoStopSprint", RPCMode.Others);
					}
				}
			}


		} else {
			if (Input.GetMouseButtonUp (0))
			{
				ManagerScript.instance.CloseOpenMenus(true);
			}
		}


		if (scrollReady && Mathf.Abs(Input.GetAxis ("Mouse ScrollWheel")) >= 1) {
			if (gunComponent.GunType != null)
			{
				melee = !melee;
				scrollReady = false;
				Invoke("ReadyScrollWheel", 0.15f);
			}
		}
	
	}

	void ReadyScrollWheel()
	{
		scrollReady = true;
	}

	#region IMelee implementation

	public void OnAttackSwing ()
	{
		if (ManagerScript.networkType != NetworkType.Single)
		{
			networkView.RPC("RPCMeleeSwing", RPCMode.Others);
		}
	}

	public void OnAttackHit ()
	{
		if (ManagerScript.networkType != NetworkType.Single) {
			networkView.RPC ("RPCMeleeHit", RPCMode.Others);
		}
	}

	#endregion

	#region IGunner implementation
	public void AssignBullet(BulletBehaviour bullet)
	{
		gunComponent.AssignBullet (bullet);
	}
	#endregion	



	[RPC]
	void RPCFireProjectile(Vector3 direction, NetworkViewID viewID)
	{

	}

	[RPC]
	void RPCFailFireProjectile()
	{
		
	}

	[RPC]
	void RPCMeleeSwing()
	{

	}

	[RPC]
	void RPCMeleeHit()
	{
		
	}

	void StopFootstepAudio()
	{
		footstepsAudio.Stop();
	}


	//INVENTORY GUI
	public bool MouseOnGui;
	public Vector2 scrollPosition1 = Vector2.zero;

	public void DrawMenu()
	{
		int xWidth = 400;
		int yWidth = 300;
		int xStart = (int)(Screen.width / 2f - xWidth/2f);
		int yStart = (int)(Screen.height / 2f - yWidth/2f);
		
		Rect guiRect = new Rect (xStart, yStart, xWidth, yWidth);
		
		if (guiRect.Contains (Event.current.mousePosition)) {
			MouseOnGui = true;
		} else {
			MouseOnGui = false;
		}
		
		GUI.Box (guiRect, GUIContent.none); 
		
		GUIStyle titleStyle = new GUIStyle (GUI.skin.label);
		titleStyle.alignment = TextAnchor.UpperCenter;
		titleStyle.fontSize = 20;
		titleStyle.fontStyle = FontStyle.Bold;
		
		
		GUIStyle subTitleStyle = new GUIStyle (GUI.skin.label);
		subTitleStyle.alignment = TextAnchor.UpperCenter;
		subTitleStyle.fontSize = 16;
		subTitleStyle.fontStyle = FontStyle.Bold;
		
		GUIStyle hintStyle = new GUIStyle (GUI.skin.label);
		hintStyle.alignment = TextAnchor.UpperCenter;
		hintStyle.fontSize = 12;
		hintStyle.fontStyle = FontStyle.Italic;
		
		//TAKE
		
		
		
		//STORE
		int heroFood = GlobalData.instance.player.FoodCount;
		int heroMeds = GlobalData.instance.player.BandagesCount;
		int heroAmmo = GlobalData.instance.player.gunComponent.Ammo;
		
		
		GUILayout.BeginArea (guiRect);
		GUILayout.Label ("Inventory", titleStyle);
		GUILayout.Label ("Click to use/equip item", hintStyle);
		//GUILayout.Label ("Hold number and click to assign hotkey", hintStyle);
		GUILayout.BeginHorizontal ();
		GUILayout.BeginVertical (GUILayout.Width (xWidth));

		scrollPosition1 = GUILayout.BeginScrollView(scrollPosition1,false,true);
		
		foreach(KeyValuePair<ItemType, int> entry in GlobalData.instance.player.inventory.items)
		{
			BaseItem item = entry.Key.GetItemInstance();
			string entryText;
			string tooltip;

			if (item.IsEquippable())
			{
				if (equipComponent.IsEquipped (item as ItemEquippable))
				{
					entryText = entry.Value+" x "+item.GetItemText() + " (Equipped)";
					tooltip = "Unequip: " + item.GetItemTooltip();
				}
				else {
					entryText = entry.Value+" x "+item.GetItemText();
					tooltip = "Equip: " + item.GetItemTooltip();
				}
			}
			else {
				entryText = entry.Value+" x "+item.GetItemText();
				tooltip = "Use: " + item.GetItemTooltip();
			}
			if (GUILayout.Button ( new GUIContent(entryText, tooltip))) 
			{
				//TAG item.Use(this);
			}
		}
		
		GUILayout.EndScrollView();
		GUILayout.Label (GUI.tooltip);
		GUILayout.EndVertical ();
		GUILayout.EndHorizontal ();
		
		
		GUILayout.EndArea ();
	}

	public bool IsMouseOver()
	{
		return MouseOnGui;
	}

	void IOptioned.HandleInputs()
	{

	}

	public void NotifyClose()
	{

	}

	#region ILocalAffector implementation
	public void AssignAffected (ILocalAffector affected)
	{
		localAffected.Add (affected);
	}
	public void UnassignAffected (ILocalAffector affected)
	{
		localAffected.Remove (affected);
	}
	#endregion

	#region ILocalAffected implementation

	public void NotifyEmigration (ILocalAffector affector)
	{
		localAffected.Remove (affector);
	}

	#endregion
}
