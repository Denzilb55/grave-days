﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GraveDays.Entity.Being;

public class BulletBehaviour : MonoBehaviour {


	private Vector2 lastPosition;
	public Vector2 direction;
	public float Penetration;


	private Transform _master;
	public Transform master
	{
		get { return _master; }

		set { 
			_master = value;
			Invoke ("FreeBullet", 0.8f);
		}
	}
	public float Damage = 40;
	public TrailRenderer trail;
	
	private float life;
	private List<Transform> hitList = new List<Transform> ();

	// Use this for initialization


	void Awake()
	{
		trail = GetComponent<TrailRenderer> ();
	}

	void Update()
	{

	}

	void OnEnable()
	{
		trail.time = 0.15f;
		lastPosition = transform.position;
		//Debug.Log ("Enable bullet " + networkView.viewID);
	}

	void FreeBullet()
	{

		if (gameObject.activeSelf) {
		//	Debug.Log ("Free bullet " + networkView.viewID);
		//	Debug.Log("Free bullet " + networkView.viewID + " ");
			MakeInactive ();
			if (ManagerScript.networkType != NetworkType.Single) {
				networkView.RPC ("MakeInactive", RPCMode.Others);
			}
		}
	}
	

	[RPC]
	public void MakeInactive()
	{
//		Debug.Log("Make inactive " + networkView.viewID);
//		Debug.Log ("Inactive bullet " + networkView.viewID);
		gameObject.SetActive (false);
		trail.time = -1;	
		master = null;
	}

	void InstatiateDirectionalBlood (RaycastHit2D hit)
	{

		GameObject blood = Instantiate (GlobalData.instance.bloodSplashHit) as GameObject;

		Vector2 v = rigidbody2D.velocity.normalized;
		blood.transform.position = (Vector2)hit.transform.position +  (hit.point - (Vector2)hit.transform.position).normalized *0.4f + (0.2f*v);
		float angle = Mathf.Atan2 (v.y, v.x) * Mathf.Rad2Deg - 90;
		blood.transform.rotation = Quaternion.AngleAxis (angle, Vector3.forward);
	}

	bool BulletHit(RaycastHit2D hit)
	{
		if (hit.transform != null) {
			
			if (hit.transform.CompareTag("Zombie") ) // || hit.transform.tag == "Survivor" || hit.transform.tag == "Player"   ) 
			{
				if (hit.transform.gameObject.transform == master)
				{
					return false;
				}
				
				if (ManagerScript.networkType == NetworkType.Single || (Network.isServer && master == GlobalData.instance.player.transform))
				{
			
					if (!hitList.Contains(hit.transform))
					{
						HealthComponent healthHit = hit.transform.gameObject.GetComponent<HealthComponent> ();
						healthHit.TakeDamage (Damage);
						
						//Vector3 pos = new Vector3 (hit.transform.position.x, hit.transform.position.y, 0.01f);
						
						GameObjectPool.instance.ServeBloodSplatter(hit.point);


	
						if (Random.Range(0f, 1f) >= Penetration)
						{
							FreeBullet();
							return true;
						}
						else {
							InstatiateDirectionalBlood (hit);
							

							hitList.Add(hit.transform);
							return false;
						}
					}
				}
				else if (Network.isClient && master == GlobalData.instance.player.transform)
				{

					//RPC bullet hits
					if (!hitList.Contains(hit.transform))
					{
						ZombieBeing zombie = hit.transform.GetComponent<ZombieBeing>();
						zombie.RPCHitByBullet(Damage);
						NetworkScript.instance.ServeBloodSplatter(RPCMode.All, hit.point);
						//hitList.Add(hit.transform);
						if (Random.Range(0f, 1f) >= Penetration)
						{
							FreeBullet();
							return true;
						}
						else {
							hitList.Add(hit.transform);

						}
					}
					
				}
				
				
			} else if (hit.transform.CompareTag("Solid")) {
				//Destroy(gameObject);
				if (ManagerScript.networkType == NetworkType.Single)
				{
					FreeBullet();
					return true;
				}
				else if (master == GlobalData.instance.player.transform){
					FreeBullet();
					return true;
					//	Debug.Log("BULLET HIT " + networkView.viewID + " " + networkView.owner);
				}
			}
		}

		return false;
	}

	void FixedUpdate()
	{
		if (master == null) {
			return;
		}

		//Ray2D ray = new Ray2D (transform.position, direction);
		int layer = (1 << 9) | (1 << 10) |(1 << 17) | (1 << 22);
		RaycastHit2D hit = Physics2D.Raycast ((Vector2)transform.position, direction, 0.8f, layer);

		if (!BulletHit(hit))
		{
			Vector2 dir = (lastPosition - (Vector2)transform.position);
			float dist = dir.magnitude;
			hit = Physics2D.Raycast ((Vector2)transform.position, dir, dist, layer);
			BulletHit(hit);
		}

		//add case for survivor
		this.lastPosition = transform.position;
	}





}
