﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BuildingBehaviour : ItemHolder {
	
	public float spawnPeriod;
	public int spawnRandom;
	public List<GameObject> goods = new List<GameObject>();

	//public int maxGoods;
	public int startingGoods;
	public List<SpawnSlotBehaviour> slots = new List<SpawnSlotBehaviour>();
	private List<SpawnSlotBehaviour> openSlots = new List<SpawnSlotBehaviour>();
	
	// Update is called once per frame
	void Update () {
//		Debug.Log ("SLOTS: " + slots.Count);

		if (Network.isClient) {
			return;
		}

		if (openSlots.Count > 0) {
			if (spawnTime <= 0) {
				SpawnGoods ();	
			} else {
				spawnTime -= Time.deltaTime;
			}
		}

		/*if ((transform.position - GlobalData.instance.player.transform.position).magnitude < 8) {
			Debug.Log(spawnTime + " " + openSlots.Count);
		}*/
	}

	

	void Start()
	{
		if (!ManagerScript.doLoad && !Network.isClient) {
			for (int i = 0; i < startingGoods; i++) {
				SpawnGoods ();
			}
		}
	}

	#region implemented abstract members of ItemHolder
	

	protected override void AwakeInit ()
	{
		foreach (var slot in slots) {
			openSlots.Add (slot);
		}
	}
	

	public override void ItemRemoved(PickupItem item)
	{
		foreach (var slot in slots) {
			if (slot.item == item)
			{
				openSlots.Add(slot);
				slot.item = null;
				return;
			}
		}
	}
	
	//external call only
	public override void ItemAdded (PickupItem item)
	{
	//	Debug.Log ("SLOTSLOAD: " + slots.Count);
		SpawnSlotBehaviour foundSlot = null;
		foreach (var slot in slots) {
			Vector2 deltaPos = (Vector2)(slot.transform.position - item.transform.position);
			if (Mathf.Abs(deltaPos.x) + Mathf.Abs(deltaPos.y) < 0.01f)
			{
				foundSlot = slot;
				break;
			}
		}

		if (foundSlot != null) {
			openSlots.Remove (foundSlot);
		//	Debug.Log("Slot found");
		} else {
			//Debug.Log("Slot not found");
		}
	}

	#endregion

	SpawnSlotBehaviour GetOpenSlot()
	{
		return openSlots[Random.Range(0,openSlots.Count)];
	}

	protected override void SpawnGoods()
	{

		int randomGood = Random.Range (0, goods.Count);

		SpawnSlotBehaviour s = GetOpenSlot ();
		if (s != null) {


			PickupItem item = ManagerScript.instance.TrySpawnPickup (s.transform.position, goods [randomGood]);
			if (item == null)
			Debug.Log(name + " ");
			if (item != null) {

				openSlots.Remove(s);
				item.SetItemHolder (this);
				s.item = item;
			}

			spawnTime = spawnPeriod + Random.Range(0, spawnRandom);
			//spawnTime = 4;
		}
	}
}
