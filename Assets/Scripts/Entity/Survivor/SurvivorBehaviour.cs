using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using GraveDays.Items.Guns;
using GraveDays.Items;
using GraveDays.Quests;
using GraveDays.Animation;
using GraveDays.Entity;
using GraveDays.Entity.Being;
using GraveDays.Entity.Being.AI;



public class SurvivorBehaviour : MonoBehaviour, IHealthReactive, ITargetable, ITargeter, IOccupant, IEquippor, IGunner, IQuestTarget, ILocalAffector {
	public int ThinkID;
	private ITargetable target;
	private Vector2 moveGoal;
	public float fatigue;
	private bool stunned = false;
	private MovementComponent movement;
	private AnimatorDecorated animatorModule;
	private MeleeComponent meleeComponent;
	public EffectsComponent effectsComponent;
	private HealthComponent healthComponent;
	public EquipComponent equipComponent;
	public SurvivorSpeechComponent speechComponent;
	private Vector2 targetVector;
	private Vector2 lastKnownTargetVector;
	private Vector2 lastKnownTargetPosition;
	private bool hasSraightPath = true;

	private ZombieScanner zombieScanner;
	public bool isMale;
	public bool isHungry;

	private List<ITargeter> targeterList = new List<ITargeter> ();
	public List<ILocalAffector> localAffected = new List<ILocalAffector> ();

		
	public BehaviourState StateAI { get; private set; }
	
	private const float exposureOnCall = 3.8f;
	
	private float searchRange = 5f;
	private float colliderRadius;

	public GunComponent gunComponent;
	public InventoryComponent inventory;
	
	public const int AVOID_LAYER = (1 << 8) | (1 << 10) | (1 << 13);
	public const int SIGHT_LAYER = (1 << 14);

	public ITargetable followTarget;
	
	public bool debugEnable;

	private int pushCount;
	private bool bumpReactReady = true;
	private const int BUMP_REACT_TIME = 1;

	private bool HasTarget;
	private bool HasFollowTarget;
	private bool HasFollowTargetMem;
	private bool IsHoldingPosition;


	private Vector2 netInterpVec;
	private float interpVal;
	private bool interpEnable;
	public bool isMoving;


	static int IDs;
	int ID;
	static int cb;  
	
	// Use this for initialization
	void Start () {
		if (!Network.isClient) {
			if (UnityEngine.Random.Range (0, 3) == 0) {
				AddAndEquip (ItemGun.Glock);
				AddToBag (ItemType.Ammo, UnityEngine.Random.Range (5, 30));
			}
			AddToBag (ItemType.Food, UnityEngine.Random.Range (0, 8));
			AddToBag (ItemType.Bandages, UnityEngine.Random.Range (0, 8));

			if (UnityEngine.Random.Range(0,2) == 0 )
			{
				AddAndEquip (ItemType.GunShotgun.GetItemInstance() as ItemEquippable);
			} else {
				AddAndEquip (ItemType.GunAK.GetItemInstance() as ItemEquippable);
			}
			AddToBag (ItemType.Ammo, 50);
		}
	}

	public void AddAndEquip(ItemEquippable item)
	{
		AddToBag (item.GetItemType());
		equipComponent.Equip (item);
	}
	
	public void AddToBag(ItemType item, int amount = 1)
	{
		if (ManagerScript.networkType == NetworkType.Single) {
			inventory.AddItem (item, amount);
		} else {
			networkView.RPC("RPCAddItem", RPCMode.All, (int)item, amount);
		}
	}

	public void RemoveFromBag(ItemType item, int amount = 1)
	{
		if (ManagerScript.networkType == NetworkType.Single) {
			inventory.RemoveItem (item, amount);
		} else {
			networkView.RPC("RPCRemoveItem", RPCMode.All, (int)item, amount);
		}
	}

	[RPC]
	void RPCAddItem(int itemIndex, int amount)
	{
		inventory.AddItem ((ItemType)itemIndex, amount);
	}

	[RPC]
	void RPCRemoveItem(int itemIndex, int amount)
	{
		inventory.RemoveItem ((ItemType)itemIndex, amount);
	}
	
	
	void Awake()
	{
		float moveSpeed = UnityEngine.Random.Range (12.5f, 13.4f);
		movement = GetComponent<MovementComponent> ();
		meleeComponent = GetComponent<MeleeComponent> ();
		gunComponent = GetComponent<GunComponent> ();
		gunComponent.gunner = this;
		zombieScanner = GetComponentInChildren <ZombieScanner> ();
		equipComponent = GetComponent<EquipComponent> ();
		movement.SetBaseSpeed (moveSpeed);
		healthComponent = GetComponent<HealthComponent> ();
		effectsComponent = GetComponent<EffectsComponent> ();
		healthComponent.Initialise (this, UnityEngine.Random.Range (100, 200));
		colliderRadius = ((CircleCollider2D)collider2D).radius * 1.2f;
		inventory = GetComponent<InventoryComponent> ();

		isMale = true;

		if (isMale) {
			speechComponent = gameObject.AddComponent<SurvivorSpeechMaleComponent>();
			animatorModule = gameObject.AddComponent<AnimatorJoe>();
		} else {
			speechComponent = gameObject.AddComponent<SurvivorSpeechFemaleComponent>();
			animatorModule = gameObject.AddComponent<AnimatorJane>();
		}

		//ManagerScript.instance.survivors.Add (this);

		ID = IDs;
		IDs++;



	}

	void OnCollisionStay2D(Collision2D collision) {
		if (collision.gameObject.CompareTag ("Survivor")) {
			Vector2 vec = (Vector2)(transform.position - collision.transform.position);
			rigidbody2D.AddForce (vec.normalized.Rotate(5) * 10);


		} else if (collision.gameObject.CompareTag ("Player") || collision.gameObject.CompareTag ("Ally")) {
			pushCount += 4;
			if (++pushCount > 64)
			{
				Physics2D.IgnoreCollision (collider2D, collision.collider);
			}
	
		}
	}

	void bumpReact() {
		bumpReactReady = false;
		Invoke ("readyBumpReact", BUMP_REACT_TIME);
	}

	void readyBumpReact() {
		bumpReactReady = true;
	}

	void OnCollisionEnter2D(Collision2D collision) {


		if (!bumpReactReady || Network.isClient || healthComponent.IsDead ())
			return;

		if (HasTarget && ( !target.IsValid() || target.GetTransform() == collision.gameObject.transform)) 
			return;

		if (collision.gameObject.CompareTag("Zombie")) {
			bumpReact();
			HealthComponent health = collision.gameObject.GetComponent<HealthComponent> ();
			

			ITargetable collisionTarget = collision.gameObject.GetComponent<ZombieBehaviour>() as ITargetable;
			if (collisionTarget != target)
			{
				if (!health.IsDead())
				{
		
					QuickSetTarget(collisionTarget, true);

				}
				return;
			}

			
		} 
		
		
	}

	
	public SaveDataZombie GetSaveObject()
	{
	
		
		return null;
	}

	
	public void LoadFromDataObject(SaveDataZombie data)
	{

	}

	public void AssignBullet(BulletBehaviour bullet)
	{
		gunComponent.AssignBullet (bullet);
	}

	public void Follow(ITargetable t)
	{
		AssignTarget(t);
		AssignFollowTarget (t);

		StateAI = BehaviourState.follow;
		speechComponent.SayFollow ();

	}

	[RPC]
	public void FollowAlly(NetworkPlayer player)
	{
		foreach (var ally in ManagerScript.instance.allies) {
			if (ally.networkView.owner == player)
			{
				SetTarget (ally);
				AssignFollowTarget (ally);
				
				StateAI = BehaviourState.follow;
				speechComponent.SayFollow ();
				return;
			}
		}


		
	}

	public bool IsFollowing(ITargetable t)
	{
		if (followTarget == t && HasFollowTarget) {
			return true;
		}

		return false;
	}

	#region IInteractable implementation
	public bool Interact (HeroBeing hero)
	{
		speechComponent.SayHello ();
		return true;

	}
	#endregion	

	#region IEquippor implementation
	
	public void EquipAction (ItemShoes shoes)
	{
		movement.AddMovementModifier (shoes.MovementModifier);
		//exposureComponent.walkingExposure = shoes.WalkingExposure;
		//exposureComponent.runningExposure = shoes.RunningExposure;
	}
	public void UnequipAction (ItemShoes shoes)
	{
		movement.AddMovementModifier (-shoes.MovementModifier);
		//exposureComponent.walkingExposure = ExposureComponent.MIN_EXPOSURE_WALKING;
		//exposureComponent.runningExposure = ExposureComponent.MIN_EXPOSURE_RUNNING;
	}
	
	public void EquipAction (ItemGun gun)
	{
		if (gunComponent.Ammo > 0 && StateAI == BehaviourState.meleeEngage) {
			StateAI = BehaviourState.stationaryEngage;
		}
	}
	
	public void UnequipAction (ItemGun gun)
	{
		//melee = true;
	}

	#endregion
	
	public void DoUnspawn()
	{
		
	}

	#region ITargeter implementation

	public void NotifyDeath (ITargetable target)
	{
		if (this.target == target) {
			ReleaseTarget ();
		}
	}

	public float GetDotToTarget()
	{
		return 1;
	}

	#endregion

	#region ITargetable implementation
	
	public void UnassignTargeter (ITargeter targeter)
	{
		targeterList.Remove (targeter);
	}
	
	public void AssignTargeter (ITargeter targeter)
	{
		targeterList.Add (targeter);
	}

	#endregion

	private void AssignTarget(ITargetable tar)
	{
		target = tar;
		HasTarget = true;
		target.AssignTargeter (this);
	}

	private void AssignFollowTarget(ITargetable tar)
	{
		IsHoldingPosition = false;
		followTarget = tar;
		HasFollowTarget = true;
		HasFollowTargetMem = true;
	}

	private void ReleaseFollowTarget()
	{
		HasFollowTarget = false;
		StateAI = BehaviourState.idleStand;
	}

	
	// Update is called once per frame
	public void ManualUpdate () {
		if (ManagerScript.instance.PAUSED || stunned || healthComponent.IsDead ()) {
			return;
		}

		animatorModule.UpdateAnimation ();
		effectsComponent.ProcessEffects ();

		if (GlobalData.instance.player != null) {
			if (Physics2D.GetIgnoreCollision (collider2D, GlobalData.instance.player.collider2D)) {

				if (pushCount > 0) {
					pushCount--;
				} else {
					Physics2D.IgnoreCollision (collider2D, GlobalData.instance.player.collider2D, false);
				}
			}
		}



		bool immediateThink = (StateAI == BehaviourState.meleeEngage || StateAI == BehaviourState.stationaryEngage);
		if (StateAI != BehaviourState.follow )
		{

			if (HasTarget && !RevalidateTarget(target))
			{
				ReleaseTarget();
			}
			else {
				immediateThink = false;
			}

		}
		else {
			if (HasTarget && !RevalidateTargetStatus(target))
			{
				ReleaseTarget();
			}
			else {
				immediateThink = false;
			}
		}
	


		if (GlobalData.instance.player.ForceFollow && HasFollowTarget) {
			AssignTarget(followTarget);
			StateAI = BehaviourState.follow;
		}


		if (Time.frameCount % 45 == ThinkID % 45 || immediateThink)
		{
			CasualThink();
		}


		Think ();

		if (StateAI == BehaviourState.meleeEngage) {	
			if (hasSraightPath) {
				if (targetVector.sqrMagnitude < 1.3*1.3)
				{
					if (meleeComponent.Attack(targetVector)) {
						animatorModule.DoPunch();
					}
				}
			}
			isMoving = true;
		} else if (StateAI == BehaviourState.stationaryEngage) {

			if (hasSraightPath) {
				if (gunComponent.Shoot (targetVector.normalized))
				{
					StateAI = BehaviourState.meleeEngage;
				}
			}

			isMoving = false;
		}

		else if (StateAI == BehaviourState.idleWalk || StateAI == BehaviourState.idleSearch || StateAI == BehaviourState.follow)
		{
			isMoving = true;
		} else {
			isMoving = false;
		}

		if (isMoving) {
			movement.Move (targetVector);
		} else {
			movement.Stop ();
		}

		float angle = Mathf.Atan2 (targetVector.y, targetVector.x) * Mathf.Rad2Deg;
		transform.eulerAngles = new Vector3 (0, 0, angle -90);
	//	animatorModule.UpdateAnimationState ();
		
	}

	AnimationDecorator GetAnimationDecorator()
	{
		if (gunComponent.GunType == null || gunComponent.Ammo == 0)
		{
			return AnimationDecorator.Unarmed;
		}
		else {
			return gunComponent.GunType.GetAnimationDecorator();
		}
	}
	
	void Intercept(float value)
	{
		
		if (StateAI == BehaviourState.meleeEngage) {
			Vector2 interceptVector = (target.GetMovingVector() * value);
			if (Vector2.Dot(targetVector.normalized, interceptVector.normalized) > - 0.3f)
			{
				targetVector += interceptVector;
			}
		}
	}

	#region ITargetable implementation
	public float GetExposure ()
	{
		return ManagerScript.instance.LightExposure/100f + 0.3f;
	}

	public bool IsMoving()
	{
		return (movement.MoveType != MovementType.none); 
	}

	public float GetSqrExposure ()
	{
		float exps = GetExposure ();
		return exps * exps;
	}
	public Transform GetTransform ()
	{
		return transform;
	}
	public void IncreaseExposure (float value)
	{

	}
	public void ReduceBaseExposureSightLost (float originalValue)
	{

	}

	public HealthComponent GetHealthComponent()
	{
		return healthComponent;
	}
	
	public EffectsComponent GetEffectsComponent()
	{
		return effectsComponent;
	}

	public Vector2 GetMovingVector ()
	{
		return targetVector.normalized;
	}

	public bool IsValid()
	{
		return !healthComponent.IsDead ();
	}
	#endregion	
	
	#region IHealthReactive implementation
	
	public void TakeDamageCallback (float amount)
	{
		
	
		
	}

	
	
	
	#endregion


	public void ClientUpdate () {
		if (ManagerScript.instance.PAUSED  || stunned) {
			return;
		}


		if (isMoving) {
			movement.Move (targetVector);
		} else {
			movement.Stop ();
		}

		float angle = Mathf.Atan2 (targetVector.y, targetVector.x) * Mathf.Rad2Deg;
		transform.eulerAngles = new Vector3 (0, 0, angle -90);
		//animatorModule.UpdateAnimationState ();
		
		if (interpEnable && interpVal < 1) {
			transform.position = Vector2.Lerp (transform.position, netInterpVec, interpVal);
			interpVal += 0.25f;
		}
		
	}

	void OnSerializeNetworkView(BitStream stream, NetworkMessageInfo info) {
		
		if (stream.isWriting)
		{

			float x = transform.position.x;
			float y = transform.position.y;
			
			stream.Serialize (ref x);
			stream.Serialize (ref y);
			
			float xVel = targetVector.x;
			float yVel = targetVector.y;
			
			stream.Serialize (ref isMoving);
			stream.Serialize (ref xVel);
			stream.Serialize (ref yVel);
		}
		else 
		if (stream.isReading)
		{
			float x = 0;
			float y = 0;
			float xVel = 0;
			float yVel = 0;
			
			stream.Serialize(ref x);
			stream.Serialize(ref y);
			
			stream.Serialize(ref isMoving);
			stream.Serialize(ref xVel);
			stream.Serialize(ref yVel);
			netInterpVec = new Vector2(x, y);
			interpVal = 0;
			//transform.position = 
			targetVector = new Vector2(xVel, yVel);
			
			if (!interpEnable)
			{
				interpEnable = true;
				transform.position = netInterpVec;
			}
		}
		
	}

	[RPC]
	public void HoldPosition(bool speak = true)
	{
		if (HasFollowTarget) {
			ReleaseFollowTarget();
			StateAI = BehaviourState.idleStand;
			moveGoal = transform.position;
			IsHoldingPosition = true;
			if (speak)
			{
				speechComponent.SayBye();
			}
			HasFollowTargetMem = false;
		}
	}

	[RPC]
	public void StopFollow(bool speak = true)
	{
		if (HasFollowTarget) {
			ReleaseFollowTarget();
			StateAI = BehaviourState.idleStand;
			IsHoldingPosition = false;
			if (speak)
			{
				speechComponent.SayBye();
			}
			HasFollowTargetMem = false;
		}
	}

	void ReleaseTarget()
	{

		if (HasTarget) {

			if (target != null) {
				target.ReduceBaseExposureSightLost (exposureOnCall);
				target.UnassignTargeter(this);
			}

			HasTarget = false;
			StateAI = BehaviourState.idleStand;
			ZombieBehaviour zomb = target as ZombieBehaviour;

		}
	}
	
	
	int CornerCastHit(Bounds bounds, Vector2 direction, float distance, int layer)
	{
		Vector2 thisPos = ((CircleCollider2D) collider2D).center + (Vector2)transform.position;
		Vector2 vecToTarget = (direction.normalized)*(colliderRadius);
		
		Vector2 [] p = new Vector2[3];
		Vector2 edge1 = vecToTarget.Rotate (90) + thisPos;
		Vector2 edge2 = vecToTarget.Rotate (-90) + thisPos;
		p [0] = (Vector2)transform.position;
		p [1] = edge1;
		p [2] = edge2;
		
		
		
		int c = 0;
		for (int i = 0; i < 3; i++) {
			RaycastHit2D hit;
			if (hit = Physics2D.Raycast (p[i], direction, distance, layer))
			{

				c++;
			}
		}
		
		
		return c;
		
	}
	
	bool IsUnhindered(Bounds bounds, Vector2 targetPos)
	{
		Vector2 thisPos = ((CircleCollider2D) collider2D).center + (Vector2)transform.position;
		Vector2 vecToTarget = ((targetPos - thisPos).normalized)*colliderRadius;
		
		Vector2 [] p = new Vector2[3];
		Vector2 edge1 = vecToTarget.Rotate (90) + thisPos;
		Vector2 edge2 = vecToTarget.Rotate (-90) + thisPos;
		p [0] = (Vector2)transform.position;
		p [1] = edge1;
		p [2] = edge2;
		
		
		int c = 0;
		for (int i = 0; i < 3; i++) {
			
			Vector2 pointVector = targetPos - p[i];
			
			RaycastHit2D hit = Physics2D.Raycast (p[i], pointVector, pointVector.magnitude, AVOID_LAYER);
			if (hit.transform == null)
			{
				c++;
			}
	
		}

		return c >= 3;
		
	}
	
	bool IsUnhindered(Bounds bounds, Vector2 targetPos, float range)
	{
		Vector2 thisPos = ((CircleCollider2D) collider2D).center + (Vector2)transform.position;
		Vector2 vecToTarget = ((targetPos - thisPos).normalized)*colliderRadius;
		
		Vector2 [] p = new Vector2[3];
		Vector2 edge1 = vecToTarget.Rotate (90) + thisPos;
		Vector2 edge2 = vecToTarget.Rotate (-90) + thisPos;
		p [0] = (Vector2)transform.position;
		p [1] = edge1;
		p [2] = edge2;
		
		int c = 0;
		for (int i = 0; i < 3; i++) {
			
			Vector2 pointVector = targetPos - p[i];
			
			RaycastHit2D hit = Physics2D.Raycast (p[i], pointVector, range, AVOID_LAYER);
			if (hit.transform == null)
			{
				c++;
			}

		}

		return c >= 3;
	}
	
	bool InLineOfSight(Vector2 targetVec)
	{
		RaycastHit2D hit = Physics2D.Raycast (transform.position, targetVec, targetVec.magnitude, SIGHT_LAYER);
		return (hit.transform == null);
	}
	
	void ComputeVectorToTarget()
	{
		if (StateAI == BehaviourState.meleeEngage || StateAI == BehaviourState.stationaryEngage) {
			targetVector = target.GetTransform ().position - transform.position;
		} else if (StateAI == BehaviourState.follow) {

			if (hasSraightPath)
			{
				targetVector = (Vector2)(target.GetTransform ().position - transform.position) + (Vector2)target.GetMovingVector()*3.2f*0;
			}
			else {
				targetVector = (Vector2)(target.GetTransform ().position - transform.position);
			}

		}
		else if (StateAI == BehaviourState.idleWalk) {
			targetVector = moveGoal - (Vector2)transform.position;
		}
		
	}
	
	void PathSteer()
	{
		if (!hasSraightPath) {
			AvoidanceSteer ();
			
		} else {
			lastKnownTargetVector = targetVector;
			lastKnownTargetPosition = target.GetTransform().position;
			//lastKnownTargetDirection = target.GetMovingVector();
		}
	}
	
	void AvoidanceSteer()
	{
		int hit = CornerCastHit (renderer.bounds, lastKnownTargetVector, colliderRadius *2f, AVOID_LAYER);
		
		int avoidCountOut = 0;
		int angleCount = 0;
		int lastHit = 0;
		
		Vector2 rotateVector = lastKnownTargetPosition - (Vector2)transform.position;
		if (rotateVector.sqrMagnitude < 0.3f) {
			rotateVector = lastKnownTargetVector;
		}
		Vector2 lastRotateVector = rotateVector;
		int dir=0;
		while ((avoidCountOut < 22 && hit != 0)) {
			avoidCountOut++;
			
			if (avoidCountOut % 2 == 0) {
				angleCount++;
				dir = 1;
			} else {
				dir = -1;
			}
			
			lastRotateVector = rotateVector;
			rotateVector = lastKnownTargetVector.Rotate (dir * angleCount * 9f);
			lastHit = hit;
			hit = CornerCastHit (renderer.bounds, rotateVector, colliderRadius *2f, AVOID_LAYER);
			
		}


		targetVector = rotateVector;
		//targetVector = lastKnownTargetPosition - (Vector2)transform.position; 
	}
	
	public void Think()
	{


		if (StateAI != BehaviourState.idleStand) {
			if (StateAI == BehaviourState.follow && HasFollowTarget && followTarget.IsValid())
			{
				hasSraightPath = IsUnhindered (renderer.bounds, followTarget.GetTransform ().position);
			}
			else if (HasTarget && target.IsValid ()) {
				hasSraightPath = IsUnhindered (renderer.bounds, target.GetTransform ().position);
			}
						
		}

		ComputeVectorToTarget ();

		if ( StateAI == BehaviourState.meleeEngage || StateAI == BehaviourState.follow)
		{
			PathSteer();
		} else if (StateAI == BehaviourState.idleWalk) {
			
			CheckMoveGoalReached ();
			
			
		}
	}
	
	void DoMoveGoalReached()
	{
		StateAI = BehaviourState.idleStand;
	}
	
	void CheckMoveGoalReached()
	{
		if (targetVector.sqrMagnitude < 1) {
			DoMoveGoalReached();
		}
	}
	
	void RandomWalk (Vector2 bias)
	{
		
		float xRandom = UnityEngine.Random.Range (-8, 8);
		float yRandom = UnityEngine.Random.Range (-8, 8);
		
		targetVector = Vector2.ClampMagnitude(new Vector2 (xRandom, yRandom), 8) + bias;
		
		moveGoal = (targetVector + (Vector2)transform.position);
		
		if (GlobalData.instance.IsInWorld (moveGoal)) {
			StateAI = BehaviourState.idleWalk;
		} else {
			StateAI = BehaviourState.idleStand;
		}

	}
	
	
	public void Stun(float period)
	{
		stunned = true;
		CancelInvoke ("StunRecover");
		Invoke ("StunRecover", period);
	}
	
	public void StunRecover()
	{
		stunned = false;
	}
	
	
	bool IdleGeneral(float thinkVal1, float thinkVal2)
	{
		if (HasFollowTargetMem && followTarget != null) {

			Vector2 vec = followTarget.GetTransform().position - transform.position;
			if (InLineOfSight(vec))
			{
				HasFollowTarget = true;
				StateAI = BehaviourState.follow;
				AssignTarget(followTarget);
				return true;
			}

		}

		return AlertThink ();
	}

	bool AlertThink()
	{

		if (SearchForTarget ()) {
			return true;			
		} else {
		
			if (HasFollowTarget)
			{
				Vector2 vecToTarget = followTarget.GetTransform().position - transform.position;
		
				if (followTarget.IsMoving() || vecToTarget.sqrMagnitude > 16)
				{
					if (target != followTarget)
					{
						if (QuickSetTarget(followTarget, true, vecToTarget))
						{
							StateAI = BehaviourState.follow;
							return true;
						}
					}
					else {
	
						StateAI = BehaviourState.follow;
						return true;
					}
				}
				else if (StateAI == BehaviourState.follow){
					ReleaseTarget();
				}
				
			}
			return false;
		}
	}

	void ConditionalThink(float thinkVal1, float thinkVal2)
	{
		if (thinkVal2 < 3) {
			AddToBag(ItemType.Ammo, 2);
			healthComponent.RecoverHealth (1);
			isHungry = true;
		} 
		AddToBag (ItemType.Ammo, 50);
		
		
		if (thinkVal1 < 80) {
			if (isHungry)
			{
				RemoveFromBag(ItemType.Food);
				isHungry = false;
			}
			
			if (healthComponent.Health < 0.8f * healthComponent.MaxHealth)
			{
				if (inventory.GetItemCount(ItemType.Bandages) > 0)
				{
					RemoveFromBag(ItemType.Bandages);
					healthComponent.RecoverHealth((ItemType.Bandages.GetItemInstance() as ItemBandages).HealValue);
					
					if (effectsComponent.IsEffected(EffectsType.Bleeding))
					{
						effectsComponent.ReduceBleed(2);
					}
					
				}
			}
		}
	}
	
	void CasualThink()
	{
		float thinkVal1 = UnityEngine.Random.Range (0, 200);
		float thinkVal2 = UnityEngine.Random.Range (0, 400);

		ConditionalThink (thinkVal1, thinkVal2);

		switch (StateAI) {
		case BehaviourState.idleStand:
			if (!IdleGeneral (thinkVal1, thinkVal2)) {
				if (!IsHoldingPosition)
				{
					if (thinkVal2 < 80 || (HasFollowTarget && thinkVal2 < 110)) {
						RandomWalk (Vector2.zero);
					}
				}
				else {
					//moveGoal = (lastKnownTargetPosition);
					StateAI = BehaviourState.idleWalk;

				}
			}	
			break;

		case BehaviourState.idleWalk:
			if (!IsUnhindered (renderer.bounds, moveGoal, 1f)) {
				StateAI = BehaviourState.idleStand;
			}
			
			IdleGeneral (thinkVal1, thinkVal2);
			break;

		case BehaviourState.meleeEngage:
			if (!hasSraightPath && (lastKnownTargetPosition - (Vector2)transform.position).sqrMagnitude < 1f) {
				Invoke ("TargetPositionReached", 0.2f);
			}
			break;

		case BehaviourState.stationaryEngage:
			break;

		case BehaviourState.idleSearch:
			IdleGeneral (thinkVal1, thinkVal2);
			break;

		case BehaviourState.follow:
			if (!GlobalData.instance.player.ForceFollow)
			{
				AlertThink ();
				
				if (!hasSraightPath && (lastKnownTargetPosition - (Vector2)transform.position).sqrMagnitude < 1f) {
					
					Invoke ("TargetPositionReached", 0.2f);
					
				}
			}
			break;

		case BehaviourState.holdPosition:

			break;
		}
			
	}

	[RPC]
	void _onDied()
	{

		//SoundPlayer.instance.Play (Sounds.playerDeath, transform, 8);
		speechComponent.SayDeath ();
		

		///ManagerScript.instance.survivors.Remove (this);
		Destroy (gameObject, 1.5f);
		CancelInvoke ();
	}

	public void OnDied()
	{
		Vector3 pos = new Vector3 (transform.position.x, transform.position.y, 0.01f);
		float xOffset = 0;
		float yOffset = 0;
		for (int i = 0; i < 13; i++) {
			GameObjectPool.instance.ServeBloodSplatter(pos + Vector3.ClampMagnitude(new Vector3(xOffset,yOffset),0.8f));
			xOffset += UnityEngine.Random.Range (-1f,1f);
			yOffset += UnityEngine.Random.Range (-1f,1f);
			
		}

		_onDied ();

		//reverse loop because Notify death modifies this targeterList through ReleaseTarget of targeter
		for (int i = targeterList.Count -1; i >= 0; i--) {
			targeterList[i].NotifyDeath(this);
		}
		
		foreach (var affected in localAffected) {
			affected.UnassignAffected(this);
		}


		if (Network.isServer) {
			networkView.RPC("_onDied", RPCMode.Others);
		}
	}
	

	void TargetPositionReached()
	{

		if (StateAI == BehaviourState.follow) {
			if (HasFollowTarget)
			{
				ITargetable temp = target;
				ReleaseFollowTarget ();

			}
		} else if (HasTarget) {
			Vector2 tVec = target.GetTransform ().position - transform.position;
			if (!InLineOfSight (tVec)) {
				ITargetable temp = target;
				ReleaseTarget ();
				QuickSetTarget (temp, false, tVec);
			} 
		}

	}	
	
	
	bool SearchForTarget()
	{
		if (localAffected.Count == 0) {
			return false;
		}
		List<ITargetable> untestedTargets = new List<ITargetable> ();

		foreach (ITargetable target in localAffected) {
			untestedTargets.Add(target);
		}

		int countOut = 0;

		while (countOut++ < 6) {
			int randIndex = UnityEngine.Random.Range(0, untestedTargets.Count);

			if (TrySetTarget (untestedTargets[randIndex])) {
				return true;
			}
			else {
				untestedTargets.RemoveAt(randIndex);

				if (untestedTargets.Count == 0) {
					return false;
				}
			}
		}

		return false;
		
	}

	bool TrySetTarget(ITargetable target)
	{
		if (SetTarget(target)) {
			return true;
		}
		
		return false;
	}

	bool RevalidateTargetStatus(ITargetable target)
	{
		return (target.IsValid());

	}
	
	bool ValidateTarget(ITargetable target)
	{
		
		return ( target.IsValid() && (target.GetTransform().position - transform.position).sqrMagnitude < 180 * target.GetExposure());
	}
	
	bool RevalidateTarget(ITargetable target)
	{
		return (target.IsValid() && (target.GetTransform().position - transform.position).sqrMagnitude < 210 * target.GetExposure());
	}
	
	bool TryFindTarget(ITargetable target)
	{
		return false;
	}
	
	void TargetRetrack(ITargetable target, float biasStrength, bool biasEnforce)
	{
		if (TryFindTarget(target))
		{
			StateAI = BehaviourState.idleSearch;
		}
		else 
		{
			if (StateAI != BehaviourState.idleWalk)
			{
				Vector2 tVec = target.GetTransform().position - transform.position;
				Vector2 bias;
				if (biasEnforce)
				{
					bias = tVec.normalized*biasStrength;
				}
				else {
					bias = Vector2.ClampMagnitude(tVec,biasStrength);
				}
				RandomWalk(bias);
				HasTarget = false;
			}
		}
	}
	
	public bool QuickSetTarget(ITargetable target, bool inLineOfSight, Vector2 targetVec)
	{
		if ( inLineOfSight)
		{
			AssignTarget(target);

			if (gunComponent.Ammo > 0 && gunComponent.GunType != null)
			{
				StateAI = BehaviourState.stationaryEngage;
			}
			else {
				StateAI = BehaviourState.meleeEngage;
			}
			lastKnownTargetVector = targetVec;
			lastKnownTargetPosition = target.GetTransform().position;
			//lastKnownTargetDirection = target.GetMovingVector();
			return true;
		}
		else
		{

			return false;
		}
	}

	public bool QuickSetTarget(ITargetable target, bool inLineOfSight)
	{
		if ( inLineOfSight)
		{
			AssignTarget(target);
			
			if (gunComponent.Ammo > 0 && gunComponent.GunType != null)
			{
				StateAI = BehaviourState.stationaryEngage;
			}
			else {
				StateAI = BehaviourState.meleeEngage;
			}

			lastKnownTargetPosition = target.GetTransform().position;

			return true;
		}
		else
		{
			return false;
		}
	}
	
	public bool SetTarget(ITargetable target)
	{
		if ((!HasTarget || target != this.target) && ValidateTarget(target)) {
			Vector2 targetVec = target.GetTransform ().position - transform.position;
			
			return QuickSetTarget(target,InLineOfSight(targetVec),targetVec); 
			
		}
		
		return false;
	}

	#region ILocalAffected implementation
	public void NotifyEmigration (ILocalAffector affector)
	{
		localAffected.Remove(affector);
	}
	#endregion

	#region ILocalAffector implementation

	public void AssignAffected (ILocalAffector affected)
	{
		localAffected.Add (affected);
	}

	public void UnassignAffected (ILocalAffector affected)
	{
		localAffected.Remove (affected);
	}

	#endregion
}
