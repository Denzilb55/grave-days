using System;
using UnityEngine;
using System.Collections.Generic;
using GraveDays.Entity.Being.AI;

namespace GraveDays.Entity.Being
{

	public abstract class AiBeing: StandardBeing,  ILocalAffector, ITargeter
	{
		protected Vector2 targetVector;
		private Vector2 localVector;
		protected ITargetable target;
		protected ITargetable attackTarget;
		private bool hasTarget;
		public bool HasAttackTarget { get; private set; }
		private float vectorLerpTime;
		private float vectorLerpAngle;

		public bool HasLineOfSight { get; private set;}
		private bool hasSraightPath;
		private bool isTargetVisible;
		private Vector2 lastKnownTargetVector;
		private Vector2 lastKnownTargetPosition;
		private Vector2 lastKnownTargetDirection;
		private bool moveToLastKnownPosition;

		public const int AVOID_LAYER = (1 << 8) | (1 << 10) | (1 << 13);
		public const int SIGHT_LAYER = (1 << 14);
		private float colliderRadius;

		public IAiNode CurrentState { get; private set; }
		protected IAiNode idleNode;
		protected float rotationLerpFactor = 25f;

		private int searchCount;
		protected int searchCountMax = 4;

		protected CollisionTrackerComponent collisionTracker;
		public static int release;

		
		public AiBeing ()
		{
		}

		protected override void OnAwake() 
		{
			base.OnAwake ();
			//colliderRadius = ((CircleCollider2D)collider2D).radius * 1.2f;
			colliderRadius = ((CircleCollider2D)collider2D).radius * 1.1f;
			SeedBehaviourTree ();
			collisionTracker = gameObject.GetComponentInChildren<CollisionTrackerComponent> ();
		}

		protected override void OnStart() 
		{
			base.OnStart ();

		}

		public void SetCurrentState(IAiNode node)
		{
			if (node != CurrentState) {
				CurrentState = node;
				CurrentState.InitialiseState();
			}
		}

		public IAiNode GetIdleNode() 
		{
			return idleNode;
		}

		public virtual void DoIdleActions()
		{

		}

		protected virtual void OnReflexUpdate()
		{

		}

		public void ReflexUpdate()
		{
			if (stunned) {
				return;
			}

			OnReflexUpdate ();

			if (hasTarget) {
				/*if (target == null) {
					Debug.Log("THIS SHOULD NOT HAPPEN!! FIX");
					hasTarget = false;
				}
				else {*/
				localVector = GetVectorTo(target);
				vectorLerpTime = 0;
				PathSteer ();
				//}
			} else {
				localVector = FacingVector;
			}
		
			UpdateAi ();
		}

		public bool IsStateThreadable()
		{
			return CurrentState.IsStateThreadable ();
		}

		protected override void SpecificUpdate()
		{
			base.SpecificUpdate ();

			if (hasTarget) {
				targetVector = Vector3.Slerp (targetVector, localVector, vectorLerpTime);
				if (vectorLerpTime < 1) {
					vectorLerpTime += Time.deltaTime * rotationLerpFactor;
				}
			}

			if (recalcRotationTransform) {
				float angle = Mathf.Atan2 (FacingVector.y, FacingVector.x) * Mathf.Rad2Deg;
				transform.eulerAngles = new Vector3 (0, 0, angle - 90);
				recalcRotationTransform = false;
			}

		}

		public bool SearchAndHuntTarget()
		{
			/*if (HasAttackTarget) {
				Debug.Log("THIS SHOULD NOT HAPPEN");
				return true;
			}*/

			searchCount++;

			if (searchCount == searchCountMax) {
				searchCount = 0;
				ITargetable tar = SearchForTarget ();

				if (tar != null) {
					if (SetAttackTarget(tar)) {
						return true;
					}
				}
			}

			return false;
		}

		public ITargetable SearchForTarget()
		{
			//List<ILocalAffected> localAffected = aiBeing.GetLocalAffected ();
		//	Debug.Log ("SEARCHING");
			if (localAffected.Count == 0) {
				return null;
			}
			List<ITargetable> untestedTargets = new List<ITargetable> ();
			
			foreach (ITargetable target in localAffected) {
				untestedTargets.Add(target);
			}
			
			int countOut = 0;
			
			while (countOut++ < 4) {
				int randIndex = GetRandom(0, untestedTargets.Count);
				
				if (IsTargetVisible(untestedTargets[randIndex], 200)) {
					
					return untestedTargets[randIndex];
				}
				else {
					untestedTargets.RemoveAt(randIndex);
					
					if (untestedTargets.Count == 0) {
						return null;
					}
				}
			}
			
			return null;
		}

		void PathSteer()
		{
			HasLineOfSight = InLineOfSight (targetVector);
			if (HasLineOfSight) {
				isTargetVisible = (target.GetTransform ().position - transform.position).sqrMagnitude < 200 * target.GetExposure ();
				hasSraightPath = IsUnhindered (renderer.bounds, target.GetTransform ().position);
			} else {
			//	Debug.Log("HAS NOT LOS");
				Invoke("ReleaseTargetIfNotInSight", 10f);
				isTargetVisible = false;
				hasSraightPath = false;
			}
			
			if (hasSraightPath) {
				CancelInvoke("ReleaseTargetIfNotInSight");
				lastKnownTargetVector = targetVector;
				lastKnownTargetPosition = target.GetTransform().position;
				moveToLastKnownPosition = true;
				lastKnownTargetDirection = target.GetMovingVector();
			} else {
				AvoidanceSteer ();
			}
		}

		void ReleaseTargetIfNotInSight()
		{
			if (!HasLineOfSight) {
			//	Debug.Log("RELEASE NOT LOS");
				ReleaseTarget();
				CancelInvoke("ReleaseTargetIfNotInSight");
			}
		}

		void AvoidanceSteer()
		{

			Vector2 localMoveVector = lastKnownTargetVector;
			if (moveToLastKnownPosition) {
				Vector2 temp = lastKnownTargetPosition - (Vector2)transform.position;
				Invoke("ReleaseTargetIfNotInSight", 6f);

				if (temp.sqrMagnitude > 1) {
					localMoveVector = temp;

				}
				else {

					moveToLastKnownPosition = false;
					lastKnownTargetVector = lastKnownTargetDirection;
					Invoke("ReleaseTargetIfNotInSight", 3f);
				}

			}

			int hit = CornerCastHit (renderer.bounds, localMoveVector, colliderRadius *1.4f, AVOID_LAYER);
			
			int avoidCountOut = 0;
			int angleCount = 0;
			int lastHit = 0;
			
			Vector2 rotateVector = localMoveVector;
			Vector2 lastRotateVector = rotateVector;
			int dir=0;
			while ((avoidCountOut < 12 && hit != 0)) {
				avoidCountOut++;
				
				if (avoidCountOut % 2 == 0) {
					angleCount++;
					dir = 1;
				} else {
					dir = -1;
				}
				
				lastRotateVector = rotateVector;
				rotateVector = localMoveVector.Rotate (dir * angleCount * 21);
				lastHit = hit;
				hit = CornerCastHit (renderer.bounds, rotateVector, colliderRadius *1.4f, AVOID_LAYER);
			}
			//Debug.Log("ROTATED");
			localVector = rotateVector;
		}

		int CornerCastHit(Bounds bounds, Vector2 direction, float distance, int layer)
		{
			Vector2 thisPos = ((CircleCollider2D) collider2D).center + (Vector2)transform.position;
			Vector2 vecToTarget = (direction.normalized)*colliderRadius;
			
			Vector2 [] p = new Vector2[3];
			Vector2 edge1 = vecToTarget.Rotate (90) + thisPos;
			Vector2 edge2 = vecToTarget.Rotate (-90) + thisPos;
			p [0] = (Vector2)transform.position;
			p [1] = edge1;
			p [2] = edge2;
			
			
			
			int c = 0;
			for (int i = 0; i < 3; i++) {
				if (Physics2D.Raycast (p[i], direction, distance, layer))
				{
					c++;
				}
			}
			
			
			return c;
			
		}
		
		bool IsUnhindered(Bounds bounds, Vector2 targetPos)
		{
			Vector2 thisPos = ((CircleCollider2D) collider2D).center + (Vector2)transform.position;
			Vector2 vecToTarget = ((targetPos - thisPos).normalized)*colliderRadius;
			
			Vector2 [] p = new Vector2[3];
			Vector2 edge1 = vecToTarget.Rotate (90) + thisPos;
			Vector2 edge2 = vecToTarget.Rotate (-90) + thisPos;
			p [0] = (Vector2)transform.position;
			p [1] = edge1;
			p [2] = edge2;
			
			
			int c = 0;
			for (int i = 0; i < 3; i++) {
				
				Vector2 pointVector = targetPos - p[i];
				
				RaycastHit2D hit = Physics2D.Raycast (p[i], pointVector, pointVector.magnitude, AVOID_LAYER);
				if (hit.transform == null)
				{
					c++;
				}
				
				
			}
			
			return c >= 3;
			
		}
		
		bool IsUnhindered(Bounds bounds, Vector2 targetPos, float range)
		{
			Vector2 thisPos = ((CircleCollider2D) collider2D).center + (Vector2)transform.position;
			Vector2 vecToTarget = ((targetPos - thisPos).normalized)*colliderRadius;
			
			Vector2 [] p = new Vector2[3];
			Vector2 edge1 = vecToTarget.Rotate (90) + thisPos;
			Vector2 edge2 = vecToTarget.Rotate (-90) + thisPos;
			p [0] = (Vector2)transform.position;
			p [1] = edge1;
			p [2] = edge2;
			
			int c = 0;
			for (int i = 0; i < 3; i++) {
				
				Vector2 pointVector = targetPos - p[i];
				
				RaycastHit2D hit = Physics2D.Raycast (p[i], pointVector, range, AVOID_LAYER);
				if (hit.transform == null)
				{
					c++;
				}
			}
			
			//	UnityEngine.Debug.Log (c);
			return c >= 3;
		}
		
		bool InLineOfSight(Vector2 targetVec)
		{
			RaycastHit2D hit = Physics2D.Raycast (transform.position, targetVec, targetVec.magnitude, SIGHT_LAYER);
			return (hit.transform == null);

		}

		public bool IsTargetExposed(ITargetable tar, int visibilityFactor)
		{
			Vector2 vecToTarget = tar.GetTransform ().position - transform.position;
			return vecToTarget.sqrMagnitude < visibilityFactor * tar.GetExposure ();
		}

		public bool IsTargetVisible(ITargetable tar, int visibilityFactor)
		{
			Vector2 vecToTarget = tar.GetTransform ().position - transform.position;
			return IsTargetExposed(tar,visibilityFactor) && InLineOfSight (vecToTarget);
		}

		public bool IsTargetVisible()
		{
			return isTargetVisible;
		}

		public ITargetable GetTarget()
		{
			if (hasTarget) {
				return target;
			} else {
				return null;
			}
		}

		#region IAiControlled implementation

		public Vector2 GetTargetVector ()
		{
			return targetVector;
		}

		public void MoveInDirection(Vector2 moveDirection)
		{
			movementModule.Move (moveDirection);
			FacingVector = moveDirection;
			//FacingVector = lastKnownTargetVector;

		}

		public List<ILocalAffector> GetLocalAffected()
		{
			return localAffected;
		}
		#endregion

		#region ITargeter implementation

		public void NotifyDeath (ITargetable target)
		{
			ReleaseAttackTarget (target);
			ReleaseTarget (target);
			collisionTracker.RemoveCollision (target);
			//}
		}

		public float GetDotToTarget()
		{
			return Vector2.Dot (FacingVector, targetVector);
		}

		#endregion

		bool ValidateTarget(ITargetable target)
		{
			return (target.IsValid() && (target.GetTransform().position - transform.position).sqrMagnitude < 200 * target.GetExposure());
		}


		protected override void OnHitByMelee(ITargetable hitter)
		{
			base.OnHitByMelee (hitter);
			/*if (HasAttackTarget) {
				if ((attackTarget.GetTransform().position - transform.position).sqrMagnitude > 0.8) {
					return;
				}
			}

			SetAttackTarget(hitter);*/
		}


		/*public bool SetTarget(ITargetable target)
		{
			if ((!HasTarget || target != this.target) && ValidateTarget(target)) {
				Vector2 targetVec = target.GetTransform ().position - transform.position;
				
				return QuickSetTarget(target,InLineOfSight(targetVec),targetVec); 
				
			}
			
			return false;
		}*/

		public bool SetAttackTarget(ITargetable target)
		{
			if (attackTarget == target) {
				return true;
			}

			ReleaseAttackTarget(target);

			if (SetTarget (target)) {
				HasAttackTarget = true;
				attackTarget = target;
				OnAquireAttackTarget();
				return true;
			}

			return false;
		}

		protected virtual void OnAquireAttackTarget()
		{

		}

		public ITargetable GetAttackTarget()
		{
			return attackTarget;
		}

		public virtual bool SetTarget(ITargetable target)
		{
		//	if (this is SurvivorBeing)
		//		Debug.Log ("SET TARGET" + Time.frameCount);

			/*if (target is HeroBeing && this is SurvivorBeing) {
				Debug.Log("WHY" + Time.frameCount);
			}*/

			if (hasTarget && target != this.target) {
				ReleaseTarget();
			}

			if (!hasTarget) {
				Vector2 targetVec = target.GetTransform ().position - transform.position;
				AssignTarget(target);

				lastKnownTargetVector = targetVec;
				lastKnownTargetPosition = target.GetTransform().position;
				moveToLastKnownPosition = true;
				lastKnownTargetDirection = target.GetMovingVector();

				return true;
				//return QuickSetTarget(target,InLineOfSight(targetVec),targetVec); 
				
			}
			
			return false;
		}

		public void AssignTarget(ITargetable target)
		{
			this.target = target;
			hasTarget = true;
			target.AssignTargeter (this);
		}

		public override void UnassignAffected(ILocalAffector affected)
		{
			base.UnassignAffected (affected);
			ReleaseAttackTarget (affected);
			ReleaseTarget (affected);
		}

		protected virtual void OnReleaseTarget()
		{

		}

		public void ReleaseAttackTarget(ITargetable tar)
		{
			if (tar == attackTarget) {
				ReleaseAttackTarget();
			}
		}

		public void ReleaseAttackTarget()
		{
			/*if (this is SurvivorBeing) {
				Debug.Log ("RELEASe TARGET" + Time.frameCount +" ID " + ((ZombieBeing)attackTarget).ID);
			}

			if (this is ZombieBeing) {
				Debug.Log("ERRROROROROR");
			}*/
			//release++;
			//Debug.Log ("RELEASEATTACK");
			if ( HasAttackTarget) {
			//	Debug.Log ("RELEASEATTACK2");
				if (!hasTarget || target != attackTarget) {
					attackTarget.UnassignTargeter(this);
				}
				/*if (this is SurvivorBeing) {
					Debug.Log ("PART2" + Time.frameCount +" ID " + ((ZombieBeing)attackTarget).ID);
				}*/

				ReleaseTarget(attackTarget);
				attackTarget = null;
				HasAttackTarget = false;
			}
		}

		public void ReleaseTarget()
		{
			//release++;
			//Debug.Log ("RELEASE2");
			if (hasTarget) {
				/*if (this is SurvivorBeing ) {
					if (target is ZombieBeing) {
					Debug.Log ("PART3" + Time.frameCount +" ID " + ((ZombieBeing)target).ID);
					} else {
						Debug.Log ("PART3" + Time.frameCount );
					}
				}*/
			//	Debug.Log ("RELEASE3");
				OnReleaseTarget();
				if (!HasAttackTarget || target != attackTarget) {
					target.UnassignTargeter(this);
				}
				hasTarget = false;
				target = null;
			}

		}

		public void ReleaseTarget(ITargetable tar)
		{
		//	release++;
		//	Debug.Log ("RELEASE1");
			if (tar == target) {
				ReleaseTarget();
			}
			
		}

		public virtual void DoUnspawn()
		{

		}

		protected override void OnStun()
		{
			StopMovement ();
		}

		public bool HasTarget()
		{
			return hasTarget;
		}
		
		protected abstract void SeedBehaviourTree ();		


		public bool TargetWithinUnitRange()
		{
			return (GetTargetVector ().sqrMagnitude < 1);
		}


		public void ResetToIdle()
		{
		//	animatorModule.UpdateAnimationState ();
			SetCurrentState(idleNode);
		}
		
		public void UpdateAi()
		{
			IAiNode node = CurrentState.UpdateAi ();
			if (node == null)
				Debug.Log (CurrentState.ToString ());

				SetCurrentState (node);
		}

	}
}

