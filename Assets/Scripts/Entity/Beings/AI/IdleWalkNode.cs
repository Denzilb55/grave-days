using System;
using UnityEngine;

namespace GraveDays.Entity.Being.AI
{
	public class IdleWalkNode<T>: BaseAiNode<T> where T : AiBeing
	{
		private Vector2 walkTarget;
		private IdleNode<T> idleNode;
		private int countOut;

		public IdleWalkNode (T aiComponent, IdleNode<T> idleNode): base(aiComponent)
		{
			this.idleNode = idleNode;
		}

		public void SetWalkTarget(Vector2 walkTarget)
		{
			this.walkTarget = walkTarget;
			countOut = 0;
		}

	
		#region implemented abstract members of BaseAiNode
		public override IAiNode UpdateAi ()
		{
			//Debug.Log ("IS WALKING" + countOut + " " + 60*4/ZombieBeing.FRAME_TICK);
			if (aiBeing.HasTarget () || countOut++ > 60*4/ZombieBeing.FRAME_TICK) {
				return idleNode;
			}

			aiBeing.SearchAndHuntTarget ();
			Vector2 targetVector = walkTarget - (Vector2)aiBeing.transform.position;

			aiBeing.MoveInDirection (targetVector.normalized);

			if (targetVector.sqrMagnitude < 1) {
				aiBeing.StopMovement();
				return idleNode;
			}

			return this;
		}

		public override bool IsStateThreadable()
		{
			return false;
		}

		public override string ToString ()
		{
			return "Idle Walking";
		}
		#endregion
	}
}

