using System;
using UnityEngine;

namespace GraveDays.Entity.Being.AI
{
	public class MeleeAttackNode<T>: ChaseNode<T> where T : AiBeing
	{

		public MeleeAttackNode (T aiBeing): base(aiBeing)
		{
	
		}
		
		public override IAiNode EngageTargetActions()
		{

			Vector2 targetVector = aiBeing.GetTargetVector();
			if (targetVector.sqrMagnitude < 1.3*1.3) {
				if (aiBeing.meleeComponent.Attack(targetVector)) {
					aiBeing.animatorModule.DoPunch();
				}
			}
			return base.EngageTargetActions();

		}
		
		public override string ToString ()
		{
			return "Melee-Attack";
		}
		
	}
}

