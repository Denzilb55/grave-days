using System;
using UnityEngine;

namespace GraveDays.Entity.Being.AI
{
	public class StationaryShootNode <T> : BaseAiNode<T> where T : SurvivorBeing
	{
		private IAiNode returnNode;

		public StationaryShootNode (T aiBeing): base(aiBeing)
		{

		}

		public override void InitialiseState()
		{
			aiBeing.StopMovement ();
		}

		public void SetReturnNode(IAiNode returnNode)
		{
			this.returnNode = returnNode;
		}

		#region implemented abstract members of BaseAiNode
		public override IAiNode UpdateAi ()
		{
			if (!aiBeing.HasAttackTarget) {
				return returnNode;
			}

			Transform targetTransform = aiBeing.GetAttackTarget ().GetTransform ();
			Vector2 vec = targetTransform.position - aiBeing.transform.position;
			vec += targetTransform.rigidbody2D.velocity * vec.magnitude * aiBeing.GetRandom (20, 35) * 0.001f;

			aiBeing.FacingVector = vec.normalized;
			if (aiBeing.CanShoot ()) {

				if (aiBeing.gunComponent.Shoot (aiBeing.FacingVector)) {
					return returnNode;
				}
				return this;
			} else {
				return returnNode;
			}
		}

		public override bool IsStateThreadable()
		{
			return false;
		}

		public override string ToString ()
		{
			return "Stationary shooting";
		}
		#endregion
	}
}

