using System;
using UnityEngine;

namespace GraveDays.Entity.Being.AI
{
	public class ZombieChaseNode<T>: ChaseNode<T> where T : ZombieBeing 
	{
		public ZombieChaseNode (T aiComponent): base(aiComponent)
		{
		}

		public override IAiNode UpdateAi ()
		{
			IAiNode baseReturn = base.UpdateAi ();
			if (baseReturn == this) {
				if (aiBeing.GetRandom(500) == 0) {
					aiBeing.AlertGrunt();
				}
			}

			return baseReturn;
		}

		public override IAiNode EngageTargetActions()
		{
			
			Vector2 targetVector = aiBeing.GetTargetVector();
			if (targetVector.sqrMagnitude < 1.3*1.3) {
				if (aiBeing.meleeComponent.Attack(targetVector)) {
					aiBeing.animatorModule.DoPunch();
				}
			}
			return base.EngageTargetActions();
			
		}
	}
}

