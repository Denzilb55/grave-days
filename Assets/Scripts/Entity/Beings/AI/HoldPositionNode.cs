using System;
using UnityEngine;

namespace GraveDays.Entity.Being.AI
{
	public class HoldPositionNode<T>: BaseAiNode<T> where T : AiBeing
	{
		protected ChaseNode<T> chaseNode;
		protected Vector2 holdPos;
		protected bool hasHoldPosition;
		
		public HoldPositionNode(T aiComponent, ChaseNode<T> chaseNode): base(aiComponent)
		{
			this.chaseNode = chaseNode;
		}
		
		public override void InitialiseState()
		{
		//	aiBeing.animatorModule.UpdateAnimationState();
		}

		public void SetHoldPosition(Vector2 pos)
		{
			holdPos = pos;
		}
		
		#region implemented abstract members of BaseAiNode
		
		public override IAiNode UpdateAi ()
		{


			if (aiBeing.HasTarget ()) {
				Debug.Log("HER1E");
				chaseNode.SetReturnNode(this);
				return chaseNode;
			}
			
			if (aiBeing.SearchAndHuntTarget ()) {
				chaseNode.SetReturnNode(this);
				return chaseNode;
			}

			Vector2 v2p = holdPos - (Vector2)aiBeing.transform.position;
			if ((v2p).sqrMagnitude > 1) {
				aiBeing.MoveInDirection(v2p);
			} else {
				Debug.Log("HERE");
				aiBeing.StopMovement();
			}
			//aiBeing.DoIdleActions ();
			
			return this;
		}

		public override bool IsStateThreadable()
		{
			return false;
		}

		public override string ToString ()
		{
			return "Holding position";
		}
		
		#endregion
	}
}

