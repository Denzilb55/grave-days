using System;
using System.Collections.Generic;

namespace GraveDays.Entity.Being.AI
{
	public enum BehaviourState {idleStand = 0, idleWalk, idleSearch, meleeEngage, avoid, stationaryEngage, follow, holdPosition }
	
	public abstract class BaseAiNode <T> : IAiNode where T : AiBeing
	{
		protected T aiBeing;

		public BaseAiNode (T aiBeing)
		{
			this.aiBeing = aiBeing;
		}

		public abstract IAiNode UpdateAi();

		public override string ToString ()
		{
			return "BASE AI - NEED TO OVERRIDE: " + base.ToString();
		}

		public virtual void InitialiseState()
		{

		}

		public abstract bool IsStateThreadable();
	}
}

