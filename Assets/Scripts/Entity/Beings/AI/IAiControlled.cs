using System;
using System.Collections.Generic;
using UnityEngine;

namespace GraveDays.Entity.Being.AI
{
	public interface IAiControlled
	{
		Vector2 GetTargetVector();
		float GetDotToTarget();
		bool SetTarget(ITargetable target);
		bool IsTargetVisible();
		bool IsTargetVisible(ITargetable target);
		void MoveInDirection(Vector2 moveDirection);
		bool HasTarget();
		List<ILocalAffector> GetLocalAffected();
	}
}

