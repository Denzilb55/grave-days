using System;
using UnityEngine;

namespace GraveDays.Entity.Being.AI
{
	public class ChaseNode<T>: BaseAiNode<T> where T : AiBeing
	{
		private IAiNode returnNode;
		private int checkTarget;

		public ChaseNode (T aiBeing): base(aiBeing)
		{

		}

		#region implemented abstract members of BaseAiNode

		public override void InitialiseState()
		{
		//	aiBeing.animatorModule.UpdateAnimationState();
		}

		public void SetReturnNode(IAiNode node)
		{
			returnNode = node;
		}

		public void SetDefaultReturnNode()
		{
			returnNode = aiBeing.GetIdleNode ();
		}

		public virtual IAiNode EngageTargetActions()
		{
			Vector2 targetDirection = aiBeing.GetTargetVector();
			aiBeing.MoveInDirection(targetDirection);
			return this;
		}

		public override IAiNode UpdateAi ()
		{
			if (aiBeing.HasAttackTarget) {
				if (checkTarget++ > 6) {
					ITargetable attackTarget = aiBeing.GetAttackTarget();
					if (!aiBeing.IsTargetExposed(attackTarget, 250)) {
						aiBeing.movementModule.Stop ();
						checkTarget = 0;
						aiBeing.ReleaseAttackTarget(attackTarget);
					//	if (aiBeing is SurvivorBeing) Debug.Log("RETURN 1");
						return returnNode;
					}
				}
				return EngageTargetActions();
			} else {
				aiBeing.movementModule.Stop ();
			//	if (aiBeing is SurvivorBeing) Debug.Log("RETURN 2");
				return returnNode;
			}
		}

		public override bool IsStateThreadable()
		{
			return true;
		}

		public override string ToString ()
		{
			return "Chase Node";
		}

		#endregion
	}
}

