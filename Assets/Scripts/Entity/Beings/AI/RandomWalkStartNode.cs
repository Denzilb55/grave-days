using System;
using UnityEngine;

namespace GraveDays.Entity.Being.AI
{
	public class RandomWalkStartNode<T>: BaseAiNode<T> where T : AiBeing 
	{
		protected IdleNode<T> idleNode;
		protected IdleWalkNode<T> walkNode;
		private const int WALK_DISTANCE = 12;

		public RandomWalkStartNode (T aiComponent, IdleNode<T> idleNode): base(aiComponent)
		{
			this.idleNode = idleNode;
			walkNode = new IdleWalkNode<T> (aiComponent, idleNode);
		}

		public override void InitialiseState()
		{
		
			//aiBeing.animatorModule.UpdateAnimationState();
		}

		#region implemented abstract members of BaseAiNode

		public override IAiNode UpdateAi ()
		{
			Vector2 v = aiBeing.GetRandomInCircleCentered(WALK_DISTANCE);
			walkNode.SetWalkTarget (v);
			//aiBeing.animatorModule.UpdateAnimationState ();
			return walkNode;
		}

		public override bool IsStateThreadable()
		{
			return false;
		}

		public override string ToString ()
		{
			return "Start random walk";
		}

		#endregion
	}
}

