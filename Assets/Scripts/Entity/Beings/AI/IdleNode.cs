using System;
using UnityEngine;

namespace GraveDays.Entity.Being.AI
{
	public class IdleNode<T>: BaseAiNode<T> where T : AiBeing
	{
		protected RandomWalkStartNode<T> walkNode;
		protected ChaseNode<T> chaseNode;

		public IdleNode(T aiComponent, ChaseNode<T> chaseNode): base(aiComponent)
		{
			walkNode = new RandomWalkStartNode<T> (aiComponent, this);
			this.chaseNode = chaseNode;
		}

		public override void InitialiseState()
		{
		//	aiBeing.RunSubTree (SubTreeType.Searching);
			//aiBeing.animatorModule.UpdateAnimationState();
		}

		#region implemented abstract members of BaseAiNode

		public override IAiNode UpdateAi ()
		{
			//Debug.Log ("IS IDLE");
			if (aiBeing.HasAttackTarget) {
			//	aiBeing.animatorModule.SetAnimationState (true);
				return chaseNode;
			}
		
			aiBeing.SearchAndHuntTarget ();
			aiBeing.DoIdleActions ();

			if (aiBeing.FlipWarpedCoin(1,60 * 5)) {
				return walkNode;
			}

			return this;
		}

		public override bool IsStateThreadable()
		{
			return false;
		}


		//protected virtual bool 

		public override string ToString ()
		{
			return "Idle";
		}

		#endregion
	}
}

