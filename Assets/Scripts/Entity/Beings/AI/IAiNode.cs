using System;
namespace GraveDays.Entity.Being.AI
{
	public interface IAiNode
	{
		IAiNode UpdateAi();
		void InitialiseState();
		bool IsStateThreadable();
	}
}

