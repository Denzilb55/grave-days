using System;
using UnityEngine;

namespace GraveDays.Entity.Being.AI
{
	public class FollowTargetNode<T>: BaseAiNode<T> where T : AiBeing
	{
		public ITargetable FollowTarget { get; private set; }

		public bool HasFollowTarget { get; private set; }
		private ChaseNode<T> chaseNode;

		public FollowTargetNode  (T aiBeing, ChaseNode<T> chaseNode): base(aiBeing)
		{
			this.chaseNode = chaseNode;
		}

		public override void InitialiseState()
		{
			aiBeing.SetTarget(FollowTarget);
		}

		public void SetFollowTarget(ITargetable target)
		{
			FollowTarget = target;
			HasFollowTarget = true;

		}

		public void FreeFollowTarget()
		{
			HasFollowTarget = false;
		}

		#region implemented abstract members of BaseAiNode
		public override IAiNode UpdateAi ()
		{
			if (aiBeing.SearchAndHuntTarget ()) {
				chaseNode.SetReturnNode(this);
				return chaseNode;
			}

			if (HasFollowTarget) {
				aiBeing.MoveInDirection(aiBeing.GetTargetVector());

				return this;
			} else {
				return aiBeing.GetIdleNode();
			}
		}

		public override bool IsStateThreadable()
		{
			return false;
		}

		public override string ToString ()
		{
			return "Following";
		}
		#endregion
	}
}

