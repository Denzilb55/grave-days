using System;
using UnityEngine;

namespace GraveDays.Entity.Being.AI
{
	public class EngageTargetNode<T>: ChaseNode<T> where T : SurvivorBeing
	{
		private IAiNode shootNode;

		public EngageTargetNode (T aiBeing, StationaryShootNode<T> shootNode): base(aiBeing)
		{
			this.shootNode = shootNode;
		}

		public override IAiNode EngageTargetActions()
		{
			if (aiBeing.CanShoot () ) {
				return shootNode;
			} else {
				Vector2 targetVector = aiBeing.GetTargetVector();
				if (targetVector.sqrMagnitude < 1.3*1.3) {
					if (aiBeing.meleeComponent.Attack(targetVector)) {
						aiBeing.animatorModule.DoPunch();
					}
				}
				return base.EngageTargetActions();
			}

		}

		public override string ToString ()
		{
			return "Engaging";
		}
		
	}
}

