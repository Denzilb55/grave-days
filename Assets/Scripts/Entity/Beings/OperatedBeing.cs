using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

using GraveDays.Animation;
using GraveDays.Items;
using GraveDays.Items.Guns;

namespace GraveDays.Entity.Being
{
	public abstract class OperatedBeing: StandardBeing, IOptioned, IGunner, IEquippor
	{
		public Light2D Torch;
		private Color torchColor = new Color(231/255f,220/255f, 207/255f,1);
		protected bool torchEnabled = false;

		public GunComponent gunComponent;
		public EquipComponent equipComponent;
		public InventoryComponent inventory;	

		public bool melee = false;

		private Vector2 netInterpVec;
		private float interpVal;
		private bool interpEnable;
		
		private float _torchRadius;
		public float TorchRadius {
			get
			{
				return _torchRadius;
			}
			set
			{
				_torchRadius = value;
				
				if (torchEnabled)
				{
					Torch.LightRadius = _torchRadius;
				}
			}
			
		}

		public OperatedBeing ()
		{
		}

		protected override void OnAwake()
		{
			base.OnAwake ();

			gunComponent = GetComponent<GunComponent> ();
			equipComponent = GetComponent<EquipComponent> ();
			inventory = GetComponent<InventoryComponent> ();

			if (inventory == null) {
				inventory = gameObject.AddComponent<InventoryComponent>();
			}

			inventory.maxCarryWeight = 100;
			melee = true;

			torchOffFunction ();
		}

		public void AddAndEquip(ItemEquippable item, int amount = 1)
		{
			equipComponent.Equip (item);
			AddToBag (item.GetItemType(), amount);
		}
		
		public void AddToBag(ItemType item, int amount = 1)
		{
			inventory.AddItem (item, amount);
			if (ManagerScript.networkType != NetworkType.Single) {
				networkView.RPC("RPCAddItem", RPCMode.Others, (int) item, amount);
			}
		}
		
		public void RemoveFromBag(ItemType item, int amount = 1)
		{
			inventory.RemoveItem (item, amount);
			if (ManagerScript.networkType != NetworkType.Single) {
				networkView.RPC("RPCRemoveItem", RPCMode.Others, (int) item, amount);
			}
		}
		
		[RPC]
		void RPCAddItem(int itemIndex, int amount)
		{
			inventory.AddItem ((ItemType)itemIndex, amount);
		}
		
		[RPC]
		void RPCRemoveItem(int itemIndex, int amount)
		{
			inventory.RemoveItem ((ItemType)itemIndex, amount);
		}

		public override AnimationDecorator GetAnimationDecorator()
		{
			if (gunComponent.GunType == null || melee)
			{
				return AnimationDecorator.Unarmed;
			}
			else {
				return gunComponent.GunType.GetAnimationDecorator();
			}
		}
				
		protected override void NetworkSerializeRead(BitStream stream, NetworkMessageInfo info) 
		{
			float x = 0;
			float y = 0;
			float xVel = 0;
			float yVel = 0;
			float xFace = 0;
			float yFace = 0;
			
			stream.Serialize(ref x);
			stream.Serialize(ref y);
			stream.Serialize(ref xVel);
			stream.Serialize(ref yVel);
			stream.Serialize(ref xFace);
			stream.Serialize(ref yFace);
			//stream.Serialize (ref exposure);
			
			netInterpVec = new Vector2(x, y);
			interpVal = 0;
			MovingVector = new Vector2(xVel, yVel);
			FacingVector = new Vector2(xFace, yFace);
			
			if (!interpEnable)
			{
				interpEnable = true;
				transform.position = netInterpVec;
			}
		}

		#region IEquippor implementation
		public void EquipAction (ItemShoes shoes)
		{
			movementModule.AddMovementModifier (shoes.MovementModifier);
		}
		public void UnequipAction (ItemShoes shoes)
		{
			movementModule.AddMovementModifier (-shoes.MovementModifier);
		}
		
		public void EquipAction (ItemGun gun)
		{
			melee = false;
		}
		
		public void UnequipAction (ItemGun gun)
		{
			melee = true;
		}
		#endregion	

		#region IGunner implementation
		public void AssignBullet(BulletBehaviour bullet)
		{
			gunComponent.AssignBullet (bullet);
		}
		#endregion	

		protected void torchOnFunction()
		{
			Torch.LightColor = torchColor;
			Torch.LightRadius = TorchRadius;
			torchEnabled = true;
		}
		
		protected void torchOffFunction()
		{
			Torch.LightColor = Color.black;
			Torch.LightRadius = 0.01f;
			torchEnabled = false;
		}

		//INVENTORY GUI
		public bool MouseOnGui;
		public Vector2 scrollPosition1 = Vector2.zero;
		
		public virtual void DrawMenu()
		{
			
		}
		
		public bool IsMouseOver()
		{
			return MouseOnGui;
		}
		
		void IOptioned.HandleInputs()
		{
			
		}
		
		public void NotifyClose()
		{
			
		}
	}
}

