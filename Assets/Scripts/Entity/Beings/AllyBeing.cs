using System;
using UnityEngine;


namespace GraveDays.Entity.Being
{
	public class AllyBeing: OperatedBeing
	{
		public AllyBeing ()
		{
		}

		#region implemented abstract members of StandardBeing

		public override void OnDied ()
		{
			networkView.RPC ("_onDied", RPCMode.All);
		}

		#endregion
	}
}

