using System;
using System.Collections.Generic;
using UnityEngine;
using GraveDays.Entity.Being.AI;
using GraveDays.Items;
using GraveDays.Items.Guns;
using GraveDays.Animation;

namespace GraveDays.Entity.Being
{
	public class SurvivorBeing: AiBeing, IEquippor
	{
		public GunComponent gunComponent;
		protected EquipComponent equipComponent;
		public InventoryComponent inventory;
		public SurvivorSpeechComponent speechComponent;
		private ZombieScanner zombieScanner;
		public bool isMale;
		public bool isHungry;

		public ITargetable followTarget;
		public bool HasFollowTarget;
		private bool HasFollowTargetMem;
		private bool IsHoldingPosition;

		private FollowTargetNode<SurvivorBeing> followNode;
		private HoldPositionNode<SurvivorBeing> holdPositionNode;
		private EngageTargetNode<SurvivorBeing> engageNode;

		public int ID;
		static int IDs;

		public SurvivorBeing ()
		{

		}

		protected override void OnAwake()
		{
			base.OnAwake ();
			rotationLerpFactor = 40f;
			ID = IDs++;

			gunComponent = GetComponent<GunComponent> ();
			equipComponent = GetComponent<EquipComponent> ();
			inventory = GetComponent<InventoryComponent> ();
			zombieScanner = GetComponentInChildren <ZombieScanner> ();
			isMale = true;
			
			if (isMale) {
				speechComponent = gameObject.AddComponent<SurvivorSpeechMaleComponent>();
				animatorModule = gameObject.AddComponent<AnimatorJoe>();
			} else {
				speechComponent = gameObject.AddComponent<SurvivorSpeechFemaleComponent>();
				animatorModule = gameObject.AddComponent<AnimatorJane>();
			}
			
			ManagerScript.instance.survivors.Add (this);



		}

		protected override void OnStart () 
		{
			base.OnStart ();

			movementModule.SetBaseSpeed (12);
			collisionTracker.AddEnemyTag ("Zombie");

			if (!Network.isClient) {
				if (UnityEngine.Random.Range (0, 15) == 0) {
					AddAndEquip (ItemGun.Glock);
					AddToBag (ItemType.Ammo, UnityEngine.Random.Range (6, 20));
				}
				else if (UnityEngine.Random.Range(0,120) == 0 )
				{
					AddAndEquip (ItemType.GunShotgun.GetItemInstance() as ItemEquippable);
					AddToBag (ItemType.Ammo, 30);
				} else if (UnityEngine.Random.Range(0,120) == 0 ){
					AddAndEquip (ItemType.GunAK.GetItemInstance() as ItemEquippable);
					AddToBag (ItemType.Ammo, 30);
				}

				AddToBag (ItemType.Food, UnityEngine.Random.Range (0, 3));
				AddToBag (ItemType.Bandages, UnityEngine.Random.Range (0, 6));

				if (UnityEngine.Random.Range(0,40) == 1) {
					AddToBag (ItemType.AntiViral);
				}
				else if (UnityEngine.Random.Range(0,20) == 1) {
					AddToBag (ItemType.Adrenaline);
				}

			}
		}

		[RPC]
		void _OnDied()
		{
			
			//SoundPlayer.instance.Play (Sounds.playerDeath, transform, 8);
			speechComponent.SayDeath ();
			


			CancelInvoke ();
			Invoke ("FinaliseDeath", 1.5f);
		//	Destroy (gameObject, 1.5f);
		}
		
		public override void OnDied()
		{
			Vector3 pos = new Vector3 (transform.position.x, transform.position.y, 0.01f);
			float xOffset = 0;
			float yOffset = 0;
			for (int i = 0; i < 13; i++) {
				GameObjectPool.instance.ServeBloodSplatter(pos + Vector3.ClampMagnitude(new Vector3(xOffset,yOffset),0.8f));
				xOffset += UnityEngine.Random.Range (-1f,1f);
				yOffset += UnityEngine.Random.Range (-1f,1f);
				
			}
			

			
			//reverse loop because Notify death modifies this targeterList through ReleaseTarget of targeter
			for (int i = targeterList.Count -1; i >= 0; i--) {
				targeterList[i].NotifyDeath(this);
			}
			
			foreach (var affected in localAffected) {
				affected.UnassignAffected(this);
			}
			
			
			if (Network.isServer) {
				networkView.RPC("_OnDied", RPCMode.Others);
			}

			_OnDied ();


		}

		void FinaliseDeath()
		{
			ManagerScript.instance.survivors.Remove (this);
			if (effectsComponent.IsEffected (EffectsType.Infected)) {
				ManagerScript.instance.SpawnZombie(transform.position);
			}
			Destroy (gameObject);
		}

		#region implemented abstract members of AiBeing

		protected override void SeedBehaviourTree ()
		{
			StationaryShootNode<SurvivorBeing> shootNode = new StationaryShootNode<SurvivorBeing> (this);

			engageNode = new EngageTargetNode<SurvivorBeing>(this, shootNode);
			idleNode = new IdleNode<SurvivorBeing> (this, engageNode);
			engageNode.SetDefaultReturnNode ();
			SetCurrentState (idleNode);

			shootNode.SetReturnNode (engageNode);
			followNode = new FollowTargetNode<SurvivorBeing> (this, engageNode);
			//IAiNode searchNode = new SearchNode<SurvivorBeing> (this, currentState);

			holdPositionNode = new HoldPositionNode<SurvivorBeing> (this, engageNode);
		}

		#endregion

		public bool CanShoot()
		{
			return (gunComponent.Ammo > 0 && gunComponent.GunType != null);
		}

		public void AddAndEquip(ItemEquippable item, int amount = 1)
		{
			equipComponent.Equip (item);
			AddToBag (item.GetItemType(), amount);
		//	animatorModule.UpdateAnimationState ();
		}
		
		public void AddToBag(ItemType item, int amount = 1)
		{
			inventory.AddItem (item, amount);
			if (ManagerScript.networkType != NetworkType.Single) {
				networkView.RPC("RPCAddItem", RPCMode.Others, (int) item, amount);
			}
		}
		
		public void RemoveFromBag(ItemType item, int amount = 1)
		{
			inventory.RemoveItem (item, amount);
			if (ManagerScript.networkType != NetworkType.Single) {
				networkView.RPC("RPCRemoveItem", RPCMode.Others, (int) item, amount);
			}
		}

		public bool IsEquipped(ItemEquippable item)
		{
			return equipComponent.IsEquipped (item);
		}


		public void Unequip(ItemEquippable item)
		{
			if (equipComponent.IsEquipped (item)) {
				equipComponent.Unequip(item);
			}
		}

		public void Equip(ItemEquippable item)
		{
			if (!equipComponent.IsEquipped (item.GetEquipType())) {
				equipComponent.Equip(item);
			}
		}


		protected override void OnReflexUpdate()
		{
			base.OnReflexUpdate ();

			if (inventory.GetItemCount (ItemType.Bandages) > 0) {
				if (healthComponent.Health < healthComponent.MaxHealth * 0.8f) {
					inventory.RemoveItem(ItemType.Bandages);
					healthComponent.RecoverHealth(8);
					if (effectsComponent.IsEffected(EffectsType.Bleeding)) {
						effectsComponent.ReduceBleed(1);
					}
				}
			}
			
			if (effectsComponent.IsEffected (EffectsType.Fever)) {
				if (inventory.GetItemCount(ItemType.AntiViral) > 0) {
					inventory.RemoveItem(ItemType.AntiViral);
					effectsComponent.RemoveEffect(EffectsType.Infected);
				}
				else if (inventory.GetItemCount(ItemType.Adrenaline) > 0) {
					inventory.RemoveItem(ItemType.Adrenaline);
					effectsComponent.ApplyAdrenalinEffect();
				}
				
				effectsComponent.AddInfected(ManagerScript.instance.CachedTime[3] *0.02f);
			}
			
			if (GetRandom (4000) == 0) {
				int randomItem = GetRandom(100);
				
				if (randomItem < 35) {
					inventory.AddItem(ItemType.Food);
				}
				else if (randomItem < 90) {
					inventory.AddItem(ItemType.Ammo,3);
				}
				else if (randomItem < 99){
					inventory.AddItem(ItemType.Adrenaline);
				}
				else {
					inventory.AddItem(ItemType.AntiViral);
				}
				
			}
		}

		protected override void SpecificUpdate()
		{
			base.SpecificUpdate ();


			//Debug.Log (CurrentState.ToString() + followNode.HasFollowTarget + followNode.FollowTarget);
		}
		
		[RPC]
		void RPCAddItem(int itemIndex, int amount)
		{
			inventory.AddItem ((ItemType)itemIndex, amount);
		}
		
		[RPC]
		void RPCRemoveItem(int itemIndex, int amount)
		{
			inventory.RemoveItem ((ItemType)itemIndex, amount);
		}

		public void Follow(ITargetable t)
		{
			//AssignTarget(t);
			//AssignFollowTarget (t);
			
			//StateAI = BehaviourState.follow;
			speechComponent.SayFollow ();
			AssignFollowTarget (t);
			
		}

		public override AnimationDecorator GetAnimationDecorator()
		{
			if (gunComponent.GunType == null || gunComponent.Ammo == 0)
			{
				return AnimationDecorator.Unarmed;
			}
			else {
				return gunComponent.GunType.GetAnimationDecorator();
			}
		}



		private void AssignFollowTarget(ITargetable tar)
		{
			followNode.SetFollowTarget (tar);
			engageNode.SetReturnNode (followNode);
			SetCurrentState(followNode);
			//IsHoldingPosition = false;
			//followTarget = tar;
			//HasFollowTarget = true;
			//HasFollowTargetMem = true;
		}
		
		public void ReleaseFollowTarget()
		{
			followNode.FreeFollowTarget ();
			engageNode.SetDefaultReturnNode ();
			StopMovement ();
			speechComponent.SayBye ();
		}

		public bool IsFollowing(ITargetable t)
		{
			/*if (followTarget == t && HasFollowTarget) {
				return true;
			}
			
			return false;*/

			return followNode.HasFollowTarget && followNode.FollowTarget == t;
		}

		#region IInteractable implementation
		public bool Interact (HeroBeing hero)
		{
			speechComponent.SayHello ();
			return true;
			
		}
		#endregion	



		public void HoldPosition (bool b)
		{
			HoldPosition ();
		}

		public void HoldPosition ()
		{
			ReleaseFollowTarget ();
			ReleaseTarget ();
			SetCurrentState(holdPositionNode);
			holdPositionNode.SetHoldPosition (transform.position);
		}

		#region IEquippor implementation

		public void EquipAction (ItemShoes shoes)
		{
			movementModule.AddMovementModifier (shoes.MovementModifier);
			//exposureComponent.walkingExposure = shoes.WalkingExposure;
			//exposureComponent.runningExposure = shoes.RunningExposure;
		}

		public void UnequipAction (ItemShoes shoes)
		{
			movementModule.AddMovementModifier (-shoes.MovementModifier);
			//exposureComponent.walkingExposure = ExposureComponent.MIN_EXPOSURE_WALKING;
			//exposureComponent.runningExposure = ExposureComponent.MIN_EXPOSURE_RUNNING;
		}
		
		public void EquipAction (ItemGun gun)
		{
			/*if (gunComponent.Ammo > 0 && StateAI == BehaviourState.meleeEngage) {
				StateAI = BehaviourState.stationaryEngage;
			}*/
		}
		
		public void UnequipAction (ItemGun gun)
		{
			//melee = true;
		}

		#endregion
	}
}

