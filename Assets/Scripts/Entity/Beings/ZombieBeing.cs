using System;
using System.Collections.Generic;
using GraveDays.Entity.Being.AI;
using UnityEngine;

namespace GraveDays.Entity.Being
{
	public class ZombieBeing: AiBeing
	{
		public int ID;
		public const int FRAME_TICK = 7;

		public int TickID {
			get;
			private set;
		}

		public static int IDs { get; private set;}

		private AudioSource zombieSource;
		public static int gruntSaturate = 0;
		private const int gruntMax = 5;
		private const float exposureOnCall = 3.8f;

	//	protected AreaComponentZombie areaComponent;

		private bool isRecycling;

		public ZombieBeing ()
		{

		}

		public virtual void OnEnable()
		{
			stunned = false;
			isRecycling = false;
			float moveSpeed = UnityEngine.Random.Range (13.5f, 15f);
			movementModule.SetBaseSpeed (moveSpeed);
			healthComponent.Initialise (this, UnityEngine.Random.Range (30, 150));
			ReleaseTarget ();
			ReleaseAttackTarget ();
			//StateAI = BehaviourState.idleStand;
			//interpEnable = false;
//			gameObject.GetComponentInChildren<ZombieAttackComponent> ().Refresh ();
			targeterList.Clear ();
			localAffected.Clear ();
			//Debug.Log("ENABLE ZOMBIE
			collisionTracker.Refresh ();
			SetCurrentState (idleNode);
			//movementModule.SetBaseSpeed (0);
		}

		protected override void OnAwake()
		{
			base.OnAwake ();
			ID = IDs++;
			TickID = ID % FRAME_TICK;
			areaComponent = GetComponent<AreaComponentZombie> ();
		}

		protected override void OnStart()
		{
			base.OnStart ();
			collisionTracker.AddEnemyTag ("Player");
			collisionTracker.AddEnemyTag ("Survivor");
		}

		[RPC]
		protected override void _OnDied()
		{
			CancelInvoke ("StunRecover");
			stunned = true;
			ReleaseTarget ();
			ManagerScript.instance.killCount++;
			StopGrunt ();
			Grunt(GlobalData.instance.zombieGrunts [2]);
			
			if (!IsInvoking ("Recycle")) {
				Invoke ("Recycle", 1.2f);
			}
			
			if (IsInvoking("UnsaturateGrunt"))
			{
				gruntSaturate--;
				CancelInvoke("UnsaturateGrunt");
			}

			/*for (int i = targeterList.Count -1; i >= 0; i--) {
				targeterList[i].NotifyDeath(this);
			}
			
			foreach (var affected in localAffected) {
				affected.UnassignAffected(this);
			}*/
			
			
		//	QuestData zombieData = new KillTargetQuestData (this);
		//	ManagerScript.instance.questManager.AdvanceQuest (QuestType.KillTarget, zombieData);  
		//	ManagerScript.instance.questManager.AdvanceQuest (QuestType.KillZombies);
		}

		#region implemented abstract members of StandardBeing
		public override void OnDied ()
		{
			if (ManagerScript.networkType == NetworkType.Single) {
				_OnDied();
			} 
			else {
				networkView.RPC ("_onDied", UnityEngine.RPCMode.All);
			}

			/*AreaComponentZombie ac = GetComponent<AreaComponentZombie> ();
			foreach (AreaBehaviour a in ac.areas)
			{
				a.ReduceInfestationLevel();

			}*/

			/*for (int i = targeterList.Count -1; i >= 0; i--) {
				targeterList[i].NotifyDeath(this);
			}
			
			foreach (var affected in localAffected) {
				affected.UnassignAffected(this);
			}*/
		}

		#endregion


		protected override void OnAquireAttackTarget()
		{
			AlertGrunt();
		}

		public void AlertGrunt()
		{
			attackTarget.IncreaseExposure(exposureOnCall);
			Grunt (0);
		}
		
		void StopGrunt()
		{
			if (zombieSource != null) {
				zombieSource.Stop();
			}
		}
		
		[RPC]
		void GruntSound(int index)
		{
			Grunt (GlobalData.instance.zombieGrunts[index]);
			Debug.Log ("GRUNT served " + networkView.viewID);
		}
		
		public void Grunt(int index)
		{



			Grunt(GlobalData.instance.zombieGrunts [index]);
			
			if (ManagerScript.networkType != NetworkType.Single) {
				Debug.Log ("GRUNT " + networkView.viewID);
				networkView.RPC ("GruntSound", RPCMode.Others, index); 
			}
		}
		
		
		void Grunt(AudioClip grunt)
		{
			if (gruntSaturate != gruntMax) {
				if (zombieSource == null) {
					zombieSource = gameObject.AddComponent<AudioSource> ();
				}
				
				if (!zombieSource.isPlaying) {
					float volume = SoundPlayer.GetVolumeOverDistance(transform, 120);
					if (volume > 0.005f)
					{
						if (volume > 1)
						{
							volume = 1;
						}
						zombieSource.PlayOneShot (grunt, volume);
						gruntSaturate++;
						Invoke("UnsaturateGrunt",0.6f);
					}
					
				}
			}
			
			
		}
		
		void UnsaturateGrunt()
		{
			gruntSaturate--;
		}



		public void OnRecycle()
		{

			foreach (AreaBehaviour a in areaComponent.areas) {
				a.zombies.Remove(this);
				a.ReduceInfestationLevel();
			}
			
			ManagerScript.instance.zombies.Remove (this);
			GameObjectPool.instance.FreeZombie (this);
		//	Debug.Log("DO RECYCLE " + ID);
			
			//reverse loop because Notify death modifies this targeterList through ReleaseTarget of targeter
			for (int i = targeterList.Count -1; i >= 0; i--) {
				targeterList[i].NotifyDeath(this);
			}
			
			foreach (var affected in localAffected) {
				affected.UnassignAffected(this);
			}
			//networkView.stateSynchronization = NetworkStateSynchronization.Off;
		}
		
		//public 
		
		[RPC]
		public void Recycle()
		{
			if (!isRecycling) {
				isRecycling = true;
				ManagerScript.instance.zombiesToRecycle.Add (this);
//				Debug.Log("AD TO RECYCLE " + ID);
			}
			
		}

		public override void DoUnspawn()
		{
			base.DoUnspawn ();
			Recycle();
			if (Network.isServer) {
				networkView.RPC("Recycle", RPCMode.Others);
			}
		}

		protected override void OnReleaseTarget()
		{
			target.ReduceBaseExposureSightLost (exposureOnCall);
		}

		public bool CanAttack()
		{
			return !stunned && IsValid ();
		}

		public override bool IsValid()
		{
			return !isRecycling && base.IsValid();
		}
		
		protected override void SeedBehaviourTree ()
		{
			ZombieChaseNode<ZombieBeing> chaseNode = new ZombieChaseNode<ZombieBeing> (this);
			idleNode = new IdleNode<ZombieBeing>(this, chaseNode);
			SetCurrentState (idleNode);
			chaseNode.SetDefaultReturnNode ();
		}
	

		public override void DoIdleActions()
		{
			if (GetRandom (800) == 0) {
				Grunt (3);
			}
		}
	}
}

