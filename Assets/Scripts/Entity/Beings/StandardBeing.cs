using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using GraveDays.Effects;
using GraveDays.Animation;

namespace GraveDays.Entity.Being
{
	public abstract class StandardBeing : MonoBehaviour, IHealthReactive, ITargetable, IOccupant, ILocalAffector
	{
		public MovementComponent movementModule;
		public AnimatorDecorated animatorModule;
		public AreaComponent areaComponent;
		public MeleeComponent meleeComponent;
		public HealthComponent healthComponent;
		public EffectsComponent effectsComponent;

		public List<ILocalAffector> localAffected = new List<ILocalAffector> ();
		protected List<ITargeter> targeterList = new List<ITargeter> ();

		protected float defaultSpeed = 12f;
		protected bool stunned;

		protected System.Random randomer;

		private Vector2 _facingVector;
		protected bool recalcRotationTransform;
		public Vector2 FacingVector {
			get {return _facingVector; }
			set {
				_facingVector = value; 
				recalcRotationTransform = true;
			}
		}
		
		public Vector2 MovingVector {
			get;
			protected set;
		}
	
		
		void Awake()
		{
			animatorModule = GetComponent<AnimatorDecorated> ();
			movementModule = GetComponent<MovementComponent> ();
			areaComponent = GetComponent<AreaComponent> ();
			movementModule.SetBaseSpeed (defaultSpeed);	
			meleeComponent = GetComponent<MeleeComponent> ();
			effectsComponent = GetComponent<EffectsComponent> ();
			healthComponent = GetComponent<HealthComponent> ();
			healthComponent.Initialise (this, 100f);	
			OnAwake ();
			randomer = new System.Random (UnityEngine.Random.Range(-100000,100000));
		}

		protected virtual void OnAwake()
		{

		}

		void Start() 
		{
			OnStart ();
		}

		protected virtual void OnStart()
		{
			
		}
				
		protected virtual void SpecificUpdate()
		{

		}

		public void UpdateAnimation()
		{
			if (!stunned) {
				animatorModule.UpdateAnimation ();
			}
		}

		public Vector2 GetVectorTo (ITargetable target) 
		{
			return target.GetTransform ().position - transform.position;
		}

		public void ProcessEffects()
		{
			effectsComponent.ProcessEffects ();
		}

		// Update is called once per frame
		public bool ManualUpdate () 
		{

			if (ManagerScript.instance.PAUSED || healthComponent.IsDead () || stunned) {
				return false;
			}

			SpecificUpdate ();

			movementModule.ManualUpdate ();

			return true;
		}

		public virtual AnimationDecorator GetAnimationDecorator()
		{
			return AnimationDecorator.Unarmed;
		}

		protected virtual void NetworkSerializeWrite(BitStream stream, NetworkMessageInfo info) 
		{
			float x = 0;
			float y = 0;
			
			float xVel = 0;
			float yVel = 0;
			
			float xFace = 0;
			float yFace = 0;
			
			x = GlobalData.instance.player.transform.position.x;
			y = GlobalData.instance.player.transform.position.y;
			
			xVel = GlobalData.instance.player.MovingVector.x;
			yVel = GlobalData.instance.player.MovingVector.y;
			
			xFace = GlobalData.instance.player.FacingVector.x;
			yFace = GlobalData.instance.player.FacingVector.y;		
			
			stream.Serialize (ref x);
			stream.Serialize (ref y);
			stream.Serialize (ref xVel);
			stream.Serialize (ref yVel);
			stream.Serialize (ref xFace);
			stream.Serialize (ref yFace);
		}
		
		protected virtual void NetworkSerializeRead(BitStream stream, NetworkMessageInfo info) 
		{
			
		}

		void OnSerializeNetworkView(BitStream stream, NetworkMessageInfo info) 
		{
			if (stream.isWriting) {
				NetworkSerializeWrite(stream, info);
			} else if (stream.isReading) {
				NetworkSerializeRead(stream, info);
			}			
		}

		
		#region ITargetable implementation
		
		public virtual float GetExposure ()
		{
			return ManagerScript.instance.LightExposure/120f + 0.2f;
		}
		
		public bool IsMoving()
		{
			return movementModule.IsMoving; 
		}
		
		public void AssignTargeter (ITargeter targeter)
		{
			targeterList.Add (targeter);
		}
		
		public void UnassignTargeter (ITargeter targeter)
		{
			targeterList.Remove (targeter);
		}
		
		public float GetSqrExposure ()
		{
			float exposure = GetExposure ();
			return exposure * exposure;
		}
		
		public Transform GetTransform()
		{
			return transform;
		}
		
		public void IncreaseExposure(float value)
		{

		}
		
		public void ReduceBaseExposureSightLost(float originalValue)
		{

		}
		
		public HealthComponent GetHealthComponent()
		{
			return healthComponent;
		}
		
		public EffectsComponent GetEffectsComponent()
		{
			return effectsComponent;
		}
		
		public Vector2 GetMovingVector()
		{
		//	if (movementModule.IsMoving) {
				return FacingVector;
			/*} else
				return Vector2.zero;*/
		}
		
		public virtual bool IsValid()
		{
			return !healthComponent.IsDead ();
		}
		#endregion

		public void StopMovement()
		{
			movementModule.Stop ();
		//	animatorModule.UpdateAnimationState ();
		}

		
		#region IHealthReactive implementation
		public virtual void TakeDamageCallback (float amount)
		{
	
		}

		protected virtual void _OnDied()
		{

			Vector3 pos = new Vector3 (transform.position.x, transform.position.y, 0.01f);
			
			healthComponent.HealthRecovery = 0;
			
			float xOffset = 0;
			float yOffset = 0;
			for (int i = 0; i < 9; i++) {
				GameObjectPool.instance.ServeBloodSplatter(pos + Vector3.ClampMagnitude(new Vector3(xOffset,yOffset),0.8f));
				xOffset += UnityEngine.Random.Range (-1f,1f);
				yOffset += UnityEngine.Random.Range (-1f,1f);
			}
			
			if (ManagerScript.instance.allies.Count > 0) {
				foreach (Transform t in transform) {
					t.parent = ManagerScript.instance.allies[0].transform;
					t.localPosition = new Vector3(0, 0, t.position.z);
				}
				
			} else {
				gameObject.transform.DetachChildren ();
			}
			ManagerScript.instance.GameOver = true;

			for (int i = targeterList.Count -1; i >= 0; i--) {
				targeterList[i].NotifyDeath(this);
			}

			Destroy (gameObject);
		}


		/// <summary>
		/// Implement OnDied to call _onDied locally and via network RPC. Remember to extend _onDied
		/// method and mark as RPC in concrete class.
		/// </summary>
		public abstract void OnDied();
		#endregion	


		#region IMelee implementation
		
		public void OnAttackSwing ()
		{
			if (ManagerScript.networkType != NetworkType.Single)
			{
				networkView.RPC("RPCMeleeSwing", RPCMode.Others);
			}
		}
		
		public void OnAttackHit ()
		{
			if (ManagerScript.networkType != NetworkType.Single) {
				networkView.RPC ("RPCMeleeHit", RPCMode.Others);
			}
		}
		
		#endregion

		public void Stun(float period)
		{
			stunned = true;
			CancelInvoke ("StunRecover");
			StopMovement ();
			OnStun ();
			if (!healthComponent.IsDead ()) {
				Invoke ("StunRecover", period);
			}
		}

		protected virtual void OnStun()
		{

		}
		
		public void StunRecover()
		{
			stunned = false;
		}

		
		[RPC]
		void RPCMeleeSwing()
		{
			
		}
		
		[RPC]
		void RPCMeleeHit()
		{
			
		}

		protected virtual void OnHitByMelee(ITargetable hitter)
		{

		}

		public void HitByMelee(float damage, float stun, Vector3 knockback, ITargetable hitter)
		{
			if (Network.isServer || ManagerScript.networkType == NetworkType.Single) {
				OnHitByMelee(hitter);
				RPCHitByMelee(damage, stun, knockback);
			} else {
				networkView.RPC("RPCHitByMelee",RPCMode.All, damage, stun, knockback);
			}
		}
		
		[RPC] 
		void RPCHitByMelee(float damage, float stun, Vector3 knockback)
		{
			healthComponent.TakeDamage (damage);
			rigidbody2D.AddForce (knockback);
			Stun (stun);
		}
		
		public void RPCHitByBullet(float damage)
		{
			networkView.RPC ("_hitByBullet", RPCMode.Server, damage);
		}
		
		[RPC] void _hitByBullet(float damage)
		{
			healthComponent.TakeDamage (damage);
		}
		
		#region ILocalAffector implementation
		public virtual void AssignAffected (ILocalAffector affected)
		{
			localAffected.Add (affected);
		}
		public virtual void UnassignAffected (ILocalAffector affected)
		{
			localAffected.Remove (affected);
		}
		#endregion
		
		/*#region ILocalAffected implementation
		
		public virtual void NotifyEmigration (ILocalAffector affector)
		{
			localAffected.Remove (affector);
		}
		
		#endregion*/

		public int GetRandom(int high)
		{
			return randomer.Next (high);
		}
		
		public int GetRandom(int low, int high)
		{
			return randomer.Next (low, high);
		}
		
		public float GetRandom(float high)
		{
			return (float)randomer.NextDouble () * high;
		}
		
		
		public Vector2 GetRandomInCircle(float radius, Vector2 centre)
		{
			float r = GetRandom (radius);
			float theta = GetRandom (2 * Mathf.PI);
			
			float x = r * Mathf.Sin (theta);
			float y = r * Mathf.Cos (theta);
			
			return (new Vector2 (x, y) + centre);
		}
		
		public Vector2 GetRandomInCircleCentered(float radius)
		{
			return GetRandomInCircle (radius, transform.position);
		}
		
		/// <summary>
		/// Flips a warped coin, where the odds of winning the flip is oddsChance to oddsBase
		/// Example (oddsBase = 100, oddsChance = 5) chance to win is 5 to 100.
		/// </summary>
		/// <returns><c>true</c>, if warped coin flip was won, <c>false</c> otherwise.</returns>
		/// <param name="oddsChance">Odds chance.</param>
		/// <param name="oddsBase">Odds base.</param>
		public bool FlipWarpedCoin(int oddsChance, int oddsBase) //oddChance to oddsBase
		{
			//	Debug.Log (GetRandom (oddsBase));
			return (oddsChance > GetRandom (oddsBase));
		}
	}
}

