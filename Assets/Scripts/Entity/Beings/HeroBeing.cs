using System;
using System.Collections.Generic;
using UnityEngine;

using GraveDays.Animation;
using GraveDays.Items;
using GraveDays.Items.Guns;

namespace GraveDays.Entity.Being
{
	public class HeroBeing: OperatedBeing
	{
		public FatigueComponent fatigueComponent;
		public ExposureComponent exposureComponent;

		public float Fatigue
		{
			get 
			{
				return fatigueComponent.Fatigue;
			}
			
			set 
			{
				fatigueComponent.Fatigue = value;
			}
		}

		public Light2D senseLight;
		private Color senseColor;
		private const float HURT_PERIOD = 0.25f;
		private float hurtTimer;

		public bool PrimaryInputs = true;
		public float Hunger;
		public float hungerMultiplier = 1f;
		
		public bool ForceFollow;
		private float forceFollowTime;
		
		private bool scrollReady;
		private float mouseTurn;

		private AudioSource zipSource;
		private AudioSource footstepsAudio;
		
		private bool holdMove;
		public int RareItemsFound;

		public PlayerClass playerClass;
		
		public int FoodCount
		{
			get {
				return inventory.GetItemCount(ItemType.Food);
			}
		}
		public int BandagesCount
		{
			get {
				return inventory.GetItemCount(ItemType.Bandages);
			}
		}

		protected override void _OnDied()
		{
			ManagerScript.instance.living.Remove (gameObject);
			SoundPlayer.instance.PlayForced(Sounds.playerDeath);
			
			Vector3 pos = new Vector3 (transform.position.x, transform.position.y, 0.01f);
			
			healthComponent.HealthRecovery = 0;
			
			float xOffset = 0;
			float yOffset = 0;
			for (int i = 0; i < 9; i++) {
				GameObjectPool.instance.ServeBloodSplatter(pos + Vector3.ClampMagnitude(new Vector3(xOffset,yOffset),0.8f));
				xOffset += UnityEngine.Random.Range (-1f,1f);
				yOffset += UnityEngine.Random.Range (-1f,1f);
			}
			Destroy (senseLight);
			Destroy (Torch);
			
			if (ManagerScript.instance.allies.Count > 0) {
				foreach (Transform t in transform) {
					t.parent = ManagerScript.instance.allies[0].transform;
					t.localPosition = new Vector3(0, 0, t.position.z);
				}
				
			} else {
				gameObject.transform.DetachChildren ();
			}
			ManagerScript.instance.GameOver = true;
			FinaliseDeath ();
		}

		void FinaliseDeath()
		{
			if (effectsComponent.IsEffected (EffectsType.Infected)) {
				ManagerScript.instance.SpawnZombie(transform.position);
			}
			Destroy (gameObject);
		}


		#region implemented abstract members of StandardBeing

		public override void OnDied ()
		{
			if (NetworkType.Single == ManagerScript.networkType) {
				_OnDied();
			} else {
				networkView.RPC ("_OnDied", RPCMode.All);
			}
		}

		#endregion

		protected override void OnAwake()
		{
			base.OnAwake ();
			scrollReady = true;
			fatigueComponent = GetComponent<FatigueComponent> ();
			healthComponent.Initialise (this, 100f);
			exposureComponent = GetComponent<ExposureComponent> ();
			footstepsAudio = gameObject.AddComponent<AudioSource> ();
			zipSource = gameObject.AddComponent<AudioSource> ();
			
			footstepsAudio.volume = 0.6f;
			Hunger = 75;
		}

		protected override void OnStart()
		{
			base.OnStart ();
			footstepsAudio.clip = GlobalData.instance.sounds [(int)Sounds.footstep1];
			movementModule.SetBaseSpeed (10);	
			PrimaryInputs = true;

		}

		void SetAnimatorComponent(PlayerClass playerClass)
		{
			switch (playerClass)
			{
			case PlayerClass.AverageJoe:
				animatorModule = gameObject.AddComponent<AnimatorJoe>();
				break;
			case PlayerClass.AverageJane:
				animatorModule = gameObject.AddComponent<AnimatorJoe>();
				break;
			case PlayerClass.Athlete:
				animatorModule = gameObject.AddComponent<AnimatorJoe>();
				break;
			case PlayerClass.Leader:
				animatorModule = gameObject.AddComponent<AnimatorJoe>();
				break;
			case PlayerClass.Soldier:
				animatorModule = gameObject.AddComponent<AnimatorJoe>();
				break;
			case PlayerClass.Farmer:
				animatorModule = gameObject.AddComponent<AnimatorJoe>();
				break;
			}
		}
		
		public void Initialise(PlayerClass playerClass)
		{
			SetAnimatorComponent (playerClass);
			this.playerClass = playerClass;
			
			if (!ManagerScript.doLoad) {
				AddAndEquip(ItemGun.Glock);
				//AddToBag(ItemType.GunAK);
				//AddAndEquip(ItemGun.Shotgun);
				AddToBag (ItemType.Food, 3);
				AddToBag (ItemType.Ammo, 25);
				AddToBag (ItemType.Bandages, 4);
				AddToBag (ItemType.Adrenaline, 3);
				AddToBag (ItemType.AntiViral, 1);
				
				switch (playerClass)
				{
				case PlayerClass.AverageJoe:
					
					break;
				case PlayerClass.AverageJane:
					
					break;
				case PlayerClass.Athlete:
					InitialiseAthlete();
					break;
				case PlayerClass.Leader:
					InitialiseLeader();
					break;
				case PlayerClass.Soldier:
					InitialiseSoldier();
					break;
				case PlayerClass.Farmer:
					InitialiseFarmer();
					break;
				}


			}
		
			
			
		}

		void InitialiseFarmer()
		{
			RPCRemoveItem ((int)ItemGun.Glock.GetItemType (), 1);
			AddAndEquip (ItemGun.Shotgun);
			AddToBag (ItemType.Ammo, 20);
			AddToBag (ItemType.Food, 2);
			InvokeRepeating ("FarmerAddFood", 60, 60);
		}

		void FarmerAddFood()
		{
			if (inventory.GetItemCount (ItemType.Food) < 2) {
				RPCAddItem ((int)ItemType.Food, 1);
			}
		}
		
		void InitialiseAthlete()
		{
			movementModule.SetBaseSpeed (defaultSpeed + 1);
			AddAndEquip ((ItemEquippable)ItemType.RunningShoes.GetItemInstance());
			fatigueComponent.MaxFatigue = 200;
			fatigueComponent.Fatigue = 200;
			
		}
		
		void InitialiseSoldier()
		{
			RPCRemoveItem ((int)ItemGun.Glock.GetItemType (), 1);
			AddAndEquip (ItemGun.AK47);
			AddToBag (ItemType.Ammo, 10);
			InvokeRepeating ("SoldierAddAmmo", 15, 15);
		}
		
		void SoldierAddAmmo()
		{
			if (inventory.GetItemCount (ItemType.Ammo) < 20) {
				RPCAddItem ((int)ItemType.Ammo, 1);
			}
		}
		
		
		void InitialiseLeader()
		{
			if (!Network.isClient) {
				SurvivorBeing surv = ManagerScript.instance.SpawnRandomSurvivor (transform.position);
				surv.GetComponent<InteractComponentSurvivor> ().TrustRating = 100;
				surv.Follow (this);
				surv.AddAndEquip (ItemGun.Glock);
				surv.AddToBag (ItemType.Ammo, 20);
				surv.AddToBag (ItemType.Bandages, 5);
				surv.AddToBag (ItemType.Food, 8);
			}
		}

		[RPC]
		void RPCAddItem(int itemIndex, int amount)
		{
			inventory.AddItem ((ItemType)itemIndex, amount);
		}
		
		[RPC]
		void RPCRemoveItem(int itemIndex, int amount)
		{
			inventory.RemoveItem ((ItemType)itemIndex, amount);
		}

		protected override void SpecificUpdate()
		{
			base.SpecificUpdate ();
			HandleInputs ();
			
			if (ForceFollow) {
				forceFollowTime -= Time.deltaTime;
				
				if (forceFollowTime <= 0) {
					ForceFollow = false;
				}
			}
			
			exposureComponent.DecayExposure (movementModule.MoveType);

			if (fatigueComponent.UpdateFatigue (movementModule.MoveType, Hunger)) {
				DoStopSprint();
			}
			
			if (hurtTimer > 0) {
				hurtTimer -= Time.deltaTime;
			} 
			else {
				senseColor = new Color (exposureComponent.Exposure, exposureComponent.Exposure, 1f);
				senseLight.LightColor = senseColor;
				hurtTimer = 0;
			}
			
			if (Hunger > 0) {
				Hunger -= Time.deltaTime * (0.1f + 0.05f * Fatigue / 100f) * hungerMultiplier;
			} 

			if (Hunger <= 0) {
				Hunger = 0;
				healthComponent.DecreaseMaxHealth(Time.deltaTime*0.5f);
			}

			//animatorModule.UpdateAnimationState ();
			UpdateAnimation ();
		}

		public void Heal(ItemBandages bandages)
		{
			if (BandagesCount > 0) {
				RemoveFromBag(bandages.GetItemType());
				healthComponent.RecoverHealth(bandages.HealValue);
				SoundPlayer.instance.PlayForced(Sounds.bandAid, 0.72f);
				
				if (effectsComponent.IsEffected(EffectsType.Bleeding))
				{
					//effectsComponent.ApplyEffect(new EffectBleeding(-1f));
					effectsComponent.ReduceBleed(1);
				}
			}
		}
		
		public void PlayZipSound()
		{
			SoundPlayer.instance.PlayForced(Sounds.bagZip, 1);
		}
		
		public void Eat(ItemFood food)
		{
			
			if (FoodCount > 0) {
				SoundPlayer.instance.PlayForced(Sounds.appleBite, 1);
				
				RemoveFromBag(food.GetItemType());
				Hunger += food.HungerValue;
				Fatigue += food.HungerValue;
				
				if (Hunger > 100) {
					Hunger = 100;
				}
				
				if (Hunger > 50) {
					healthComponent.IncreaseMaxHealth (food.HungerValue * 0.25f);
				}
			}
		}

		public override float GetExposure()
		{
			return exposureComponent.Exposure;
		}

		bool IsFatigued()
		{
			return Fatigue < 1f;
		}

		void HandleSurfaceType ()
		{
			Ray ray = new Ray (transform.position, Vector3.forward);
			RaycastHit hit;
			if (Physics.Raycast (ray, out hit, 5f)) {
				if (hit.transform.CompareTag ("Earth")) {
					footstepsAudio.clip = GlobalData.instance.sounds [(int)Sounds.earthyFootstep];
				}
				else
				if (hit.transform.CompareTag ("Water")) {
					footstepsAudio.clip = GlobalData.instance.sounds [(int)Sounds.waterFootstep];
				}
				else
				if (hit.transform.CompareTag ("Wood")) {
					footstepsAudio.clip = GlobalData.instance.sounds [(int)Sounds.footstepWood];
				}
				else {
					footstepsAudio.clip = GlobalData.instance.sounds [(int)Sounds.footstep1];
				}
			}
			if (!footstepsAudio.isPlaying) {
				footstepsAudio.Play ();
				CancelInvoke ("StopFootstepAudio");
			}
		}
		
		void MovementInput ()
		{
			float mouseX = Input.GetAxis ("Mouse X");
			mouseTurn =  (mouseX * -1.35f + mouseTurn *0.82f  );
			mouseTurn = mouseTurn > 15 ? 15 : mouseTurn;
			mouseTurn = mouseTurn < -15 ? -15 : mouseTurn;
			//mousePos.z = 5.23f;
			Vector2 targetPos = Camera.main.WorldToScreenPoint (transform.position);
			
			//FacingVector = (mousePos - targetPos).normalized;
			FacingVector = transform.up;
			
			transform.Rotate (Vector3.forward, mouseTurn);
			//bool moveForward = Input.GetMouseButton (1);
			bool WASD = cInput.GetKey("Forward") || cInput.GetKey("Backward") || cInput.GetKey("Strafe Left") || cInput.GetKey("Strafe Right");
			if ( Input.GetKey (KeyCode.Space) || WASD) {
				if (WASD) {
					MovingVector = Vector2.zero;
				}
		
				if (cInput.GetKey("Forward")) {
					MovingVector += new Vector2 (0, 1);
				}
				if (cInput.GetKey("Backward")) {
					MovingVector += new Vector2 (0, -1);
				}
				if (cInput.GetKey("Strafe Left")) {
					MovingVector += new Vector2 (-1, 0);
				}
				if (cInput.GetKey("Strafe Right")) {
					MovingVector += new Vector2 (1, 0);
				}
				MovingVector.Normalize();
				MovingVector = transform.TransformVector(MovingVector);
				float directionDot = Vector2.Dot (MovingVector, FacingVector) * 0.32f + 0.78f;
				if (directionDot > 1) {
					directionDot = 1;
				}
				else {
					//movement.StopSprint ();
					DoStopSprint();
				}
				movementModule.SetBaseSpeed (defaultSpeed * directionDot);
			
				//movement.Move (MovingVector);
				ManagerScript.instance.CloseOpenMenus (false);
				HandleSurfaceType ();


			}
			else {
				if (MovingVector != Vector2.zero)
				{
					MovingVector = Vector2.zero;
					Invoke ("StopFootstepAudio", 0.2f);
				}
			}

			if (MovingVector != Vector2.zero) {
				movementModule.Move (MovingVector);
			} else {
				movementModule.Stop();
			}
		}
		
		[RPC]
		public void DoStartSprint()
		{
			movementModule.Sprint(8f);
			footstepsAudio.volume = 0.85f;
			animatorModule.SetAnimSpeedMultiplier (0.76f);
		}
		
		[RPC]
		public void DoStopSprint()
		{
			movementModule.StopSprint();
			footstepsAudio.volume = 0.6f;
			animatorModule.SetAnimSpeedMultiplier (1.15f);
		}

		void StopFootstepAudio()
		{
			footstepsAudio.Stop();
		}
		
		void HandleInputs()
		{
			
			if (Input.GetKey (KeyCode.F5)) {
				Hunger = 100;
				Fatigue = 100;
				healthComponent.RecoverHealth(100);
				//effectsComponent.ApplyEffect(new EffectBleeding(-1000));
				effectsComponent.ReduceBleed(1000);
				
				AddToBag(ItemType.Ammo,50);
			}
			
			
			if (Input.GetKeyDown (KeyCode.Alpha1)) {
				melee = true;
			}
			
			if (Input.GetKeyDown (KeyCode.Alpha2) && gunComponent.GunType != null) {
				melee = false;
			}
			
			if (Input.GetKeyDown (KeyCode.Alpha3)) {
				Eat(ItemType.Food.GetItemInstance() as ItemFood);
			}
			
			if (Input.GetKeyDown (KeyCode.Alpha4)) {
				Heal(ItemType.Bandages.GetItemInstance() as ItemBandages);
			}
			
			MovementInput ();
			
			
			if (PrimaryInputs) {
				
				if (!movementModule.IsSprinting ) {

					if ( cInput.GetKey("Fire/Attack")) {
						if (melee) {
							if (meleeComponent.Attack(FacingVector)) {
								animatorModule.DoPunch();
							}
						}
						else {
							gunComponent.Shoot(FacingVector);
						}
					}
					else if (cInput.GetKey("Quick Melee")) {
						if (meleeComponent.Attack(FacingVector)) {
							animatorModule.DoPunch();
						}
					}
					
				}

				if (cInput.GetKeyDown("Flashlight")) {
					Debug.Log("HERE");
					if (torchEnabled) {
						TorchOff ();
					} else {
						TorchOn ();
					}
				}
				else if (cInput.GetKeyDown("Inventory")) {
					ManagerScript.instance.SetOptionActivated(this);
					PrimaryInputs = false;
					Screen.lockCursor = false;
				}
				else if (Input.GetKey (KeyCode.Q)) {
					ForceFollow = true;
					forceFollowTime = 3;
				}
				else if (Input.GetKeyDown (KeyCode.Tab)) {
					ManagerScript.instance.HomeMarker.transform.position = new Vector3(transform.position.x, transform.position.y, -0.2f); 
					ManagerScript.instance.HomeMarker.SetActive(true);
					ManagerScript.instance.HomeMarkerPosition = transform.position;
				}
				
				
				if (cInput.GetKey("Sprint") && !IsFatigued()) {
					if (!movementModule.IsSprinting)
					{
						DoStartSprint();
						if (ManagerScript.networkType != NetworkType.Single)
						{
							networkView.RPC("DoStartSprint", RPCMode.Others);
						}
					}
				}
				else if (movementModule.IsSprinting) {
					DoStopSprint();

					if (ManagerScript.networkType != NetworkType.Single) {
						networkView.RPC("DoStopSprint", RPCMode.Others);
					}
				}

				
				
			} 
			else {
				if (Input.GetMouseButtonUp (0))	{
					ManagerScript.instance.CloseOpenMenus(true);
				}
			}
			
			
			if (scrollReady && Mathf.Abs(Input.GetAxis ("Mouse ScrollWheel")) >= 1) {
				if (gunComponent.GunType != null) {
					melee = !melee;
					scrollReady = false;
					Invoke("ReadyScrollWheel", 0.15f);
				}
			}
			
		}

		[RPC]
		public void TorchOn()
		{
			if (!torchEnabled) {
				exposureComponent.AddLightEmitterExposure (60f);
				torchOnFunction ();
			}
			
			if (ManagerScript.networkType != NetworkType.Single) {
				networkView.RPC("TorchOn", RPCMode.Others);
			}
		}
		
		[RPC]
		public void TorchOff()
		{
			if (torchEnabled) {
				exposureComponent.RemoveLightEmitterExposure ();
				torchOffFunction ();
			}
			
			if (ManagerScript.networkType != NetworkType.Single) {
				networkView.RPC("TorchOff", RPCMode.Others);
			}
		}
		
		void ReadyScrollWheel()
		{
			scrollReady = true;
		}

		#region IHealthReactive implementation
		public override void TakeDamageCallback (float amount)
		{
			base.TakeDamageCallback (amount);
			hurtTimer = HURT_PERIOD;
			senseLight.LightColor = Color.red;
		}
		#endregion

		public override void DrawMenu()
		{
			int xWidth = 400;
			int yWidth = 300;
			int xStart = (int)(Screen.width / 2f - xWidth/2f);
			int yStart = (int)(Screen.height / 2f - yWidth/2f);
			
			Rect guiRect = new Rect (xStart, yStart, xWidth, yWidth);
			
			if (guiRect.Contains (Event.current.mousePosition)) {
				MouseOnGui = true;
			} else {
				MouseOnGui = false;
			}
			
			GUI.Box (guiRect, GUIContent.none); 
			
			GUIStyle titleStyle = new GUIStyle (GUI.skin.label);
			titleStyle.alignment = TextAnchor.UpperCenter;
			titleStyle.fontSize = 20;
			titleStyle.fontStyle = FontStyle.Bold;
			
			
			GUIStyle subTitleStyle = new GUIStyle (GUI.skin.label);
			subTitleStyle.alignment = TextAnchor.UpperCenter;
			subTitleStyle.fontSize = 16;
			subTitleStyle.fontStyle = FontStyle.Bold;
			
			GUIStyle hintStyle = new GUIStyle (GUI.skin.label);
			hintStyle.alignment = TextAnchor.UpperCenter;
			hintStyle.fontSize = 12;
			hintStyle.fontStyle = FontStyle.Italic;

			int heroFood = GlobalData.instance.player.FoodCount;
			int heroMeds = GlobalData.instance.player.BandagesCount;
			int heroAmmo = GlobalData.instance.player.gunComponent.Ammo;
			
			
			GUILayout.BeginArea (guiRect);
			GUILayout.Label ("Inventory", titleStyle);
			GUILayout.Label ("Click to use/equip item", hintStyle);
			//GUILayout.Label ("Hold number and click to assign hotkey", hintStyle);
			GUILayout.BeginHorizontal ();
			GUILayout.BeginVertical (GUILayout.Width (xWidth));
			
			scrollPosition1 = GUILayout.BeginScrollView(scrollPosition1,false,true);
			
			foreach(KeyValuePair<ItemType, int> entry in inventory.items)
			{
				BaseItem item = entry.Key.GetItemInstance();
				string entryText;
				string tooltip;
				
				if (item.IsEquippable())
				{
					if (equipComponent.IsEquipped (item as ItemEquippable))
					{
						entryText = entry.Value+" x "+item.GetItemText() + " (Equipped)";
						tooltip = "Unequip: " + item.GetItemTooltip();
					}
					else {
						entryText = entry.Value+" x "+item.GetItemText();
						tooltip = "Equip: " + item.GetItemTooltip();
					}
				}
				else {
					entryText = entry.Value+" x "+item.GetItemText();
					tooltip = "Use: " + item.GetItemTooltip();
				}
				if (GUILayout.Button ( new GUIContent(entryText, tooltip))) 
				{
					item.Use(this);
				}
			}
			
			GUILayout.EndScrollView();
			GUILayout.Label (GUI.tooltip);
			GUILayout.EndVertical ();
			GUILayout.EndHorizontal ();
			
			
			GUILayout.EndArea ();
		}

	}
}

