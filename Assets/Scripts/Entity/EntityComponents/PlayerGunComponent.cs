﻿using UnityEngine;
using System.Collections;
using System;
using GraveDays.Items.Guns;




public class PlayerGunComponent : GunComponent {
	
	private ExposureComponent exposureComponent;

	void Awake()
	{
		exposureComponent = GetComponent<ExposureComponent> ();
		inventory = GetComponent<InventoryComponent> ();
		equipComponent = GetComponent<EquipComponent> ();

	}

	public override void EmptyGunSpecifics()
	{
		SoundPlayer.instance.PlayForced(Sounds.gunEmptyClick);
	}

	public override void ShootSpecifics()
	{
		SoundPlayer.instance.PlayForced(gunShotSound);
		exposureComponent.AddBaseExposure (13f);
	}

	void Start()
	{
		gunShotSound = Sounds.gun9mm;
	}
}
