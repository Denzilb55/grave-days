using UnityEngine;
using GraveDays.Entity.Being;

public class MeleeComponentSurvivor: MeleeComponent
{
	SurvivorBeing survivor;


	protected override void Initialise()
	{
		base.Initialise ();
		attackPeriod = 0.6f;
		knockback = 90f;
		damage = 8f;
		stunBase = 0.2f;
		stunRandom = 0.18f;
	}

	void Awake()
	{
		survivor = GetComponent<SurvivorBeing> ();
	}

	protected override void AttackSpecifics()
	{
		//SoundPlayer.instance.PlayForced (Sounds.airSwing);
		survivor.speechComponent.SaySwing ();

	}
	
	protected override void AttackHitSpecifics(StandardBeing targetHit)
	{
		survivor.speechComponent.SayHit ();
	}
	
	
}
