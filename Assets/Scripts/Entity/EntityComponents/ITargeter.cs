using System;

namespace GraveDays.Entity
{
	public interface ITargeter: IDeathListener
	{
		float GetDotToTarget();
	}
}


