using System;
using UnityEngine;
using System.Collections.Generic;
using GraveDays.Items;
using GraveDays.Entity.Being;


public class InteractComponentAlly : InteractInventoryComponent, IOptioned
{

	private AllyBehaviour ally;
	

	private bool inspect;

	

	protected override string GetInventoryTitle ()
	{
		return "Inspect";
	}
	protected override string GetPutTitle ()
	{
		return "Give to " + name;
	}
	protected override string GetTakeTitle ()
	{
		return name;
	}

	protected override void Initialise()
	{
		ally = GetComponent<AllyBehaviour> ();
	}

	public override void NotifyClose()
	{

	}


	void OnDestroy()
	{
	//	survivorNames.Add (name);
	}

	void Start()
	{
		GetComponent<ToolTipComponent>().ToolTip = "E: "+name;
	}
	


	public override void DepositItem(HeroBeing interactor, IItem item, int amount = 1)
	{
		int current = interactor.inventory.GetItemCount (item);

		if (current < amount) {
			amount = current;
		}

		//ally.networkView.RPC ("RPCAddItem", ally.networkView.viewID.owner, (int)item.GetItemType (), amount);
		//int remains = interactor.inventory.RemoveItem (item, amount);
		int remains = InventoryComponent.TransferItem(item, interactor.inventory, inventory, ref amount);

		if (remains == 0 && item.IsEquippable()) {
			GraveDays.Items.ItemEquippable equipItem = (item as GraveDays.Items.ItemEquippable);
			
			if (interactor.equipComponent.IsEquipped(equipItem))
			{
				interactor.equipComponent.Unequip(equipItem);
			}
		}
	}

	

}


