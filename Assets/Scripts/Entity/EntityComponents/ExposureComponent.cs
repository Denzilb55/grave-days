﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class SaveDataExposureStats
{
	public float exposure;
	public float lightEmitterExposure;
}

public class ExposureComponent : MonoBehaviour {
	private float lightEmitterExposure;


	private float baseExposure = 0f;
	private const float BASE_EXPOSURE_SATURATE = 120;
	public const float MIN_EXPOSURE = 0.5f;
	public const float MIN_EXPOSURE_WALKING = 4f;
	public const float MIN_EXPOSURE_RUNNING = 10f;
	public float walkingExposure = MIN_EXPOSURE_WALKING;
	public float runningExposure = MIN_EXPOSURE_RUNNING;

	public float Exposure
	{
		get{
			float _base_exposure = baseExposure>65?65:baseExposure;
			float lightContribution;
			if (lightEmitterExposure > ManagerScript.instance.LightExposure)
			{
				lightContribution = lightEmitterExposure + 0.5f*ManagerScript.instance.LightExposure;
			}
			else 
			{
				lightContribution = 0.5f*lightEmitterExposure + ManagerScript.instance.LightExposure;
			}
			float _exposure = _base_exposure  + lightContribution;
			if (_exposure > 100)
			{
				_exposure = 100;
			}

			return (_exposure)/100f;
		}
	}

	public SaveDataExposureStats GetSaveObject()
	{
		SaveDataExposureStats exposureSave = new SaveDataExposureStats ();
		exposureSave.exposure = baseExposure;
		exposureSave.lightEmitterExposure = lightEmitterExposure;

		return exposureSave;
	}

	public void LoadFromSaveObject(SaveDataExposureStats data)
	{
		baseExposure = data.exposure;
		lightEmitterExposure = data.lightEmitterExposure;
	}

	public void AddLightEmitterExposure(float exposure)
	{
		lightEmitterExposure = exposure;
	}

	public void RemoveLightEmitterExposure()
	{
		baseExposure += lightEmitterExposure * 0.25f;
		lightEmitterExposure = 0;
	}

	public void AddBaseExposure(float value)
	{
		baseExposure += value;
		
		if (baseExposure > BASE_EXPOSURE_SATURATE) {
			baseExposure = BASE_EXPOSURE_SATURATE;
		}
	}

	public void ReduceBaseExposureSightLost(float originalValue)
	{
		if (baseExposure > 30) {
			baseExposure -= originalValue;
			if (baseExposure < 30)
			{
				baseExposure = 30;
			}
		}
	}
	


	public void DecayExposure(MovementType movementType)
	{
		float minExposure = 0;
		int decayFactor = 0;
		switch (movementType) {
		case MovementType.none: 
			minExposure = MIN_EXPOSURE;
			decayFactor = 3;
			break;
		case MovementType.walking: 
			minExposure = walkingExposure;
			decayFactor = 1;
			break;
		case MovementType.running: 
			minExposure = runningExposure;
			return;

		}
		if (baseExposure > minExposure) {
			baseExposure -= Time.deltaTime*decayFactor*1.6f;
		} else {
			baseExposure = minExposure;
		}
	}

}
