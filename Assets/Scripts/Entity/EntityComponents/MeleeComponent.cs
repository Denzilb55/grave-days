using UnityEngine;
using System.Collections.Generic;
using GraveDays.Entity.Being;

public class MeleeComponent: MonoBehaviour
{
	
	private bool attack_ready = true;
	protected float attackPeriod = 0.57f;
	protected float knockback = 130f;
	protected float damage = 10f;
	protected float stunBase = 0.42f;
	protected float stunRandom = 0.28f;
	protected float range = 0.92f;
	private IMelee meleeUnit;
	private FatigueComponent fatigueComponent;
	protected List<string> attackTags;

	private Vector2 stikeDirection;
	protected int attackLayer;
	private GameObject hitObject;

	protected StandardBeing thisBeing;
	
	void Start () 
	{
		attackTags = new List<string> ();
		thisBeing = GetComponent<StandardBeing> ();
		Initialise ();
	}

	protected virtual void Initialise()
	{
		fatigueComponent = GetComponent<FatigueComponent> ();
		meleeUnit = GetComponent<HeroBeing> () as IMelee;
		attackTags.Add ("Zombie");
		attackLayer = (1 << 9) | (1 << 10);
	}

	/*public void SetOwner (HeroBehaviour owner){
		this.owner = owner;
	}*/
	
	void AttackRefresh()
	{
		attack_ready = true;
	}

	void AttackHit()
	{

		RaycastHit2D hit = Physics2D.Raycast ((Vector2)transform.position, stikeDirection, range, attackLayer);

		if (hit.transform != null) {
			hitObject = hit.transform.gameObject;
			if (attackTags.Contains(hitObject.transform.tag)) {

				StandardBeing being = hitObject.GetComponent<StandardBeing> ();

				float stun = stunBase + Random.Range (0f, stunRandom);
				being.HitByMelee(damage, stun, stikeDirection * knockback, thisBeing); 


				AttackHitSpecifics(being);
				if (meleeUnit != null)
				{
					meleeUnit.OnAttackHit();
				}
			}
		} 

	}

	protected virtual void AttackSpecifics()
	{
		SoundPlayer.instance.Play (Sounds.airSwing, transform, 2);
	}
	
	protected virtual void AttackHitSpecifics(StandardBeing targetHit)
	{
		SoundPlayer.instance.Play (Sounds.attackHit, transform, 3.8f);
	}

	public void FakeAttack()
	{
		AttackSpecifics();
	}

	public void FakeAttackHit()
	{
		AttackHitSpecifics (null);
	}
	
	public bool Attack(Vector2 FacingVector)
	{
		if (attack_ready) {
			AttackSpecifics();


			stikeDirection = FacingVector;
			Invoke("AttackHit",0.085f);
			attack_ready = false;

			float attack_period = attackPeriod;

			if (fatigueComponent != null)
			{
				fatigueComponent.Fatigue -= 6;
				if (fatigueComponent.Fatigue == 0)
				{
					attack_period+= 0.45f;
				}
			}
			Invoke("AttackRefresh",attack_period);

			if (meleeUnit != null)
			{
				meleeUnit.OnAttackSwing();
			}
			return true;
		}
		return false;
	}
}

