
using UnityEngine;
using GraveDays.Entity.Being;

public class PlayerMeleeComponent: MeleeComponent
{
	
	protected override void AttackSpecifics()
	{
		SoundPlayer.instance.PlayForced (Sounds.airSwing);
	}
	
	protected override void AttackHitSpecifics(StandardBeing being)
	{
		SoundPlayer.instance.PlayForced (Sounds.attackHit);
	}
	

}

