﻿using UnityEngine;
using System.Collections;
using System;
using GraveDays.Items;
using GraveDays.Items.Guns;

[Serializable]
public class SaveDataGunStats
{
	public int gunID;
}


public class GunComponent : MonoBehaviour {
	
	private bool shoot_ready = true;
	protected EquipComponent equipComponent;
	public Sounds gunShotSound;
	public IGunner gunner;
	

	public int Ammo 
	{ 
		get
		{
			return inventory.GetItemCount(ItemType.Ammo);
	
		}
	}
	protected InventoryComponent inventory;

	public ItemGun GunType {
		get
		{
			ItemEquippable weapon = equipComponent.GetEquipped(EquipType.Weapon);
			if (weapon != null)
			{
				return weapon as ItemGun;
			}
			return null;
		}

	}

	void Awake()
	{
		inventory = GetComponent<InventoryComponent> ();
		equipComponent = GetComponent<EquipComponent> ();

	}

	void Start () {

		gunShotSound = Sounds.bulletShort;
	}

	public SaveDataGunStats GetSaveObject()
	{
		SaveDataGunStats gunSave = new SaveDataGunStats ();
		gunSave.gunID = GunType.ID;

		return gunSave;
	}



	public void LoadFromSaveObject(SaveDataGunStats data)
	{
	//	EquipGun (ItemGun.Guns [data.gunID]);
	}



	/*public void AddAmmo (int value)
	{
		inventory.AddItem (ItemType.Ammo, value);
	}	

	public void RemoveAmmo(int value = 1)
	{
		inventory.RemoveItem(ItemType.Ammo, value);
	}*/

	void ShootRefresh()
	{
		shoot_ready = true;
	}

	public virtual void ShootSpecifics()
	{
		SoundPlayer.instance.Play(gunShotSound, transform, 100);
	}

	public virtual void EmptyGunSpecifics()
	{
		SoundPlayer.instance.Play(Sounds.gunEmptyClick, transform, 5f);
	}

	public BulletBehaviour InstantiateBullet(Vector3 fireForce)
	{
		BulletBehaviour shot = GameObjectPool.instance.GetBullet(transform.position, transform.rotation);
		shot.rigidbody2D.AddForce (fireForce);
		return shot;
	}

	public void AssignBullet(BulletBehaviour bullet)
	{
		bullet.master = transform;
		bullet.Damage = GunType.ShotDamage;
		bullet.Penetration = GunType.Penetration;
	}
	
	public bool Shoot(Vector2 direction) //return true when out of ammo
	{

		if (shoot_ready && GunType != null) {
			if (Ammo > 0)
			{
				ShootSpecifics();
				GunType.Shoot(this,direction,gunner,inventory);
				shoot_ready = false;
				Invoke ("ShootRefresh", GunType.ShootPeriod);

			}
			else {
				EmptyGunSpecifics();
				shoot_ready = false;
				Invoke ("ShootRefresh", GunType.ShootPeriod);
//				gunner.OnAttemptShootEmpty();
				return true;
			}



		} 

		return false;
	}
}
