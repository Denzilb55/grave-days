using System;
using System.Collections.Generic;
using GraveDays.Items;
using UnityEngine;
using GraveDays.Entity.Being;

public class EquipComponent : MonoBehaviour
{
	//public ItemEquippable [] equippedItems = new ItemEquippable[BaseItem.EQUIP_COUNT];
	public Dictionary<EquipType, ItemEquippable> equippedItems = new Dictionary<EquipType, ItemEquippable>();
	IEquippor equipor;

	void Awake()
	{
		HeroBeing hero = GetComponent<HeroBeing> ();

		if (hero != null) {
			equipor = hero as IEquippor;
		} else {
			equipor = GetComponent<SurvivorBeing>() as IEquippor;
			if (equipor == null)
			{
				equipor = GetComponent<AllyBehaviour>() as IEquippor;
			}
		}
	}
	 

	public bool IsEquipped(EquipType type)
	{
		return equippedItems.ContainsKey (type);
	}

	public bool IsEquipped (ItemEquippable item)
	{
		return equippedItems.ContainsValue (item);
	}

	public ItemEquippable GetEquipped (EquipType type)
	{
		ItemEquippable equipped = null;
		equippedItems.TryGetValue (type, out equipped);
		return equipped;
	}

	public void Equip (ItemEquippable item)
	{
		ItemEquippable equipped = GetEquipped (item.GetEquipType ());
		if (equipped != null) 
		{
			Unequip(equipped);
		}

		item.EquipAction (equipor);
		equippedItems.Add (item.GetEquipType (), item);
	}

	public void Unequip (ItemEquippable item)
	{
		item.UnequipAction (equipor);
		equippedItems.Remove (item.GetEquipType ());
	}


}


