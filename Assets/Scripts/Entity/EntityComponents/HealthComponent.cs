//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.1
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using UnityEngine;

[Serializable]
public class SaveDataHealthStats
{
	public float maxHealth;
	public float health;
	public float maxLimit;
	public float healthRecovery;
}

public class HealthComponent: MonoBehaviour
{

	private IHealthReactive reactant;
	public float MaxHealth { get; private set; }
	public float Health { get; private set; }
	public float MaxLimit { get; set; }
	public float HealthRecovery { get; set; }


	void Update()
	{
		if (HealthRecovery > 0) {
			RecoverHealth(HealthRecovery * Time.deltaTime);
		}
	}


	public void Initialise(IHealthReactive reactant, float maxHealth)
	{
		this.reactant = reactant;
		MaxHealth = maxHealth;
		Health = MaxHealth;
		MaxLimit = 200;
	}

	public void Initialise(IHealthReactive reactant, float maxHealth, float maxLimit)
	{
		this.reactant = reactant;
		MaxHealth = maxHealth;
		Health = MaxHealth;
		MaxLimit = maxLimit;
	}

	public SaveDataHealthStats GetSaveObject()
	{
		SaveDataHealthStats healthSave = new SaveDataHealthStats ();
		healthSave.maxHealth = MaxHealth;
		healthSave.health = Health;
		healthSave.maxLimit = MaxLimit;
		healthSave.healthRecovery = HealthRecovery;
		return healthSave;
	}

	public void LoadFromSaveObject(SaveDataHealthStats data)
	{
		Health = data.health;
		MaxHealth = data.maxHealth;
		MaxLimit = data.maxLimit;
		HealthRecovery = data.healthRecovery;
	}

	public void TakeDamage(float amount)
	{
		if (Health <= 0)
			return;

		Health -= amount;

		if (Health <= 0) {
			reactant.OnDied();
		} else {
			reactant.TakeDamageCallback (amount);
		}

	}

	public void DecreaseMaxHealth(float value)
	{
		MaxHealth -= value;
		if (Health > MaxHealth) {
			Health = MaxHealth;

			if (Health <= 0) {
				reactant.OnDied();
			} 
		}
	}

	public void IncreaseMaxHealth(float value)
	{
		MaxHealth += value;

		if (MaxHealth > MaxLimit) {
			MaxHealth = MaxLimit;
		}
	}

	public void RecoverHealth(float amount)
	{
		Health += amount;

		if (Health > MaxHealth) {
			Health = MaxHealth;
		}
	}
	
	public bool IsDead()
	{
		return Health <= 0;
	}
	



}


