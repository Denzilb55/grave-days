using System;

namespace GraveDays.Entity
{
	public interface ILocalAffector: ITargetable
	{
		void AssignAffected(ILocalAffector affected);
		void UnassignAffected(ILocalAffector affected);
	}
}


