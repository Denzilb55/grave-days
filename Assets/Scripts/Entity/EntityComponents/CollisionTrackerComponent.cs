﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GraveDays.Entity.Being;
using GraveDays.Entity;

public class CollisionTrackerComponent : MonoBehaviour {

	protected AiBeing being;
	public List<ITargetable> collisions = new List<ITargetable>();
	protected List<string> enemyTags = new List<string>();


	void Awake()
	{
		being = gameObject.GetComponentInParent<AiBeing> ();
		enemyTags.Clear ();
	}

	public void RemoveCollision(ITargetable target)
	{
		collisions.Remove (target);
	}
	
	public void Refresh()
	{
		collisions.Clear ();
	}

	public void AddEnemyTag(string tag)
	{
		enemyTags.Add (tag);
	}

	void AddTarget(ITargetable target)
	{
		//if (collisions
		if (!Network.isClient && collisions.Count == 0) {
			//if (being.GetTarget() != target) {
			//	being.ReleaseTarget();
			being.SetAttackTarget(target);
			//}
		}
		collisions.Add (target);
	}

	void RemoveTarget(ITargetable target)
	{
		collisions.Remove (target);
		if (being.GetAttackTarget() == target && collisions.Count > 0) {
			/*if ((collisions[0].GetTransform().position - being.transform.position).magnitude > 5)
			{
				Debug.Log("WTF");
			}*/
			being.SetAttackTarget(collisions[0]);
		}

	}

	void OnTriggerEnter2D(Collider2D collision) {
		if (enemyTags.Contains(collision.tag)) {
			AddTarget(collision.GetComponent<StandardBeing>());
		}
	}

	void OnTriggerExit2D(Collider2D collision) {
		if (enemyTags.Contains(collision.tag)) {
			RemoveTarget(collision.GetComponent<StandardBeing>());
		}
	}


}
