﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class SaveDataMovementStats
{
	public float baseSpeed;
	public float modifierSpeed;
}

[Serializable]
public class SerializableVector2
{
	public float x;
	public float y;
	
	public SerializableVector2(float x, float y)
	{
		this.x = x;
		this.y = y;
	}
	
	// User-defined conversion from Serial to Vec2 
	public static implicit operator Vector2(SerializableVector2 d)
	{
		return new Vector2(d.x, d.y);
	}
	//  User-defined conversion from Vec2 to Serial 
	public static implicit operator SerializableVector2(Vector2 d)
	{
		return new SerializableVector2(d.x, d.y);
	}

	// User-defined conversion from Serial to Vec2 
	public static implicit operator Vector3(SerializableVector2 d)
	{
		return new Vector3(d.x, d.y, 0);
	}
	//  User-defined conversion from Vec2 to Serial 
	public static implicit operator SerializableVector2(Vector3 d)
	{
		return new SerializableVector2(d.x, d.y);
	}
}

public enum MovementType {none = 0, walking, running}
public class MovementComponent : MonoBehaviour  {
	public float MoveSpeed {
		get { return baseSpeed + modifierSpeed; }
	}
	
	private float baseSpeed;
	private float modifierSpeed;

	public bool IsMoving { get; private set; }
	public Vector2 MovingVector { get; private set; }

	
	public bool IsSprinting { get; private set; }

	public SaveDataMovementStats GetSaveObject()
	{
		SaveDataMovementStats movementStats = new SaveDataMovementStats ();
		movementStats.baseSpeed = baseSpeed;
		movementStats.modifierSpeed = modifierSpeed;

		return movementStats;
	}

	public void LoadFromSaveObject(SaveDataMovementStats data)
	{
		baseSpeed = data.baseSpeed;
		modifierSpeed = data.modifierSpeed;
	}

	public MovementType MoveType {
		get{
			if (IsMoving)
			{
				if (IsSprinting)
				{
					return MovementType.running;
				}
				else
				{
					return MovementType.walking;
				}
			}
			else
			{
				return MovementType.none;
			}
		}
	}
	private float sprintSpeed;

	public void SetBaseSpeed(float val)
	{
		baseSpeed = val;
	}

	public void AddMovementModifier(float val)
	{
		modifierSpeed += val;
	}

	public void ManualUpdate()
	{
		if (IsMoving) {
			Vector2 moveForce = MovingVector * Time.deltaTime * 60 * MoveSpeed;
			rigidbody2D.AddForce (moveForce);
		} 
	}

	public void Move(Vector2 moveDirection)
	{
		MovingVector = moveDirection.normalized;
		IsMoving = true;
	}

	public void Sprint(float additionalSpeed)
	{
		if (!IsSprinting) {
			IsSprinting = true;
			sprintSpeed = additionalSpeed;
			modifierSpeed += additionalSpeed;
		}

	}

	public void StopSprint()
	{
		if (IsSprinting) {
			IsSprinting = false;
			modifierSpeed -= sprintSpeed;
		}
	}

	public void Stop()
	{
		IsMoving = false;
		StopSprint ();
	}
}
