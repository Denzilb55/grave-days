using UnityEngine;
using GraveDays.Entity.Being;

public class MeleeComponentZombie: MeleeComponent
{
	protected override void Initialise()
	{
		attackTags.Add ("Player");
		attackTags.Add ("Survivor");
		attackTags.Add ("Ally");
		attackPeriod = 0.65f;
		knockback = 0;
		damage = 0.8f;
		stunBase = 0.05f;
		stunRandom = 0.2f;
		attackLayer = (1 << 11) | (1 << 17) | (1 << 22);
	}

	void Awake()
	{


	}

	protected override void AttackSpecifics()
	{
		//SoundPlayer.instance.PlayForced (Sounds.airSwing);


	}
	
	protected override void AttackHitSpecifics(StandardBeing targetHit)
	{
		EffectsComponent effects = targetHit.GetEffectsComponent();
		
		if (effects != null) {
			float randomVal = UnityEngine.Random.Range(0,6f);
			if (randomVal < 3) {
				effects.AddBleed(0.6f);
				
				if (randomVal < 1) {
					effects.AddInfected(randomVal*16);
				}
			}
		}

	}
	
	
}
