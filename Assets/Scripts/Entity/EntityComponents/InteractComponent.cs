﻿using UnityEngine;
using System.Collections;
using GraveDays.Entity.Being;

public abstract class InteractComponent : MonoBehaviour {



	public virtual bool Interact(HeroBeing hero)
	{
		if (((Vector2)(hero.transform.position) - (Vector2)(transform.position)).sqrMagnitude < 5) {
			return true;
		}

		return false;
	}




}
