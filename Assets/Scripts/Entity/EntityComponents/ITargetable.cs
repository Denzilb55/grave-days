using System;
using UnityEngine;

namespace GraveDays.Entity
{
	public interface ITargetable
	{
		float GetExposure();
		float GetSqrExposure();
		Transform GetTransform();
		void IncreaseExposure(float value);
		void ReduceBaseExposureSightLost(float originalValue);
		Vector2 GetMovingVector();
		bool IsValid();
		bool IsMoving();
		HealthComponent GetHealthComponent();
		EffectsComponent GetEffectsComponent();
		void AssignTargeter(ITargeter targeter);
		void UnassignTargeter(ITargeter targeter);
	}
}


