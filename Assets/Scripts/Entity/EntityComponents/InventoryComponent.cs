﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[Serializable]
public class SaveDataInventory
{
	public ItemType [] items;
	public int [] amounts;

	public SaveDataInventory(int count)
	{
		items = new ItemType[count];
		amounts = new int[count];
	}
}


public class InventoryComponent : MonoBehaviour, ILateProcess {
	
	public Dictionary<ItemType,int> items = new Dictionary<ItemType, int>();
	private List<ItemType> affectedItems = new List<ItemType> ();
	private List<int> newAmounts = new List<int>();
	public float maxCarryWeight = 40;
	public float currentWeight;

	public SaveDataInventory GetSaveObject()
	{
		SaveDataInventory inventorySave = new SaveDataInventory (items.Count);
		int c = 0;
		foreach (KeyValuePair<ItemType,int> entry in items) {
			inventorySave.items[c] = entry.Key;
			inventorySave.amounts[c] = entry.Value;
			c++;
		}

		return inventorySave;
	}
	
	void Awake()
	{
		if (ManagerScript.doLoad) {
			items.Clear();
		}
	}
	
	public void LoadFromSaveObject(SaveDataInventory data)
	{
		items.Clear();
		for (int c = 0; c < data.items.Length; c++)
		{
			items.Add(data.items[c],data.amounts[c]);

		}
	}

	public int AddItem(IItem item, int amount = 1)
	{
		return AddItem (item.GetItemType(), amount);
	}

	public static int TransferItem (IItem item, InventoryComponent fromInv, InventoryComponent toInv, ref int amount)
	{
		return TransferItem (item.GetItemType (), fromInv, toInv, ref amount);

	}

	public static int TransferItem (ItemType item, InventoryComponent fromInv, InventoryComponent toInv, ref int amount)
	{
		int space = toInv.SpaceFor (item);

		if (amount > space) {
			amount = space;
		}

		int count = fromInv.GetItemCount (item);

		if (count < amount) {
			amount = count;
		}


		toInv.AddItem (item, amount);

		 if (ManagerScript.networkType != NetworkType.Single) {
			toInv.networkView.RPC ("RPCAddItem", RPCMode.Others, (int)item, amount);
			fromInv.networkView.RPC ("RPCRemoveItem", RPCMode.Others, (int)item, amount);
		}

		return fromInv.RemoveItem (item, amount);
	}

	public int SpaceFor(ItemType item)
	{
		return (int)((maxCarryWeight - currentWeight) / item.GetItemInstance ().GetWeight ());
	}

	public int AddItem(ItemType item, int amount = 1)
	{
		int current;
		int toTake = SpaceFor (item);

		if (amount > toTake) {
			amount = toTake;
		}

		if (amount == 0) {
			return 0;
		}

		if (items.TryGetValue (item, out current)) {
			items[item] = current + amount;
		} else {
			items.Add (item, amount);
		}

		currentWeight += amount * item.GetItemInstance ().GetWeight ();
		return amount;
	}

	public int GetItemCount(IItem item)
	{
		return GetItemCount (item.GetItemType ());
	}

	public int GetItemCount(ItemType item)
	{
		int amount = 0;
		if (items.TryGetValue (item, out amount)) {
			return amount;
		} else {
			return 0;
		}
	}




	public int RemoveItem(IItem item, int amount = 1)
	{
		return RemoveItem (item.GetItemType(), amount);
	}
	
	public int RemoveItem(ItemType item, int amount = 1) //return remaining count
	{
		int current;
		if (items.TryGetValue (item, out current)) {
			if (current >= amount)
			{
				LateRemove(item, current - amount);
				currentWeight -= amount * item.GetItemInstance ().GetWeight ();
				return current - amount;
			}
			else {
				LateRemove(item, 0);
				currentWeight -= (amount - current) * item.GetItemInstance ().GetWeight ();
				return 0;
			}


		} 
		return 0;
	}



	void LateRemove(ItemType item, int amount)
	{
		affectedItems.Add(item);
		newAmounts.Add(amount);
		ManagerScript.instance.lateProcesses.Add (this);
	}

	public void Process()
	{
		int c = 0;
		foreach (var item in affectedItems) {
			
			if (newAmounts[c] == 0)
			{
				items.Remove(item);
			}
			else {
				items[item] = newAmounts[c];
			}
			c++;
		}
		affectedItems.Clear ();
		newAmounts.Clear ();
	}

	/*public int RemoveItemImmediate(IItem item, int amount = 1)
	{
		return RemoveItemImmediate (item.GetItemType(), amount);
	}
	
	public int RemoveItemImmediate(ItemType item, int amount = 1)
	{
		int current;
		if (items.TryGetValue (item, out current)) {
			if (current > amount)
			{
				//affectedItems.Add(item);
				//newAmounts.Add(current - amount);
				items[item] = current - amount;
				return amount;
			}
			else {
				//affectedItems.Add(item);
				//newAmounts.Add(0);
				items.Remove(item);
				return (current - amount);
			}
		} 
		return 0;
	}*/
	
}
