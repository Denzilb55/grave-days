using System;
using UnityEngine;
using System.Collections.Generic;
using GraveDays.Items;
using GraveDays.Entity.Being;


public class InteractComponentSurvivor : InteractInventoryComponent, IOptioned
{
	private SurvivorBeing survivorBeing;
	private string follow = "F: Follow";
	private string stopFollow = "F: Stop Following";
	private string inspect = "I: Inspect";
	private string holdPosition = "H: Hold Position";

	private int _trustRating;
	public int TrustRating {
		get {
			return _trustRating;
		}

		set {
			_trustRating = value;

			if (_trustRating > 100)
			{
				_trustRating = 100;
			}
			else if (_trustRating < 0)
			{
				_trustRating = 0;
			}
		}
	}

	

	private bool survivorInspect;

	public override void NotifyClose()
	{

	}

	
	protected override string GetInventoryTitle ()
	{
		return "Inspect";
	}
	protected override string GetPutTitle ()
	{
		return "You";
	}
	protected override string GetTakeTitle ()
	{
		return name;
	}

	protected override void Initialise()
	{
		survivorBeing = GetComponent<SurvivorBeing> ();
		TrustRating = UnityEngine.Random.Range (25, 80);

	}


	void OnDestroy()
	{
	//	survivorNames.Add (name);
	}

	void Start()
	{
		GetComponent<ToolTipComponent>().ToolTip = "E: "+name;
	}

	public override bool Interact(HeroBeing hero)
	{
		if (base.Interact (hero)) {

			survivorBeing.Interact(hero);
			survivorInspect = false;
			return true;
		}
		
		return false;
	}

	public override void DrawMenu()
	{
		if (survivorInspect) {
			Rect rect = new Rect (Screen.width / 2 - 80, Screen.height / 2 - 60, 160, 120);
			
			base.DrawMenu();
		} else {
			Rect rect = new Rect (Screen.width / 2 - 65, Screen.height / 2 - 50, 130, 100);
			
			GUILayout.BeginArea (rect);
			if (!survivorBeing.IsFollowing (GlobalData.instance.player)) {
				GUILayout.Box (follow);
			} else {
				GUILayout.Box (stopFollow);
				GUILayout.Box (holdPosition);
			}
			GUILayout.Box (inspect);

			GUILayout.EndArea ();
		}
	}

	public override void DrawSubStuff(GUIStyle subTitleStyle)
	{
		GUILayout.Label ("Trust: " + TrustRating, subTitleStyle);
		HealthComponent health = survivorBeing.GetHealthComponent ();
		GUILayout.Label ("Health: " + health.Health.ToString("f1") + " / " + health.MaxHealth, subTitleStyle);

	}


	public override void HandleInputs()
	{
	//	Debug.Log ("INPUTS");
		if (Input.GetKeyDown (KeyCode.F)) {

			if (!survivorBeing.IsFollowing (GlobalData.instance.player)) {
				if (TrustRating >= 40)
				{



					if (ManagerScript.networkType == NetworkType.Single)
					{
						survivorBeing.Follow (GlobalData.instance.player);
					} 
					else {
						survivorBeing.networkView.RPC("StopFollow", RPCMode.All, false);
						survivorBeing.Follow (GlobalData.instance.player);
						if (Network.isClient)
						{
							survivorBeing.networkView.RPC("FollowAlly", RPCMode.Server, Network.player);
						}
					}
				}
				else {
					//SoundPlayer.instance.Play(Sounds.dontTrustYou);
					survivorBeing.speechComponent.SayNotFollow();
				}
			} else {
				if (ManagerScript.networkType == NetworkType.Single)
				{
					survivorBeing.ReleaseFollowTarget ();
				} 
				else {
					survivorBeing.networkView.RPC("ReleaseFollowTarget", RPCMode.All, true);
				}
			}
			

		} else if (Input.GetKeyDown (KeyCode.I)) {
			survivorInspect = true;
		}
		else if (Input.GetKeyDown (KeyCode.H)) {
			survivorBeing.HoldPosition(true);
		}
	}

	public override void TakeItem(HeroBeing interactor, IItem item, int amount = 1)
	{
		if ((!item.IsRare() && TrustRating >= 60) || TrustRating >= 80) {
			survivorBeing.speechComponent.SayGive();

			int remains = InventoryComponent.TransferItem(item, inventory, interactor.inventory, ref amount);

			if (remains == 0 && item.IsEquippable()) {
				GraveDays.Items.ItemEquippable equipItem = (item as GraveDays.Items.ItemEquippable);
				
				survivorBeing.Unequip(equipItem);
			}

			if (item.IsRare ()) {
				TrustRating -= 35;
			} else {
				TrustRating -= 4;
			}
		} else {
			survivorBeing.speechComponent.SayNotGive();
		}
	}
	
	public override void DepositItem(HeroBeing interactor, IItem item, int amount = 1)
	{
		if (item.IsRare ()) {
			TrustRating += 30;
		} else {
			TrustRating += 3;
		}
		int itemCount = interactor.inventory.GetItemCount (item);
		if (amount > itemCount) {
			amount = itemCount;
		}
		
		int remains = InventoryComponent.TransferItem(item, interactor.inventory, inventory, ref amount);

		if (item.IsEquippable())
		{
			ItemEquippable equipItem = (item as GraveDays.Items.ItemEquippable);
			if (remains == 0) {
				if (interactor.equipComponent.IsEquipped(equipItem))
				{
					interactor.equipComponent.Unequip(equipItem);
				}
			}

			if (inventory.GetItemCount(equipItem.GetItemType()) > 0) {
				survivorBeing.Equip(equipItem);
			}
		}

		survivorBeing.speechComponent.SayTake ();
	}
	

}


