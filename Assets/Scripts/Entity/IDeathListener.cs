using System;

namespace GraveDays.Entity
{
	public interface IDeathListener
	{
		void NotifyDeath(ITargetable target);
	}
}

