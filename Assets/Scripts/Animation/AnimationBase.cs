﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AnimationBase : MonoBehaviour {
	public List<Texture> frameTex = new List<Texture>();
	public Material[] frameMat;

	public float _totalAnimationPeriod;
	public float TotalAnimationPeriod {
		get { return _totalAnimationPeriod; }
		set {
			_totalAnimationPeriod = value;
			AnimationFramePeriod = _totalAnimationPeriod/frameTex.Count;
		}
	}

	public float AnimationFramePeriod{ get; private set; }


	void Awake()
	{
		frameMat = new Material[frameTex.Count];
		AnimationFramePeriod = _totalAnimationPeriod/frameTex.Count;
	}
	 

}
