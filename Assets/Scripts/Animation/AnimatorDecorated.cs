using System;

namespace GraveDays.Animation
{
	public abstract class AnimatorDecorated: AnimatorComponent
	{
	
		protected virtual void  UpdateAnimationState ()
		{
	
		}

		public override void UpdateAnimation()
		{
			UpdateAnimationState ();
			base.UpdateAnimation ();
		}

		public virtual void DoPunch()
		{
			SetAnim (AnimationsType.TopDownJoePunch, false);
		}

	}
}

