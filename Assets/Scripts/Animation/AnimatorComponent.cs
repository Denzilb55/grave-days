﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GraveDays.Entity.Being;

namespace GraveDays.Animation
{
	public enum FacingDirection {Up = 0, Right, Down, Left}
	public enum AnimationDecorator {Undefined = -1,Unarmed = 0, Glock, Rifle, Shotgun}
	public enum AnimationType {Default = 0, Walking, Standing, Melee}

	public abstract class AnimatorComponent : MonoBehaviour 
	{
		protected AnimationsType currentAnim;
		protected AnimationsType defaultAnimation;
		private float animationTime;
		private int key = 0;
		protected bool keepLastFrame;
		protected bool loop;
		protected StandardBeing being;

		protected float animSpeedMultiplier ;

		void Start()
		{
			loop = true;
			SetAnim (GetDefaultAnim ());
			animSpeedMultiplier = 1;
			currentAnim = GetDefaultAnim ();//.GetAnimation();
			defaultAnimation = GetDefaultAnim ();
			being = GetComponent<StandardBeing> ();
		}

		protected abstract AnimationsType GetDefaultAnim ();

		protected virtual void DoAnimationStart()
		{
		
		}
		
		protected virtual void  DoAnimationEnd()
		{

		}

		public void SetAnimSpeedMultiplier (float value)
		{
			animSpeedMultiplier = value;
		}

		public virtual void UpdateAnimation()
		{
			TickAnimation ();
		}

		protected void SetAnim(AnimationsType anim, bool loop = true)
		{
			if (currentAnim != anim && this.loop == true) {
				DoAnimationStart();
				currentAnim = anim;
				key = 0;
				ApplyFrame();
				this.loop = loop;

				if (loop) {
					defaultAnimation = currentAnim;
				}
			} 	
		}

		private void HardDefault()
		{
			currentAnim = defaultAnimation;
			key = 0;
			loop = true;
		}

		private bool TickAnimation()
		{

			animationTime += Time.deltaTime;
			if (animationTime >= currentAnim.GetAnimation().AnimationFramePeriod * animSpeedMultiplier)
			{
				if (key < currentAnim.GetAnimation().frameMat.Length -1) {
					key++;
					ApplyFrame ();
					return false;
				}
				else {
					DoAnimationEnd();
					if (loop) {
						key = 0;
	
					}
					else {
						HardDefault();
						if (!keepLastFrame) {
							ApplyFrame ();
						}
					}
					//return true;
				}

			}
			return false;
		}
		

		void ApplyFrame()
		{
			renderer.sharedMaterial = currentAnim.GetAnimation().frameMat [key];
			animationTime = 0;
		}


		public static FacingDirection GetFacingDirection(Vector3 facingVector)
		{
			float angle = Mathf.Atan2(facingVector.x,  facingVector.y);
			
			
			if (Mathf.Abs (angle) < Mathf.PI / 4) { //UP
				return FacingDirection.Up;
			} else if (angle < Mathf.PI * 3 / 4 && angle > 0) { //RIGHT
				return FacingDirection.Right;
			} else if (angle > -Mathf.PI * 3 / 4 && angle < 0) { //LEFT
				return FacingDirection.Left;
			} else { //DOWN
				return FacingDirection.Down;
			}
			
		}
	}
}
