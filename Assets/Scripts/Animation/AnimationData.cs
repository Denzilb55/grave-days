﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum AnimationsType {HeroIdleDown = 0, HeroWalkDown, HeroIdleUp, HeroWalkUp, HeroIdleRight, HeroWalkRight, HeroIdleLeft, HeroWalkLeft,
						ZombieWalk, ZombieIdleTop, BloodSplatter, TopDownJoePunch, ChestOpen, ChestClosed, ZombieWalkDown, ZombieIdleDown,
						Surv1WalkDown, Surv1WalkUp, Surv1IdleUp, Surv1WalkRight, Surv1IdleRight, Surv1IdleLeft, Surv1WalkLeft, Surv1IdleDown,
						WalkFDown, WalkFUp,IdleFUp, WalkFRight, IdleFRight,IdleFLeft,WalkFLeft,IdleFDown,
	WalkDownSoldier, WalkUpSoldier,IdleUpSoldier, WalkRightSoldier, IdleRightSoldier, IdleLeftSoldier, WalkLeftSoldier, IdleDownSoldier, 
	TopDownUnarmedJoe, TopDownUnarmedJoeIdle, TopDownShotgunJoe, TopDownShotgunJoeIdle, TopDownAKJoe, TopDownAKJoeIdle, TopDownGlockJoe, TopDownGlockJoeIdle}

public static class AnimationsTypeMethods
{
	public static AnimationBase GetAnimation(this AnimationsType a)
	{
		return AnimationData.instance.animations [(int)a];
	}

}

public class AnimationData : MonoBehaviour {


	public List<AnimationBase> animations = new List<AnimationBase>();
	public static AnimationData instance;


	public static AnimationBase GetAnimation(AnimationsType anim)
	{
		return instance.animations[(int)anim];
	}

	// Use this for initialization
	void Awake () {
		instance = this;
		List<Texture> uniqueTexs = new List<Texture> ();
		foreach (AnimationBase ani in animations) {
			foreach (Texture tex in ani.frameTex)
			{
				if (!uniqueTexs.Contains(tex))
				{
					uniqueTexs.Add(tex);
				}
			}

		}

		foreach (Texture tex in uniqueTexs) {
			Material mat = new Material(Shader.Find("2DVLS/Cutout"));
			mat.mainTexture = tex;

			foreach (AnimationBase ani in animations) {
				int c = 0;
				foreach (Texture aniTex in ani.frameTex)
				{

					if (aniTex == tex)
					{
						ani.frameMat[c] = mat;
					}
					c++;
				}
			}
		}


		/*foreach (Texture tex in zombieTextures) {
			Material mat = new Material(Shader.Find("2DVLS/Diffuse"));
			mat.mainTexture = tex;
			zombieFramesMat.Add(mat);
		}*/


//		UnityEditor.AssetDatabase.CreateAsset (mat, "Assets/newMat1.mat");

	}
	
	// Update is called once per frame
	void Update () {
	
	}


}
