//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.1
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using System.Linq;



public class SoundPlayer: MonoBehaviour
{
	private List<AudioSource> primarySources = new List<AudioSource>();

	//public List<AudioClip> sounds = new List<AudioClip>();
	private List<AudioSource> secondarySources = new List<AudioSource>();
	private List<AudioSource> availableSources = new List<AudioSource>();
	private List<List<AudioSource>> spesificSources = new List<List<AudioSource>> ();
	public int [] distantSounds;
	private List<int> spesificSourceCount = new List<int> ();

	//public List<Sounds> resolveDistantSounds = new List<Sounds> ();
	//public List<int> distantSoundsLength = new List<int>();

	public static SoundPlayer instance;
	private AudioSource primarySource;

	private int soundsCount;

	void Awake()
	{
		instance = this;
		secondarySources.Clear ();
		primarySources.Clear ();

		primarySource = gameObject.AddComponent<AudioSource> ();

		soundsCount = (int)Enum.GetValues(typeof(Sounds)).Cast<Sounds>().Last() + 1;
		//distantSounds = new int [soundsCount];
		for (int i = 0; i < soundsCount; i++) {
			spesificSources.Add(new List<AudioSource>());
			spesificSourceCount.Add(3);
		}

		SetSpesificCount (1, Sounds.hereYouGo);
		SetSpesificCount (1, Sounds.humanOkaySad);
		SetSpesificCount (1, Sounds.thankYou);
		SetSpesificCount (1, Sounds.cantLetYouDoThat);
		SetSpesificCount (1, Sounds.bagZip);
		SetSpesificCount (1, Sounds.dontTrustYou);

		for (int i = 0; i < 8; i++) {
			primarySources.Add(gameObject.AddComponent<AudioSource>());
			secondarySources.Add(gameObject.AddComponent<AudioSource>());
			availableSources.Add(secondarySources[i]);
		}



	}

	void SetSpesificCount(int count, Sounds sound)
	{
		spesificSourceCount [(int)sound] = count;
	}

	void Update()
	{
		availableSources.Clear ();
		foreach (AudioSource AS in secondarySources) {
			if (!AS.isPlaying)
			{
				availableSources.Add(AS);
			}
		}



	}

	public void PlayForced(Sounds sound, float volume = 1)
	{
		if (volume > 1)
		{
			volume = 1;
		}
		primarySource.PlayOneShot (GlobalData.instance.sounds [(int)sound], volume);
	}



	public static float GetVolumeOverDistance(Transform sourceTransform, float volumeBoost = 4)
	{
		return volumeBoost/((Vector2)(sourceTransform.position - Camera.main.transform.position)).sqrMagnitude;
	}

	public static float GetVolumeOverDistance(Vector2 sourcePos, float volumeBoost = 4)
	{
		return volumeBoost/((sourcePos - (Vector2)Camera.main.transform.position)).sqrMagnitude;
	}

	public void Play(Sounds sound, Transform sourceTransform, float volumeBoost = 4)
	{
		float volume = GetVolumeOverDistance (sourceTransform, volumeBoost);
		if (volume > 0.007f) {
			Play (sound, volume);
		}
	}

	public void Play(Sounds sound, Vector2 sourcePos, float volumeBoost = 4)
	{
		float volume = GetVolumeOverDistance (sourcePos, volumeBoost);
		if (volume > 0.007f) {
			Play (sound, volume);
		}
	}



	public void Play(Sounds sound, float volume = 1)
	{
		int soundIndex = (int)sound;
		if (volume < 0.08f && volume > 0.007f) {
		
			PlaySecondary(sound, volume);
			return;
			
		}
		for (int i = 0; i < spesificSourceCount[soundIndex]; i++) {
			AddSource (soundIndex, i+1);
			if (!spesificSources [soundIndex] [i].isPlaying) {
				spesificSources [soundIndex] [i].volume = volume;
				spesificSources [soundIndex] [i].Play ();
				break;
			}
		}
		
	}
	

	void AddSource(int index, int sourceCount)
	{
		if (spesificSources [index].Count < sourceCount) {
			spesificSources [index].Add(gameObject.AddComponent<AudioSource>());
			spesificSources [index][sourceCount-1].clip = GlobalData.instance.sounds[index];
		}
	}
	

	public void PlaySecondary(Sounds sound, float volume = 1)
	{
		if (availableSources.Count > 0) {
			if (volume > 1)
			{
				volume = 1;
			}
			availableSources[0].PlayOneShot(GlobalData.instance.sounds [(int)sound], volume);
			availableSources.RemoveAt(0);
		}
	}




}


