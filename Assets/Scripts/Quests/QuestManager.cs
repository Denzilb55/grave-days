
using System;
using System.Collections.Generic;

namespace GraveDays.Quests
{
	public enum QuestType {KillTarget=0, KillZombies}
	public class QuestManager
	{
		public readonly Dictionary<QuestType, List<BaseQuest>> ActiveQuests;

		public QuestManager ()
		{
			ActiveQuests = new Dictionary<QuestType, List<BaseQuest>>();
		}

		public void AddQuest(BaseQuest quest)
		{
			if (!ActiveQuests.ContainsKey(quest.GetQuestType())) {
				ActiveQuests.Add(quest.GetQuestType(), new List<BaseQuest>());
			}
			ActiveQuests[quest.GetQuestType()].Add(quest);
		}

		public void RemoveQuest(BaseQuest quest)
		{
			if (ActiveQuests.ContainsKey(quest.GetQuestType())) {
				ActiveQuests[quest.GetQuestType()].Remove(quest);
			}
		}

		public void AdvanceQuest(BaseQuest quest, QuestData questData)
		{

		}

		public void AdvanceQuest(QuestType questType, QuestData questData=null)
		{
			List<BaseQuest> advanceList = new List<BaseQuest>();
			foreach (BaseQuest quest in ActiveQuests[questType]) {
				advanceList.Add(quest);
			}
			foreach (BaseQuest quest in advanceList) {
				quest.AdvanceQuest(questData);
			}
		}

		public void CompleteQuest(BaseQuest quest)
		{
			RemoveQuest(quest);
		}
		
	}
}

