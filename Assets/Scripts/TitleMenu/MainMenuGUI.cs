using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MainMenuGUI : MonoBehaviour {
	enum MenuState {TitleScreen = 0, HostGame, JoinGame, StartNewGame}
	int connectedPlayers = 1;

	public Canvas menuCanvas;
	public GUISkin guiSkin;
	public AudioClip clip1;
	public AudioSource source;

	int CharacterCount = 6;
	MenuState menuState = MenuState.TitleScreen;
	string ip = "192.168.";
	string playerName = "Nameless";
	// Use this for initialization
	void Start () {
		NetworkScript.peers.Clear ();
		Network.minimumAllocatableViewIDs = 100;
		if (PlayerPrefs.HasKey ("defIP")) {
			ip = PlayerPrefs.GetString("defIP");
		}

		if (PlayerPrefs.HasKey ("playerName")) {
			playerName = PlayerPrefs.GetString("playerName");
		}

		if (PlayerPrefs.HasKey ("retry")) {
			if (Network.isClient)
			{
				menuState = MenuState.JoinGame;
			}
			else if (Network.isServer) {
				menuState = MenuState.HostGame;
			}
			PlayerPrefs.DeleteKey("retry");
		}

		cGUI.OnGUIToggled += EnableMainMenuButtons;
		LoadMainMenu ();
		source = gameObject.AddComponent<AudioSource> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	

	void OnConnectedToServer()
	{
		Debug.Log ("CONNECTED");
		menuState = MenuState.JoinGame;
	}

	void OnDisconnectedFromServer(NetworkDisconnection info)
	{
		menuState = MenuState.TitleScreen;
		NetworkScript.peers.Clear ();
	}

	[RPC] void WelcomePlayer(NetworkPlayer player)
	{
		if (player != Network.player) {
			NetworkScript.peers.Add (player);
		}
	}

	[RPC] void IntroduceToPlayer(NetworkPlayer player)
	{
		if (!NetworkScript.peers.Contains (player)) {
			NetworkScript.peers.Add(player);
		}
	}
	
	[RPC] void FarewellPlayer(NetworkPlayer player)
	{
		NetworkScript.peers.Remove (player);
	}
	
	void OnPlayerConnected(NetworkPlayer player)
	{
		networkView.RPC ("WelcomePlayer", RPCMode.AllBuffered, player); 
		networkView.RPC ("IntroduceToPlayer", player, Network.player);
		Debug.Log ("PLAYER CONNECTED");
	}
	
	void OnPlayerDisconnected(NetworkPlayer player)
	{
		networkView.RPC ("FarewellPlayer", RPCMode.AllBuffered, player); 
		Debug.Log ("PLAYER DISCONNECTED");
	}

	void OnFailedToConnect()
	{
		Debug.Log ("FAILED TO CONNECT");
	}


	

	[RPC]
	void LaunchGame()
	{
		ManagerScript.doLoad = false;
		ManagerScript.PreLoadActions();
		Application.LoadLevel(1);

		#if UNITY_WEBPLAYER
		Screen.fullScreen = true;
		#endif
	}

	void StartHost()
	{
		networkView.RPC ("LaunchGame", RPCMode.All);
	}

	void DrawHostScreen()
	{
		Rect screenRect = new Rect (0, 0, Screen.width, Screen.height);
		GUILayout.BeginArea (screenRect);
		GUILayout.FlexibleSpace();
		GUILayout.BeginHorizontal();
		GUILayout.FlexibleSpace();
		GUILayout.BeginVertical();
		
		GUIStyle titleStyle  = new GUIStyle(GUI.skin.label);
		titleStyle.fontSize = 50;

		GUILayout.Label ("Host Game",titleStyle, GUILayout.Width (500));

		for (int i = 0; i < CharacterCount; i++) {
			if ((PlayerClass)i == PlayerClass.AverageJane) continue;

			if (GUILayout.Button (((PlayerClass)i).ToString(), GUILayout.Width (150))) {
				PlayerPrefs.SetInt ("PlayerClass", i);
			}
		}
		GUILayout.Space (25);


		if (GUILayout.Button ("START", GUILayout.Width (150))) {
			StartHost();
		}
		
		if (GUILayout.Button ("Back", GUILayout.Width (150))) {
			Network.Disconnect();
			menuState = MenuState.TitleScreen;
			connectedPlayers = 1;
		}
		
		GUILayout.Label ("Current Players: " + (1+NetworkScript.peers.Count));
		GUILayout.EndVertical();
		
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		
		GUILayout.FlexibleSpace();
		GUILayout.EndArea();
	}

	void DrawJoinScreen()
	{
		Rect screenRect = new Rect (0, 0, Screen.width, Screen.height);
		GUILayout.BeginArea (screenRect);
		GUILayout.FlexibleSpace();
		GUILayout.BeginHorizontal();
		GUILayout.FlexibleSpace();
		GUILayout.BeginVertical();
		
		GUIStyle titleStyle  = new GUIStyle(GUI.skin.label);
		titleStyle.fontSize = 50;
		GUILayout.Label ("Join Game",titleStyle, GUILayout.Width (500));

		for (int i = 0; i < CharacterCount; i++) {
			if ((PlayerClass)i == PlayerClass.AverageJane) continue;
			if (GUILayout.Button (((PlayerClass)i).ToString(), GUILayout.Width (150))) {
				PlayerPrefs.SetInt ("PlayerClass", i);
			}
		}
		GUILayout.Space (25);

		if (GUILayout.Button ("Back", GUILayout.Width (150))) {
			Network.Disconnect();
			menuState = MenuState.TitleScreen;
			connectedPlayers = 1;
		}
		
		GUILayout.Label ("Current Players: " + (1+NetworkScript.peers.Count));
		GUILayout.EndVertical();
		
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		
		GUILayout.FlexibleSpace();
		GUILayout.EndArea();
	}

	void StartNewGame(PlayerClass pClass)
	{
		PlayerPrefs.SetInt ("PlayerClass", (int)pClass);
		StartNewGame ();
	}

	void StartNewGame()
	{
		ManagerScript.doLoad = false;
		ManagerScript.networkType = NetworkType.Single;
		ManagerScript.PreLoadActions();
		Application.LoadLevel(1);	
		Screen.lockCursor = true;
	}

	void DrawNewGameScreen()
	{
		Rect screenRect = new Rect (0, 0, Screen.width, Screen.height);
		GUILayout.BeginArea (screenRect);
		GUILayout.FlexibleSpace();
		GUILayout.BeginHorizontal();
		GUILayout.FlexibleSpace();
		GUILayout.BeginVertical();
		
		GUIStyle titleStyle  = new GUIStyle(GUI.skin.label);
		titleStyle.fontSize = 50;
		GUILayout.Label ("New Game",titleStyle, GUILayout.Width (500));
		for (int i = 0; i < CharacterCount; i++) {
			if ((PlayerClass)i == PlayerClass.AverageJane) continue;
			if (GUILayout.Button (((PlayerClass)i).ToString(), GUILayout.Width (150))) {

				StartNewGame((PlayerClass)i);
			}
		}

		GUILayout.Space (25);
		if (GUILayout.Button ("Back", GUILayout.Width (150))) {
			menuState = MenuState.TitleScreen;
		}
		

		GUILayout.EndVertical();
		
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		
		GUILayout.FlexibleSpace();
		GUILayout.EndArea();
	}

	void DrawTitleScreen()
	{
		Rect screenRect = new Rect (0, 0, Screen.width, Screen.height);
		GUILayout.BeginArea (screenRect);
		GUILayout.FlexibleSpace();
		GUILayout.BeginHorizontal();
		GUILayout.FlexibleSpace();
		GUILayout.BeginVertical();
		
		GUIStyle titleStyle  = new GUIStyle(GUI.skin.label);
		titleStyle.fontSize = 50;
		GUILayout.Label ("Grave Days",titleStyle, GUILayout.Width (500));
		GUILayout.Label ("Note: Ugly GUI is temporary", GUILayout.Width (500));

		if (ManagerScript.HasSave())
		{
			if (GUILayout.Button ("Continue", GUILayout.Width (150))) {
				ManagerScript.doLoad = true;
				ManagerScript.levelJustLoaded = true;
				ManagerScript.networkType = NetworkType.Single;
				ManagerScript.PreLoadActions();
				Application.LoadLevel(1);
			}
		}

		GUILayout.Label ("Name:");
		playerName = GUILayout.TextField (playerName);

		if (GUILayout.Button ("Start New Game", GUILayout.Width (150))) {
			menuState = MenuState.StartNewGame;
		}
		
		if (GUILayout.Button ("Host Game", GUILayout.Width (150))) {
			NetworkScript.peers.Clear();
			PlayerPrefs.SetString("playerName", playerName);
			bool useNat = !Network.HavePublicAddress();
			ManagerScript.networkType = NetworkType.Server;
			Network.InitializeServer(8, 25000, useNat);
			menuState = MenuState.HostGame;
			NetworkScript.peers.Clear();
		}
		
		GUILayout.Label ("Join IP address:");
		ip = GUILayout.TextField (ip);
		if (GUILayout.Button ("Join Game", GUILayout.Width (150))) {
			//	Network.Connect("127.0.0.1", 25000);
			ManagerScript.networkType = NetworkType.Client;
			PlayerPrefs.SetString("defIP", ip);
			PlayerPrefs.SetString("playerName", playerName);
			NetworkScript.peers.Clear();
			if (Network.Connect(ip, 25000) == NetworkConnectionError.NoError)
			{

			}
		}


		
		if (GUILayout.Button ("Quit", GUILayout.Width (150))) {
			Application.Quit();
		}
		GUILayout.Label ("Music by Kevin MacLeod", GUILayout.Width (500));
		
		GUILayout.EndVertical();
		
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		
		GUILayout.FlexibleSpace();
		GUILayout.EndArea();
	}

	/*void OnGUI()
	{

		switch (menuState) {
		case MenuState.TitleScreen: DrawTitleScreen(); break;
		case MenuState.HostGame: DrawHostScreen(); break;
		case MenuState.JoinGame: DrawJoinScreen(); break;
		case MenuState.StartNewGame: DrawNewGameScreen(); break;
		}

	}*/

	public void QuitGame()
	{
		Application.Quit ();
	}

	public void OptionsClicked()
	{
		cGUI.cSkin = guiSkin;
		cGUI.ToggleGUI ();
		DisableMainMenuButtons ();
		source.PlayOneShot (clip1);
	}

	public void LoadMainMenu()
	{
		EnableMainMenuButtons ();
		DisableNewGameMenuButtons ();
	}

	private void EnableMainMenuButtons()
	{
		EnableButtonSet ("MainMenuButton");
	}

	private void EnableNewGameMenuButtons()
	{
		EnableButtonSet ("NewGameMenuButton");
	}

	private void DisableMainMenuButtons()
	{
		DisableButtonSet ("MainMenuButton");
	}
	
	private void DisableNewGameMenuButtons()
	{
		DisableButtonSet ("NewGameMenuButton");
	}

	private void EnableButtonSet(string typeText)
	{
		for (int i = 0; i < menuCanvas.transform.childCount; i++) {
			Transform child = menuCanvas.transform.GetChild(i);
			if (child.tag == typeText) {
				child.gameObject.SetActive(true);
			}
		}
	}

	private void DisableButtonSet(string typeText)
	{
		for (int i = 0; i < menuCanvas.transform.childCount; i++) {
			Transform child = menuCanvas.transform.GetChild(i);
			if (child.tag == typeText) {
				child.gameObject.SetActive(false);
			}
		}
	}

	public void SelectSoldier()
	{
		PlayerPrefs.SetInt ("PlayerClass", (int)PlayerClass.Soldier);
	}

	public void SelectAverageJoe()
	{
		PlayerPrefs.SetInt ("PlayerClass", (int)PlayerClass.AverageJoe);
	}

	public void SelectFarmer()
	{
		PlayerPrefs.SetInt ("PlayerClass", (int)PlayerClass.Farmer);
	}

	public void SelectLeader()
	{
		PlayerPrefs.SetInt ("PlayerClass", (int)PlayerClass.Leader);
	}

	public void SelectAthlete()
	{
		PlayerPrefs.SetInt ("PlayerClass", (int)PlayerClass.Athlete);
	}
	
	public void NewGameClicked()
	{
		DisableMainMenuButtons ();
		EnableNewGameMenuButtons ();
		source.PlayOneShot (clip1);
	}

	public void StartGameClicked()
	{
		source.PlayOneShot (clip1);
		StartNewGame ();
	}

	public void CreateButton(Transform panel ,Vector3 position, Vector2 size, UnityEngine.Events.UnityAction method)
	{
		GameObject button = new GameObject();
		button.transform.parent = panel;
		button.AddComponent<RectTransform>();
		button.AddComponent<Button>();
		button.transform.position = position;
		button.GetComponent<RectTransform> ().sizeDelta = size;
		//SetSize(size);
		button.GetComponent<Button>().onClick.AddListener(method);
	}
}
