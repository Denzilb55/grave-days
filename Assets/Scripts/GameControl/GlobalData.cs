﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using GraveDays.Entity.Being;

public enum Sounds {gun9mm = 0, footstep1, playerDeath, gunCocking, bandAid, 
	appleBite, earthyFootstep, waterFootstep, chestOpen, bagZip, 
	airSwing, attackHit, gunEmptyClick, footstepWood, chestStore, 
	pageFlip, ak4all, humanHuh, humanOkaySad, humanFollowing, bulletShort, thankYou, cantLetYouDoThat,
	dontTrustYou, hereYouGo,
	FFineTakeIt, FNeededThis, FSeeYouLater, FFollow, FDontTouch, FGoAway, FHi, FScream, airswing2, slap}

public class GlobalData : MonoBehaviour {
	public GameObject bloodSplatter;
	public GameObject zombieTemplate;
	public GameObject healthBoxTemplate;
	public GameObject ammoBoxTemplate;
	public GameObject foodCrate;
	public GameObject survivorMaleTemplate;
	public GameObject survivorFemaleTemplate;
	public GameObject bloodSplashHit;

	private List<string> survivorNamesMale = new List<string>();
	private List<string> survivorNamesFemale = new List<string>();

	public List<GameObject> survivorTypes = new List<GameObject>();

	public HeroBeing player;
	public GameObject playerTemplate;
	public GameObject allyTemplate;
	public static GlobalData instance;
	public GameObject Holder;
	public GameObject background;
	public List<AudioClip> songs = new List<AudioClip> ();
	public List<AudioClip> zombieGrunts = new List<AudioClip>();
	public List<AudioClip> sounds = new List<AudioClip> ();

	public float WorldLeft
	{
		get { return background.collider.bounds.min.x; }
	}

	public float WorldRight
	{
		get { return background.collider.bounds.max.x; }
	}

	public float WorldTop
	{
		get { return background.collider.bounds.max.y; }
	}

	public float WorldDown
	{
		get { return background.collider.bounds.min.y; }
	}

	public bool IsInWorld(Vector2 point)
	{
		if (point.x > WorldLeft && point.x < WorldRight && point.y < WorldTop && point.y > WorldDown) {

			return true;
		}

		return false;
	}



	public string GetRandomMaleName()
	{
		int randomName = UnityEngine.Random.Range (0, survivorNamesMale.Count);
		return survivorNamesMale [randomName];
	}
	
	public string GetRandomFemaleName()
	{
		int randomName = UnityEngine.Random.Range (0, survivorNamesFemale.Count);
		return survivorNamesFemale [randomName];
	}

	// Use this for initialization
	void Start () {

	}

	void Awake()
	{
		instance = this;
		if (ManagerScript.instance != null) {
			ManagerScript.instance.data = this;
		}

		survivorTypes.Add (survivorMaleTemplate);
		survivorTypes.Add (survivorFemaleTemplate);
		
		survivorNamesMale.Clear ();
		survivorNamesFemale.Clear ();
		survivorNamesMale.Add ("Tiaan Naude");
		survivorNamesMale.Add ("Andre Heunis");
		survivorNamesMale.Add ("Denzil Buchner");
		survivorNamesFemale.Add ("Vereese van Tonder");
		survivorNamesFemale.Add ("Geomarr van Tonder");
		survivorNamesFemale.Add ("Charne Prinsloo");
		survivorNamesFemale.Add ("Lindi Vermeulen");
		survivorNamesFemale.Add ("Christine Ahree");
		survivorNamesFemale.Add ("Ujama Swartz");
		survivorNamesFemale.Add ("Kitzy van Tonder");
		survivorNamesMale.Add ("Rohan Buchner");
		survivorNamesMale.Add ("Vincent Rae");
		survivorNamesFemale.Add ("Riki Schutte");
		survivorNamesMale.Add ("Lee Goodrick");
		survivorNamesMale.Add ("Kevin Smith");
		survivorNamesMale.Add ("Jeff Jefferson");
		survivorNamesMale.Add ("Andy Anderson");
		survivorNamesMale.Add ("Peter Peterson");
		survivorNamesMale.Add ("Harris Harrison");
		survivorNamesMale.Add ("Eric Ericson");
		survivorNamesMale.Add ("Jack Jackson");
		survivorNamesMale.Add ("John Boberg");
		survivorNamesMale.Add ("Bongani Mahlangu");
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
