﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class SpecialSpawnSlotBehaviour : ItemHolder {

	public bool UseForRare;
	public List<GameObject> goods = new List<GameObject>(); 
	public GameObject itemObject;
	public PickupItem pickup;
	public float spawnPeriod;
	public float spawnRandom;




	protected override void AwakeInit ()
	{
		spawnTime = GetSpawnTime ();
		
		if (itemObject != null) {
			pickup = itemObject.GetComponent<PickupItem> ();
			pickup.SetItemHolder (this);
		}
	}


	void OnDrawGizmos()
	{
		Gizmos.color = Color.blue;
		Gizmos.DrawSphere(transform.position, 0.5f);
	}

	#region implemented abstract members of ItemHolder

	public override void ItemAdded (PickupItem item)
	{
		pickup = item;
		itemObject = item.gameObject;
	}


	public override void ItemRemoved(PickupItem item)
	{
		if (pickup == item) {
			pickup = null;
			itemObject = null;
			spawnTime = GetSpawnTime ();
		} 

	}

	#endregion

	void Update()
	{
		if (itemObject == null && !Network.isClient) {
			if (spawnTime <= 0) {
				SpawnGoods ();	
			} else {
				spawnTime -= Time.deltaTime;
			}
		}
	}

	protected override void SpawnGoods()
	{
		if (this.itemObject == null && goods.Count > 0) {
			int randomGood = Random.Range (0, goods.Count);

			pickup = ManagerScript.instance.TrySpawnPickup ((Vector2)transform.position, goods [randomGood]);
			if (pickup != null) {
				pickup.SetItemHolder (this);
				itemObject = pickup.gameObject;

			}

		}

	}

	float GetSpawnTime()
	{
		return spawnPeriod + Random.Range (0, spawnRandom);
	}
}
