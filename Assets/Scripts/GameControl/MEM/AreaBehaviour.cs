﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using GraveDays.Entity.Being;

[Serializable]
public class SaveDataArea
{
	public float zombiePool;
	public float infestationLevel;
	public float infestationEndurance;
}

public class AreaBehaviour : MonoBehaviour {
	private int index;
	public static int indexCount = 0;
	public int ZombiePool
	{
		get { return (int)_zombiePool; }
	}

	public float _zombiePool = 10;
	public float infestationLevel = 0.1f;
	public float infestationEndurance = 1f;
	public float infestationSaturate = 2.8f;
	public static float infestationSpawnRate = 0.07f;
	private static float infestationGrowthOverall = 0.000048f;
	private static float infestationStartMultiplifer = 0.98f;
	public float infestationGrowth;
	public float radius;
	public List<ZombieBeing> zombies = new List<ZombieBeing>();

	void Start()
	{
		radius = (collider2D as CircleCollider2D).radius;
		index = indexCount++;

	
		infestationLevel *= infestationStartMultiplifer;
		InvokeRepeating ("SpreadInfection", 5f, 5f);
	}

	public SaveDataArea GetSaveObject()
	{
		SaveDataArea areaSave = new SaveDataArea ();
		areaSave.infestationEndurance = infestationEndurance;
		areaSave.infestationLevel = infestationLevel;
		areaSave.zombiePool = _zombiePool;
		
		return areaSave;
	}
	
	public void LoadFromSaveObject(SaveDataArea data)
	{
		_zombiePool = data.zombiePool;
		infestationEndurance = data.infestationEndurance;
		infestationLevel = data.infestationLevel;
	}

	public override string ToString()
	{
		return "Area " + index;
	}

	public void ReduceInfestationLevel()
	{
		infestationLevel -= 0.28f / radius; 

		if (infestationLevel < 0) {
			infestationLevel = 0;
		}
	}



	public void SpreadInfection()
	{
		infestationGrowth += infestationGrowthOverall * (Mathf.Sqrt (infestationLevel) * 4 +  infestationSaturate) * ManagerScript.instance.CachedTime[99];

		infestationLevel +=  infestationGrowth * 0.001f;
		
		if (infestationLevel > infestationSaturate) {
			infestationLevel = infestationSaturate;
		}
		
		if (zombies.Count + ZombiePool < infestationLevel * radius) {
			_zombiePool += (radius * infestationLevel - (zombies.Count + ZombiePool) ) * infestationSpawnRate;
		} 
	}

	void SpreadInfection_old()
	{
		infestationGrowth += infestationGrowthOverall * Time.deltaTime * (Mathf.Sqrt(infestationLevel)*0.8f  + 0.2f * infestationSaturate);
		infestationLevel += Time.deltaTime * infestationGrowth;
		
		if (infestationLevel > infestationSaturate) {
			infestationLevel = infestationSaturate;
		}
		
		if (zombies.Count + ZombiePool < infestationLevel * radius) {			
			_zombiePool += (radius * infestationLevel - (zombies.Count + ZombiePool) ) * infestationSpawnRate * Time.deltaTime;
		} 
	}
	
	void OnDrawGizmos()
	{
		Gizmos.color = Color.yellow;
		Gizmos.DrawSphere(transform.position, 1f);
	}

}
