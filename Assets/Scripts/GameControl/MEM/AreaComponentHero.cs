using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using GraveDays.Entity.Being;

public class AreaComponentHero : AreaComponent 
{
	private Vector2 lastSpawnPos;

	protected override void DoAreaLeave(AreaBehaviour area)
	{


		if (ManagerScript.networkType == NetworkType.Single || Network.isServer) {
			LinkedList<ZombieBeing> unspawnZombies = new LinkedList<ZombieBeing> ();

			foreach (ZombieBeing zombie in area.zombies) {
				if (zombie != null) {
					AreaComponent zombieArea = zombie.GetComponent<AreaComponent> ();
					if (!DoesShareArea (zombieArea)) {
						zombieArea.CurrentArea._zombiePool += 1;
						unspawnZombies.AddLast (zombie);
					}
				} else
						Debug.Log ("NULL ZOMBIE");
			}

			foreach (var z in unspawnZombies) {
				z.DoUnspawn ();
			}
		}
		//Debug.Log ("UNSPAWN ZOMBIES " + count);
	}

	protected override void DoAreaEnter(AreaBehaviour area)
	{
		if (ManagerScript.networkType == NetworkType.Single || Network.isServer ) {
			DoSpawns (area, 20);
		}
	}

	void Update()
	{
		if (ManagerScript.instance.PAUSED || Network.isClient)
		{
			return;
		}

		if (Time.frameCount%30 == 1) {

			float t1 = Time.realtimeSinceStartup;
		
			for (int i = 0; i < areas.Count; i++)
			{
				DoSpawns (areas[i],4);
			}


		}

	}

	/*Vector2 GetRandomPos(AreaBehaviour area)
	{

	}*/

	void DoSpawns(AreaBehaviour area, int maxSpawns)
	{
	
		int zombiesToSpawn = area.ZombiePool;


		if (zombiesToSpawn > maxSpawns) {
			zombiesToSpawn = maxSpawns;
		}

		while (zombiesToSpawn > 0) {
			zombiesToSpawn--;
			Vector2 pos = Random.insideUnitCircle*area.radius + ((Vector2)area.transform.position);
			if (DoSpawnSingle(area.transform.position, area.radius)) {
				area._zombiePool--;
				break;
			}
		}

		for (int i = 0; i < zombiesToSpawn; i++) {
			if (UnityEngine.Random.Range(0, 10) < 5.8f) {
				if (DoSpawnSingle(lastSpawnPos, 5)) {
					area._zombiePool--;
				}
			}
			else {
				if (DoSpawnSingle(area.transform.position, area.radius)) {
					area._zombiePool--;
				}
			}

		}
	}

	bool DoSpawnSingle( Vector2 centre, float radius)
	{
	
		Vector2 pos = Random.insideUnitCircle*radius + centre;
		if (OutOfView(pos)) {
			ZombieBeing zomb = ManagerScript.instance.TrySpawnZombie(pos);
			if (zomb != null) {
				lastSpawnPos = zomb.GetTransform().position;
				return true;
			}
			 
			/*if (zomb != null)
			{
				area._zombiePool -= 1;
				
			}*/
		}
		return false;
	}



	bool _outOfViewOwn(Vector2 pos)
	{
		return ((pos - (Vector2)transform.position).sqrMagnitude > 180);
	}

	bool OutOfView(Vector2 pos)
	{
		if (ManagerScript.networkType == NetworkType.Single) {
			return _outOfViewOwn(pos);
		} else {
			if (!_outOfViewOwn(pos))
			{
				return false;
			}

			foreach (var ally in ManagerScript.instance.allies) {
				if (!ally.GetComponent<AreaComponentHero>()._outOfViewOwn(pos))
				{
					return false;
				}
			}
		}

		return true;
	}

	bool DoesShareAreaAllies(AreaComponent zombie)
	{
		foreach (var ally in ManagerScript.instance.allies) {
			if (ally.GetComponent<AreaComponentHero>()._DoesShareAreaOwn(zombie))
			{
				return true;
			}
		}

		return false;
	}

	public bool _DoesShareAreaOwn(AreaComponent zombie)
	{
		foreach (AreaBehaviour areaZombie in zombie.areas) {
			if (lastArea == areaZombie) {
				return true;
			}
			
			foreach (AreaBehaviour areaHero in areas) {
				if (areaZombie == areaHero) {
					return true;
				}
			}
		}

		return false;
	}

	public override bool DoesShareArea(AreaComponent zombie)
	{
		if (_DoesShareAreaOwn (zombie)) {
			return true;
		}

		if (DoesShareAreaAllies(zombie))
		{
			return true;
		}

		return false;
	}
}
