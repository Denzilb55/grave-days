﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class AreaComponent : MonoBehaviour {
	//public List<AreaBehaviour> areas = new List<AreaBehaviour>();
	protected AreaBehaviour lastArea;
	public List<AreaBehaviour> areas = new List<AreaBehaviour>();
	public AreaBehaviour CurrentArea
	{
		get { 
			if (areas.Count == 0)
			{
				return lastArea;
			}
			else {
				return areas[0];
			}
		}
	}
	


	protected abstract void DoAreaEnter (AreaBehaviour area);

	protected abstract void DoAreaLeave(AreaBehaviour area);


	public abstract bool DoesShareArea (AreaComponent zombie);

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.CompareTag("Area")) {
			AreaBehaviour area = other.GetComponent<AreaBehaviour>();
			if (!areas.Contains(area))
			{
				areas.Add(area);
				DoAreaEnter(area);
				lastArea = area;
			}

		}
		//Debug.Log ("ADDED: " + transform.tag);
	}

	void OnTriggerExit2D(Collider2D other)
	{
		if (other.CompareTag("Area")) {
			AreaBehaviour area = other.GetComponent<AreaBehaviour>();
			areas.Remove(area);
			DoAreaLeave(area);

		}
	}
}
