﻿using UnityEngine;
using System.Collections;
using GraveDays.Entity.Being;

public class AreaComponentZombie : AreaComponent 
{
	private ZombieBeing zombie;

	void Awake()
	{
		zombie = GetComponent<ZombieBeing> ();
	}

	protected override void DoAreaLeave(AreaBehaviour area)
	{
	
		if (ManagerScript.networkType == NetworkType.Single || Network.isServer) {
			AreaComponent zombieArea = zombie.GetComponent<AreaComponent> ();
			if (!GlobalData.instance.player.areaComponent.DoesShareArea (zombieArea)) {
				zombieArea.CurrentArea._zombiePool += 1;
				area.zombies.Remove (zombie);
				zombie.DoUnspawn ();

			} else if (areas.Count == 0) {

			} else {
					area.zombies.Remove (zombie);
			}
		}
	}

	protected override void DoAreaEnter(AreaBehaviour area)
	{
		if (areas.Count == 1 && lastArea != null) {
			if (areas[0] != lastArea) {
				lastArea.zombies.Remove (zombie);
			}
		} else {
			area.zombies.Add (zombie);
			//Debug.Log("ADD ZOMBIE");
		}



	}
	
	#region implemented abstract members of AreaComponent
	public override bool DoesShareArea (AreaComponent zombie)
	{
		throw new System.NotImplementedException (); //implement if required
	}
	#endregion
}
