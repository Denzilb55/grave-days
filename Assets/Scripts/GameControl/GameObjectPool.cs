﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GraveDays.Entity.Being;

public class GameObjectPool : MonoBehaviour {

	private const int BULLET_MAX = 100;
	public GameObject bullet;
	private static BulletBehaviour [] bullets = new BulletBehaviour[BULLET_MAX];
	public static GameObjectPool instance;
	int bulletPointer;

	public GameObject bloodSplatter;
	private const int BLOOD_SPLATTER_MAX = 2000;
	private static GameObject [] bloodSplatters = new GameObject[BLOOD_SPLATTER_MAX];
	int splatterPointer;


	public GameObject zombie;
	private Stack<ZombieBeing> zombiesPooled = new Stack<ZombieBeing> ();
	public const int INITIAL_ZOMBIES = 150;
	private int thinkID;

	int s = 0;
	

	void Awake () {
		instance = this;

		Network.minimumAllocatableViewIDs = 100;
	
		for (int i = 0; i < BULLET_MAX; i++)
		{
			bullets[i] = ((GameObject)Instantiate(bullet)).GetComponent<BulletBehaviour>();
			bullets[i].gameObject.SetActive(false);
			bullets[i].transform.parent = GlobalData.instance.Holder.transform;

			if (Network.isServer)
			{
				bullets[i].networkView.viewID = Network.AllocateViewID();
				Debug.Log("Allocated bullet: " + bullets[i].networkView.viewID);
			}
		}

		for (int i = 0; i < BLOOD_SPLATTER_MAX; i++)
		{
			bloodSplatters[i] = (GameObject)Instantiate(bloodSplatter);
			bloodSplatters[i].transform.Rotate (Vector3.forward * Random.Range (-180, 180));
			bloodSplatters[i].SetActive(false);
			bloodSplatters[i].transform.parent = GlobalData.instance.Holder.transform;
		}

		for (int i = 0; i < INITIAL_ZOMBIES; i++)
		{
			GameObject zomb = (GameObject)Instantiate(zombie);
			zomb.SetActive(false);
			ZombieBeing zb = zomb.GetComponent<ZombieBeing>();
			if (zb == null) Debug.Log("NULLLL");
		//TAG	zb.ThinkID = thinkID++;
			zb.transform.parent = GlobalData.instance.Holder.transform;
			zombiesPooled.Push(zb);
			if (Network.isServer)
			{
				zb.networkView.viewID = Network.AllocateViewID();
				Debug.Log("Allocated initial zombie: " + zb.networkView.viewID);
			}
		}
	}

	public ZombieBeing GetZombie(Vector3 position)
	{
		//if (s != 0)
		//return null;

		//s = 2;
		ZombieBeing zomb;
		if (zombiesPooled.Count > 0) {
			zomb = zombiesPooled.Pop();
		

			zomb.gameObject.SetActive (true);

		} else {
			zomb = ((GameObject)Instantiate(zombie)).GetComponent<ZombieBeing>();
			//TAG zomb.ThinkID = thinkID++;

			if (Network.isServer)
			{
				zomb.networkView.viewID = Network.AllocateViewID();
				Debug.Log("Allocated additional zombie: " + zomb.networkView.viewID);
			}

			#if UNITY_EDITOR
			zomb.transform.parent = GlobalData.instance.Holder.transform;
			#endif
		}
		zomb.transform.position = position;
		/*if (zombiesPooled.Contains (zomb)) {
			Debug.Log("Not popped properly");
			new System.Diagnostics.StackTrace();
		}*/



		ManagerScript.instance.zombies.Add (zomb);
		return zomb;
	}

	public void FreeZombie(ZombieBeing zomb)
	{

		/*if (zombiesPooled.Contains (zomb)) {
			Debug.Log("Already on stack");
			new System.Diagnostics.StackTrace();
		}*/
		if (Network.isServer) {
			ManagerScript.instance.zombiesToNetSync.Remove(zomb);
		}
		zombiesPooled.Push (zomb);
		zomb.gameObject.SetActive (false);
	}

	public void ServeBloodSplatter(Vector3 position)
	{
		if (ManagerScript.networkType == NetworkType.Single) {
			GetBloodSplatter (position);
		} else {
			NetworkScript.instance.ServeBloodSplatter (RPCMode.All, position);
		}

	}

	public GameObject GetBloodSplatter(Vector3 position)
	{
		GameObject b = bloodSplatters [splatterPointer];

		b.transform.position = new Vector3(position.x, position.y, 0.01f);
		b.SetActive (true);
		splatterPointer++;
		
		
		if (splatterPointer > BLOOD_SPLATTER_MAX-1) {
			splatterPointer = 0;
		}
		
		return b;
	}

	public BulletBehaviour GetBullet(Vector3 position, Quaternion rotation)
	{
		BulletBehaviour b = bullets [bulletPointer];

		b.transform.position = position;
		b.transform.rotation = rotation;
		b.rigidbody2D.velocity = Vector2.zero;
		b.gameObject.SetActive (true);
		bulletPointer++;
	

		if (bulletPointer > BULLET_MAX-1) {
			bulletPointer = 0;
		}
	
		return b;
	}

}
