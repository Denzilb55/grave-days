﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GraveDays.Entity.Being;

public class NetworkScript : MonoBehaviour {

	ManagerScript manager;
	GlobalData data;
	public static NetworkScript instance;
	bool networkReady = false;
	public static List<NetworkPlayer> peers = new List<NetworkPlayer>();
	List<NetworkPlayer> readyPeers = new List<NetworkPlayer>();
	bool networkStarted = false;
	bool doneLoaded = false;
	static int networkIDs;
	int networkID = 0;


	void Awake()
	{
		manager = GetComponent<ManagerScript> ();
		data = GetComponent<GlobalData> ();
		instance = this;

	}

	void OnLevelWasLoaded(int level) {
		if (Application.loadedLevel != 1) {
			return;
		}

		ManagerScript.instance.InitialiseHeroPlayer ();


		if (ManagerScript.networkType == NetworkType.Single) {

		} else {
			networkReady = false;
			networkStarted = true;
			manager.PAUSED = true;
			readyPeers.Clear();
		}

		if (Network.isServer) {

			Vector3 pos = data.player.transform.position;
			AllocatePlayer (pos, networkID);

			Vector3 allyPos = pos;

			foreach (var peer in peers) {

			}

			networkID = 0;
			if (peers.Count == 0)
			{
				manager.PAUSED = false;
				networkReady = true;
			}
		} else if (Network.isClient) {
			networkView.RPC("NotifyServerDoneLoaded", RPCMode.Server, Network.player);
		}

	}

	[RPC] void SyncToNewPlayer(NetworkPlayer player)
	{
		if (Network.player != player) {
			Debug.Log("Send sync replicate");
			networkView.RPC("ReplicatePlayer",  player, data.player.networkView.viewID, data.player.transform.position, data.player.name);
		}
	}

	[RPC] void NotifyServerDoneLoaded(NetworkPlayer player)
	{
		Vector3 allyPos = data.player.transform.position + new Vector3 (0.5f, 0, 0);
		networkIDs++;
		networkView.RPC ("AllocatePlayer", player, allyPos, networkIDs);
		networkView.RPC ("SyncToNewPlayer", RPCMode.All, player);
		/*foreach (ZombieBehaviour zomb in manager.zombies) {
			float x = zomb.transform.position.x;
			float y = zomb.transform.position.y;
			NetworkViewID viewID = zomb.networkView.viewID;
			networkView.RPC ("RPCSpawnZombie", RPCMode.Others, x, y, viewID); 
			Debug.Log("SEND SPAWN ZOMBIE");
		}*/

	}

	[RPC] void WelcomePlayer(NetworkPlayer player)
	{
		if (player != Network.player) {
			NetworkScript.peers.Add (player);

		}
	}
	
	[RPC] void FarewellPlayer(NetworkPlayer player)
	{
		//peers.Remove (player);
		readyPeers.Remove (player);

		AllyBehaviour removeAlly = null;
		foreach (var a in manager.allies) {
			if (a.networkView.viewID.owner == player)
			{
				removeAlly = a;
				break;
			}
		}

		if (removeAlly != null) {
			manager.allies.Remove(removeAlly);
		}
	}

	[RPC] void LateJoin()
	{

	}

	[RPC]
	void LaunchGame()
	{

	}
	
	void OnPlayerConnected(NetworkPlayer player)
	{

		if (networkStarted)
		{

			Vector3 allyPos =  data.player.transform.position + new Vector3(0.5f, 0, 0);
			networkIDs++;
			networkView.RPC ("LaunchGame", player);
		}
		networkView.RPC ("WelcomePlayer", RPCMode.All, player); 
		Debug.Log ("PLAYER CONNECTED");
	}
	
	void OnPlayerDisconnected(NetworkPlayer player)
	{
		Network.RemoveRPCs(player);
		Network.DestroyPlayerObjects(player);
		networkView.RPC ("FarewellPlayer", RPCMode.All, player); 
		Debug.Log ("PLAYER DISCONNECTED");
	}

	void OnDisconnectedFromServer(NetworkDisconnection info)
	{
		if (info == NetworkDisconnection.LostConnection) {
			ManagerScript.instance.OpenMenu ();
		}
	}

	[RPC]
	void ClientReady(NetworkPlayer player)
	{
		if (!readyPeers.Contains (player)) {
			readyPeers.Add (player);
			if (readyPeers.Count == peers.Count) {
				networkView.RPC ("AllReady", RPCMode.All);
			}
		}
	}

	[RPC]
	void AllReady()
	{
		manager.InitialiseHeroPlayer();
		manager.PAUSED = false;
		networkReady = true;
	}

	[RPC]
	void AllocatePlayer(Vector3 position, int ID)
	{
		
		data.player.transform.position = position;
		NetworkViewID myID = Network.AllocateViewID();
		data.player.networkView.viewID = myID;

	
		Debug.Log ("Allocated and send replicate " + myID);
		data.player.name = PlayerPrefs.GetString ("playerName");
		int pClass = 0;
		if (PlayerPrefs.HasKey ("PlayerClass")) {
			pClass = PlayerPrefs.GetInt("PlayerClass");
		}
		networkView.RPC("ReplicatePlayer",  RPCMode.Others, myID, position, data.player.name, pClass);
		networkID = ID;
	}
	
	[RPC]
	void ReplicatePlayer(NetworkViewID viewID, Vector3 position, string playerName, int classIndex)
	{
		
		foreach (var a in manager.allies) {
			if (a.networkView.viewID == viewID)
			{
				return;
			}
		}

		GameObject allyObj = Instantiate (data.allyTemplate) as GameObject;
		allyObj.transform.position = position;
		allyObj.name = playerName;

		AllyBehaviour ally = allyObj.GetComponent<AllyBehaviour> ();
		ally.Initialise ((PlayerClass)classIndex);


		ally.networkView.viewID = viewID;
		manager.allies.Add (ally);
		Debug.Log ("Get replicate " + viewID);
	}

	void Update()
	{

		if (!networkReady && manager.allies.Count == peers.Count && networkStarted && Network.isClient) {
			networkView.RPC("ClientReady", RPCMode.Server, Network.player);
		}
	}

	public void ServeBloodSplatter(RPCMode rpcMode, Vector3 position)
	{
		networkView.RPC ("RemoteBloodSplatter", rpcMode, position);
	}

	public void NetworkSpawnZombie (ZombieBeing zomb)
	{
		float x = zomb.transform.position.x;
		float y = zomb.transform.position.y;
		NetworkViewID viewID = zomb.networkView.viewID;
		networkView.RPC ("RPCSpawnZombie", RPCMode.Others, x, y, viewID); 
	}

	public void NetworkSpawnSurvivor (SurvivorBeing surv, int index, string name)
	{
		float x = surv.transform.position.x;
		float y = surv.transform.position.y;

		NetworkViewID viewID = Network.AllocateViewID();
		surv.networkView.viewID = viewID;
		networkView.RPC ("RPCSpawnSurvivor", RPCMode.Others, x, y, viewID, index, name); 
	}

	public void NetworkSpawnPickup(PickupItem pickup)
	{
		NetworkViewID viewID = Network.AllocateViewID();
		pickup.networkView.viewID = viewID;
		networkView.RPC ("RPCSpawnPickup", RPCMode.Others, pickup.transform.position, viewID, (int)pickup.type);
	}

	[RPC]
	void RPCSpawnPickup(Vector3 pos, NetworkViewID viewID, int typeInt)
	{
		manager.SpawnPickup (pos, (PickupType)typeInt).networkView.viewID = viewID;
	}

	[RPC]
	void RPCSpawnSurvivor(float x, float y, NetworkViewID viewID, int index, string name)
	{
		SurvivorBeing surv = manager.SpawnSurvivor(new Vector2(x,y), index);
		surv.networkView.viewID = viewID;
		surv.name = name;
	}

	[RPC]
	void RPCSpawnZombie(float x, float y, NetworkViewID viewID)
	{
		manager.SpawnZombie(new Vector2(x,y)).networkView.viewID = viewID;
	}

	[RPC] 
	void RemoteBloodSplatter(Vector3 position)
	{
		GameObjectPool.instance.GetBloodSplatter (position);
	}


	[RPC] 
	void InstantiateNetworkBullet(Vector3 fireVector, Vector3 fireOrigin, Quaternion rotation, NetworkPlayer player)
	{
		BulletBehaviour bullet = GameObjectPool.instance.GetBullet (fireOrigin, rotation);
		NetworkViewID viewID = bullet.networkView.viewID;
		bullet.rigidbody2D.AddForce (fireVector);
		bullet.direction = fireVector;
		Debug.Log ("Server get bullet " + viewID);
		networkView.RPC ("InstantiateClientBullet", RPCMode.Others, fireVector, fireOrigin, rotation, player, viewID);
		if (player == Network.player) {
			GlobalData.instance.player.AssignBullet(bullet);			
		} else {
			SoundPlayer.instance.Play (Sounds.bulletShort, fireOrigin, 100);
		}
	}
	
	void InstantiateServerFiredBullet(Vector3 fireVector, Vector3 fireOrigin, Quaternion rotation, IGunner gunner)
	{
		BulletBehaviour bullet = GameObjectPool.instance.GetBullet (fireOrigin, rotation);
		NetworkViewID viewID = bullet.networkView.viewID;
		bullet.rigidbody2D.AddForce (fireVector);
		bullet.direction = fireVector;
		Debug.Log ("Server get bullet " + viewID);
		networkView.RPC ("InstantiateClientBullet", RPCMode.Others, fireVector, fireOrigin, rotation, Network.player, viewID);
		gunner.AssignBullet (bullet);
	}
	

	public void FireNetworkBullet(Vector3 fireVector, Vector3 fireOrigin, Quaternion rotation, IGunner gunner = null)
	{

		if (Network.isServer) {
			InstantiateServerFiredBullet(fireVector, fireOrigin, rotation, gunner);
		} else {
			networkView.RPC("InstantiateNetworkBullet", RPCMode.Server, fireVector, fireOrigin, rotation, Network.player); 
		}
	}
	

	[RPC] void InstantiateClientBullet(Vector3 fireVector, Vector3 fireOrigin, Quaternion rotation, NetworkPlayer player, NetworkViewID viewID)
	{
		BulletBehaviour bullet = GameObjectPool.instance.GetBullet(fireOrigin, rotation);
		bullet.networkView.viewID = viewID;
		bullet.rigidbody2D.AddForce (fireVector);
		bullet.direction = fireVector;

		Debug.Log ("Get bullet from server " + viewID);

		if (Network.player.Equals (player)) {
			GlobalData.instance.player.AssignBullet (bullet);
		} else {
			SoundPlayer.instance.Play(Sounds.bulletShort,fireOrigin, 100);
		}
	}


	void OnSerializeNetworkView(BitStream stream, NetworkMessageInfo info) {


		if (stream.isWriting) {
			if (Network.isServer)
			{
				//SendZombieSpawns(stream);
				float time = manager.currentTime;
				stream.Serialize(ref time);
			}
			else {
		
			}
		} else if (stream.isReading) {
			if (Network.isServer)
			{
		
			}
			else {
				//ReceiveZombieSpawns(stream);
				float time = 0;
				stream.Serialize(ref time);
				manager.SetCurrentTime(time);
			}
		}
		
	}

	[RPC]
	void RejoinLobby()
	{
		PlayerPrefs.SetInt ("retry", 1);
		Application.LoadLevel (0);
		#if UNITY_WEBPLAYER
		Screen.fullScreen = false;
		#endif
	}

}
