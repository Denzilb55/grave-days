﻿using UnityEngine;
using System.Collections;

public class SpawnSlotBehaviour : MonoBehaviour {

	public PickupItem item;

	public bool HasItem()
	{
		return item != null;
	}

	void OnDrawGizmos()
	{
		Gizmos.color = Color.green;
		Gizmos.DrawSphere(transform.position, 0.4f);
	}
}
