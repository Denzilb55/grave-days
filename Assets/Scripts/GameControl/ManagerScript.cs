using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System;
using GraveDays.Items.Guns;
using GraveDays.Quests;
using GraveDays.Entity.Being;
using GraveDays.Entity;

[Serializable]
public class SaveData
{
	public SaveDataHero heroSave;
	
	public int killCount;
	public float currentTime;
	public float score;

	public SaveDataArea [] areaSaves;
	public SaveDataZombie[] zombieSaves;
	public SaveDataPickup[] pickupSaves;
	public SaveDataItemHolder[] itemHolderSaves;
	public SaveDataInventory[] containerSaves;

	public SaveData (int areaCount, int zombieCount, int pickupsCount, int itemHolderCount, int containerCount)
	{
		areaSaves = new SaveDataArea[areaCount];
		zombieSaves = new SaveDataZombie[zombieCount];
		pickupSaves = new SaveDataPickup[pickupsCount];
		itemHolderSaves = new SaveDataItemHolder[itemHolderCount];
		containerSaves = new SaveDataInventory[containerCount];
	}
}

public enum NetworkType {Single = 0, Client, Server}

public class ManagerScript : MonoBehaviour {

	//NETWORK FIELDS

	public static NetworkType networkType;
	ParameterizedThreadStart threadedUpdatesStart;
	Thread updatesThread;
	////// 

	public bool GameOver;
	//public Light DayLight;
	public Light2D DayLight;
	public GameObject player;
	public List<GameObject> living = new List<GameObject> ();
	public List<ZombieBeing> zombies = new List<ZombieBeing>();
	public List<ZombieBeing> zombiesToNetSync = new List<ZombieBeing> ();
	public List<ZombieBeing> zombiesToRecycle = new List<ZombieBeing> ();
	public Dictionary<int, KeyValuePair<ZombieBeing,int> > zombiesToProcess;
	public List<AllyBehaviour> allies = new List<AllyBehaviour> ();
	public List<SurvivorBeing> survivors = new List<SurvivorBeing> ();
	public List<GameObject> pickups = new List<GameObject> ();
	public List<AreaBehaviour> areas = new List<AreaBehaviour> ();
	public GlobalData data;
	private SongPlayer songPlayer;
	public int killCount;

	public GameObject barracks;
	public GameObject initialPickupsHolder;

	public QuestManager questManager;
	public static ManagerScript instance;
	public NetworkScript networkScript;
	public float currentTime {
		get;
		private set;
	}
	private float startHours;
	private float score;
	private int lastThinkID;

	private string Tooltip;
	private Vector2 TooltipPos;

	private InteractComponent interactHovered;
	private IOptioned optionedActivated;

	private bool menuOpen;
	public bool PAUSED = false;

	private float fpsMem;
	private float fpsMemRatio = 0.12f;
	

	private int lightUpCount;
	private int lightDownCount;
	
	private int lightDetail = 0;
	public Light2D.LightDetailSetting lightDetailSetting = Light2D.LightDetailSetting.Rays_500;
	public Light2D visibilityField;

	private float spawnTime;

	public GameObject HomeMarker;
	public Vector2 HomeMarkerPosition;

	public static bool doLoad = false;
	public static bool levelJustLoaded = false;

	public SurvivorBehaviour selectedSurv;
	public ZombieBeing selectdZomb;

	public bool autoLightAdjust = true;
	public int lightMax = 9;

	private Queue<float> time = new Queue<float>();
	public List<float> CachedTime = new List<float>();



	public const int SPAWN_BLOCK_LAYER  = (1 << 8 | 1 << 13 | 1 << 10 | 1 << 11 | 1 << 17 | 1 << 22);

	public GUISkin skin;

	public float LightExposure
	{
		get
		{
			return DayLight.Intensity * 70f;
		}
	}

	public float currentHours
	{
		get { return currentTime / 60f; }
	}

	public float DailyHour
	{
		get { return currentHours % 24;} 
	}

	public float HoursPlayed
	{
		get { return currentHours - startHours;}
	}

	public int NewThinkID
	{
		get 
		{
			return ++lastThinkID % 60;
		}
	}

	public List<ILateProcess> lateProcesses = new List<ILateProcess>();
	private List<ZombieBeing> lingeringProcessedZombies;

	public string networkDebugText;

	public static void PreLoadActions()
	{
		ChestBehaviour.chests.Clear ();
		foreach (var p in instance.pickups)
		{
			Destroy(p);
		}
		instance.pickups.Clear ();
		ItemHolder.itemHolders.Clear ();

	}

	void Awake()
	{

		if (instance == null) {
			instance = this;
		} else {
			DestroyImmediate(instance.gameObject);
			instance = this;
		//	return;
		}

		questManager = new QuestManager();


		if (GlobalData.instance != null) {
			data = GlobalData.instance;
		}


		if (Application.loadedLevel == 1) {
			HomeMarker.SetActive (false);
			currentTime = 18f * 60;
			startHours = currentHours;
			PAUSED = false;
			menuOpen = false;
			Time.timeScale = 1;
			AreaBehaviour.indexCount = 0;
			Application.targetFrameRate = 60;
			networkScript = GetComponent<NetworkScript> ();
		}



		if (networkType != NetworkType.Single) {
			Network.minimumAllocatableViewIDs = 100;
			Debug.Log("Allocate");
		}

		InitialiseInputs ();
	}

	void InitialiseInputs()
	{
		cInput.SetKey ("Forward", "W");
		cInput.SetKey ("Backward", "S");
		cInput.SetKey ("Strafe Left", "A");
		cInput.SetKey ("Strafe Right", "D");

		cInput.SetKey ("Flashlight", "F");
		cInput.SetKey ("Sprint", Keys.LeftShift);
		cInput.SetKey ("Fire/Attack", "Mouse0");
		cInput.SetKey ("Quick Melee", "Mouse1");
		cInput.SetKey ("Interact", "E");
		cInput.SetKey ("Inventory", "I");
	}

	void InitialiseThreads()
	{
		updatesThread = new Thread (threadedUpdatesStart);
	}


	public void SetOptionActivated(IOptioned optioned)
	{
		optionedActivated = optioned;
	}



	void OnGUI()
	{
		if (Application.loadedLevel != 1) {
			return;
		}

		Rect rect = new Rect (0, 0, 180, 150);

		HeroBeing hero = GlobalData.instance.player;
		//guiText += "Health: " + health.Health.ToString("f0") + " / " + health.MaxHealth.ToString("f0") +" \n"; 
		// "Hunger: " + hero.Hunger.ToString("f0") + " / 100 \n"; 
		string guiText = "Ammo: " + hero.gunComponent.Ammo + " \n"; 
		guiText += "Food (3): " + hero.FoodCount + " \n"; 
		guiText += "Bandages (4): " + hero.BandagesCount + " \n"; 
		guiText += "Carry weight: " + hero.inventory.currentWeight + " / " + hero.inventory.maxCarryWeight + " \n"; 
		if (hero.melee) {
			guiText += "Weapon: Fists \n"; 
		} else {
			//guiText += "Weapon: 9mm glock \n"; 
			guiText += "Weapon: "+ hero.gunComponent.GunType.GetItemText()+" \n"; 
		}


		int xStart = Screen.width / 2 - 335;

		Rect healthRect = new Rect (xStart + 5, Screen.height - 50, 240, 50);
		GUIStyle healthStyle = new GUIStyle(GUI.skin.label);
		healthStyle.fontSize = 23;
		healthStyle.normal.textColor = Color.white;
		if (data.player.effectsComponent.IsEffected(EffectsType.Fever)) {
			healthStyle.normal.textColor = Color.green;
		}
		GUI.Label(healthRect, "HEALTH: " +hero.healthComponent.Health.ToString("f0") + "/" + hero.healthComponent.MaxHealth.ToString("f0"), healthStyle); 

		Rect fatigueRect = new Rect (xStart + 250, Screen.height - 50, 240, 50);
		GUI.Label(fatigueRect, "FATIGUE: " +hero.Fatigue.ToString("f0") + "/" + hero.fatigueComponent.MaxFatigue, healthStyle); 

		Rect hungerRect = new Rect (xStart + 500, Screen.height - 50, 240, 50);
		GUI.Label(hungerRect, "HUNGER: " +hero.Hunger.ToString("f0") + "/100", healthStyle); 

		//guiText += "Hours: " + HoursPlayed.ToString("f1") + "\n";
		guiText += "Zombies: " + zombies.Count  +"("+zombiesToProcess.Count+")\n";
		guiText += "Light detail: " + lightDetail + "\n";
		//guiText += "Z-Pool: " + GetPoolZombies() + "\n";
		//guiText += "Kills: " + killCount + "\n";
	//	if (Time.frameCount % 20 == 0)
	//		Debug.Log (fpsMem.ToString("f0") + " " + zombies.Count + " " + lightDetail);

		if (NetworkScript.peers.Count >0 )
		networkDebugText = Network.GetLastPing (NetworkScript.peers [0]).ToString();

		guiText += "FPS: " + fpsMem.ToString("f0") + "\n";
		guiText += "Network: " + networkDebugText+ "\n";
		GUI.Box (rect, guiText);

		rect = new Rect (Screen.width/2f, 0, 400, 50);
		GUIStyle scoreStyle = new GUIStyle (GUI.skin.label);
		scoreStyle.fontSize = 20;
		guiText = "Hours Survived: " + HoursPlayed.ToString("f1");
		GUI.Label (rect, guiText, scoreStyle);


		if (menuOpen) {
			GUIStyle menuStyle = new GUIStyle(GUI.skin.box);
			menuStyle.alignment = TextAnchor.UpperCenter;
			menuStyle.fontSize = 25;
			menuStyle.fontStyle = FontStyle.Bold;
			GUI.Box (new Rect (Screen.width / 2f - 120, 50, 250, 500), GUIContent.none);
			GUI.Box (new Rect (Screen.width / 2f - 120, 50, 250, 500), "Grave Days", menuStyle);

			if (!Network.isClient)
			{
				if (GUI.Button(new Rect (Screen.width / 2f - 120, 95, 240, 30), "Retry"))
				{
					if (networkType == NetworkType.Single)
					{
						Application.LoadLevel(1);

						PreLoadActions();
					}
					else  {
						NetworkScript.instance.networkView.RPC("RejoinLobby", RPCMode.All);
					}
				}
			}

			if (GUI.Button(new Rect (Screen.width / 2f - 120, 130, 240, 30), "Light Quality: " + lightDetail))
			{
				SetLightDetail(lightDetail+1);
				lightMax = lightDetail;
				PlayerPrefs.SetInt("LightDetailSetting",lightDetail);
			}

			if (GUI.Button(new Rect (Screen.width / 2f - 120, 165, 240, 30), "Auto-light Adjust: " + autoLightAdjust))
			{
				autoLightAdjust = !autoLightAdjust;
				PlayerPrefs.SetInt("AutoLightAdjust",autoLightAdjust?1:0);
			
			}

			string keyBindingsText = "Keybindings";

			if (GUI.Button(new Rect (Screen.width / 2f - 120, 200, 240, 30), keyBindingsText))
			{
				cGUI.cSkin = skin;
				cGUI.ToggleGUI();
				cGUI.cSkin = skin;
				//cGUI.cSkin = cInput
			}


			string quitText = "Save and Quit";
			if (player == null)
			{
				quitText = "Quit";
			}
			if (GUI.Button(new Rect (Screen.width / 2f - 120, 235, 240, 30), quitText))
			{
				if (player != null)
				{
				//	Save();
				}
				if (ManagerScript.networkType != NetworkType.Single)
				{
					Network.Disconnect();
				}
				Application.LoadLevel(0);
				#if UNITY_WEBPLAYER
				Screen.fullScreen = false;
				#endif
			}



			GUIStyle controlsStyle = new GUIStyle(GUI.skin.label);
			menuStyle.alignment = TextAnchor.UpperCenter;
			int x = 50;

			GUI.Label(new Rect (Screen.width / 2f -50 , 240+x, 250, 30), "Shortcut - eat food: 3", controlsStyle);
			GUI.Label(new Rect (Screen.width / 2f -50 , 260+x, 250, 30), "Shortcut - use bandages: 4", controlsStyle);
			/*GUI.Label(new Rect (Screen.width / 2f -50 , 240+x, 250, 30), "Walk: WASD/ right mouse", controlsStyle);
			GUI.Label(new Rect (Screen.width / 2f -50 , 260+x, 250, 30), "Shoot: left mouse", controlsStyle);
			GUI.Label(new Rect (Screen.width / 2f -50 , 280+x, 250, 30), "Interact (chests): E", controlsStyle);
			GUI.Label(new Rect (Screen.width / 2f -50 , 300+x, 250, 30), "Switch to melee: 1", controlsStyle);
			GUI.Label(new Rect (Screen.width / 2f -50 , 320+x, 250, 30), "Switch to gun: 2", controlsStyle);
			GUI.Label(new Rect (Screen.width / 2f -50 , 340+x, 250, 30), "Eat food: 3", controlsStyle);
			GUI.Label(new Rect (Screen.width / 2f -50 , 360+x, 250, 30), "Use bandage: 4", controlsStyle);
			GUI.Label(new Rect (Screen.width / 2f -50 ,	380+x, 250, 30), "Toggle flashlight: F", controlsStyle);
			GUI.Label(new Rect (Screen.width / 2f -50 , 400+x, 250, 30), "Sprint: Left Shift", controlsStyle);
			GUI.Label(new Rect (Screen.width / 2f -50 , 420+x, 250, 30), "Order halt: H", controlsStyle);
			GUI.Label(new Rect (Screen.width / 2f -50 , 440+x, 250, 30), "Order follow: Q", controlsStyle);*/

			Rect creditsRect = new Rect (Screen.width -262, Screen.height - 75, 260, 72);
			string creditsString = "Developed by Denzil Buchner (@DenzilZA) \n Music by Kevin MacLeod \n F9 - tribute to C&C Generals";
			GUI.Box (creditsRect, creditsString);
			GUI.Box (creditsRect, creditsString);
			GUI.Box (creditsRect, creditsString);
			GUI.Box (creditsRect, creditsString);
		}


		if (Tooltip != null) {

			Vector2 pos = Camera.main.WorldToScreenPoint(TooltipPos);
			GUI.Box(new Rect(pos.x + 4, Screen.height - pos.y + 4, 140,22),Tooltip);
		}

		if (optionedActivated != null) {
			optionedActivated.DrawMenu();
		}
	}




	Light2D.LightDetailSetting GetLightDetailSetting(int lightDetail)
	{
		switch (lightDetail)
		{
		case -1:
			return Light2D.LightDetailSetting.Rays_400;
		case -2: 
			return Light2D.LightDetailSetting.Rays_300;
		case -3:
			return Light2D.LightDetailSetting.Rays_200;


		case 0: 
			return Light2D.LightDetailSetting.Rays_200;
			
		case 1: 
			return Light2D.LightDetailSetting.Rays_400;
		case 2: 
			return Light2D.LightDetailSetting.Rays_500;
		case 3: 
			return Light2D.LightDetailSetting.Rays_600;

		case 4: 
			return Light2D.LightDetailSetting.Rays_700;

		case 5: 
			return Light2D.LightDetailSetting.Rays_800;

		case 6:
			return Light2D.LightDetailSetting.Rays_900;

		case 7: 

			return Light2D.LightDetailSetting.Rays_1000;

		case 8: 
			return Light2D.LightDetailSetting.Rays_2000;

		case 9: 
			return Light2D.LightDetailSetting.Rays_3000;
			
		case 10: 
			return Light2D.LightDetailSetting.Rays_4000;

		

		default: 
			return Light2D.LightDetailSetting.Rays_50;
		
		}
	}

	void SetLightDetail(int detailLevel)
	{
		lightDetail = detailLevel;
		if (lightDetail > 10)
		{
			lightDetail = 0;
		} else if (lightDetail < -3)
		{
			lightDetail = -3;
		}
		
		Light2D.LightDetailSetting lsDetail = GetLightDetailSetting (lightDetail);
		//DayLight.LightDetail = lsDetail;
		//GlobalData.instance.player.Torch.LightDetail = lsDetail;
		visibilityField.LightDetail = lsDetail;

		foreach (AllyBehaviour ally in allies) {
			ally.Torch.LightDetail = GetLightDetailSetting (lightDetail -1);
		}
	}

	public void InitialiseHeroPlayer()
	{

		if (player == null) {
			player = Instantiate (data.playerTemplate) as GameObject;
			data.player = player.GetComponent<HeroBeing> ();
			foreach (Transform t in player.transform) {
				if (t.CompareTag ("DayLight")) {
					DayLight = t.GetComponent<Light2D> ();
				} else if (t.CompareTag ("Visibility")) {
					visibilityField = t.GetComponent<Light2D>();
					float dist = Camera.main.orthographicSize *(new Vector2(Screen.width,Screen.height)).magnitude / Screen.height *1.3f;
					visibilityField.CastDistance = dist; 
					visibilityField.LightRadius = dist; 
				}
			}

			int pClass = 0;
			if (PlayerPrefs.HasKey ("PlayerClass")) {
				pClass = PlayerPrefs.GetInt("PlayerClass");
			}

			//if (networkType == NetworkType.Single)
			//{
				data.player.Initialise((PlayerClass)pClass);
			//}

		/*		ITargetable targetZombie = SpawnZombie(data.player.transform.position + new Vector3(0,5,0));
			targetZombie = SpawnZombie(data.player.transform.position + new Vector3(0,-5,0));
			targetZombie = SpawnZombie(data.player.transform.position + new Vector3(3,5,0));*/
		//	SpawnSurvivor(data.player.transform.position + new Vector3(0,2,0), 0);

		//	questManager.AddQuest(new KillTargetQuest(targetZombie));
		//	questManager.AddQuest(new KillZombiesQuest(200));
		}
	}

	void Start () {
		if (Application.loadedLevel != 1) {
			return;
		}

		InitialiseHeroPlayer ();

		InitialiseSongPlayer ();
		songPlayer.Play ();
		//Debug.Log ("PLAYING");
		living.Add (player);

		if (PlayerPrefs.HasKey ("LightDetailSetting")) {
			int lightDetailLevel = PlayerPrefs.GetInt ("LightDetailSetting");
			SetLightDetail (lightDetailLevel);
		}

		if (PlayerPrefs.HasKey ("AutoLightAdjust")) {
			autoLightAdjust = PlayerPrefs.GetInt ("AutoLightAdjust") == 1;
		}

		//lingeringProcessedZombies = new List<ZombieBehaviour> ();
		zombiesToProcess = new Dictionary<int, KeyValuePair<ZombieBeing, int> > ();

	}

	void GenerateLevel()
	{
		if (Network.isClient) {
			return;
		}
		int initialPickups = 40;
		while (pickups.Count < initialPickups) {
			SpawnPickups (initialPickups - pickups.Count);
//			Debug.Log(initialPickups + " " + pickups.Count);
		}

		/*foreach (Transform child in initialPickupsHolder.transform) {
			pickups.Add(child.gameObject);
		}*/
		SpawnRareItems ();

		int randVal = 0;
		for (int i = 0; i < 24; i++) {
			randVal = UnityEngine.Random.Range (0, ChestBehaviour.chests.Count);
			ChestBehaviour.chests[randVal].DepositItem(null, BaseItem.Bandages);
		}

		for (int i = 0; i < 40; i++) {
			randVal = UnityEngine.Random.Range (0, ChestBehaviour.chests.Count);
			ChestBehaviour.chests[randVal].DepositItem(null, BaseItem.Ammo);
		}

		for (int i = 0; i < 20; i++) {
			randVal = UnityEngine.Random.Range (0, ChestBehaviour.chests.Count);
			ChestBehaviour.chests[randVal].DepositItem(null, BaseItem.Food);
		}

		for (int i = 0; i < 15; i++) {
			randVal = UnityEngine.Random.Range (0, ChestBehaviour.chests.Count);
			ChestBehaviour.chests[randVal].DepositItem(null, BaseItem.Adrenaline);
		}

		for (int i = 0; i < 10; i++) {
			randVal = UnityEngine.Random.Range (0, ChestBehaviour.chests.Count);
			ChestBehaviour.chests[randVal].DepositItem(null, BaseItem.ViralShot);
		}

		randVal = UnityEngine.Random.Range (0, ChestBehaviour.chests.Count);
		ChestBehaviour.chests[randVal].DepositItem(null, ItemGun.AK47);

		randVal = UnityEngine.Random.Range (0, ChestBehaviour.chests.Count);
		ChestBehaviour.chests[randVal].DepositItem(null, ItemGun.Glock);
	}

	void SpawnRareItems()
	{


		int c = 0;
		List<ChestBehaviour> chests = new List<ChestBehaviour> ();

		while (c < BaseItem.rareItems.Count) {
			int randVal = UnityEngine.Random.Range (0, ChestBehaviour.chests.Count);

			if (!chests.Contains(ChestBehaviour.chests[randVal]))
			{
				chests.Add(ChestBehaviour.chests[randVal]);
				c++;
			}

		}

		c = 0;
		foreach (var chest in chests) {
			chest.DepositItem(null, BaseItem.rareItems[c]);
			c++;
		}


	}
	

	public void DestroyPlayer()
	{
		living.Remove (player);
	
	}
	

	void InitialiseSongPlayer()
	{
		songPlayer = new SongPlayer (this.audio);
		foreach (AudioClip song in data.songs)
		{
			songPlayer.AddSong(song);
		}
	}

	void ToolTipHover()
	{

		//Collider2D col = Physics2D.OverlapCircle(Camera.main.ScreenToWorldPoint (Input.mousePosition),0.3f,layerMask);
		//Debug.Log (col.transform);

		Vector2 origin = player.transform.position;
		//Vector2 direction =  (Vector2)Camera.main.ScreenToWorldPoint (Input.mousePosition) - origin;
		Vector2 direction = player.transform.up;

		//RaycastHit2D hit = Physics2D.Raycast (origin, direction, 1.2f, ~(1 << 16 ) );

		RaycastHit2D hit = Physics2D.Raycast (origin, direction, 1.2f, ((SPAWN_BLOCK_LAYER | (1 << 12))  &(~(1 << 11)) ));


		if (hit.transform != null) {
//			Debug.Log (hit.transform.gameObject);
		
			GameObject hoverObject = hit.transform.gameObject;
			var tool = hoverObject.GetComponent<ToolTipComponent> ();
			interactHovered = hoverObject.GetComponent<InteractComponent>();

			if (tool != null && tool.ShowTooltip) {
				Tooltip = tool.ToolTip;
				TooltipPos = hit.point;
				TooltipPos = hit.collider.bounds.center;
			} 
			else {
				Tooltip = null;
				interactHovered = null;
			}
			
		} else {

			Tooltip = null;
			interactHovered = null;
		}
	}

	public bool IsOptionMenuOpen()
	{
		return optionedActivated != null;
	}

	public void CloseOpenMenus(bool isMouseClick)
	{
		if (!isMouseClick || (optionedActivated != null && !optionedActivated.IsMouseOver ())) {
			if (optionedActivated != null)
			{
				optionedActivated.NotifyClose();
				optionedActivated = null;
			}
			GlobalData.instance.player.PrimaryInputs = true;
			Screen.lockCursor = true;
		}
	}

	void LateUpdate()
	{
		if (Application.loadedLevel != 1) {
			return;
		}
		foreach (var p in lateProcesses) {
			p.Process();
		}
		lateProcesses.Clear ();

		//Debug.Log (AiBeing.release);
	}

	void AutoLightAndFPS ()
	{
		if (float.IsInfinity (fpsMem)) {
			fpsMem = 80;
		}

		float fpsCurrent = 1f / (Time.deltaTime);


		fpsMem = (1 - fpsMemRatio) * fpsMem + fpsMemRatio * fpsCurrent;
		if (!PAUSED && autoLightAdjust) {
			if (fpsMem > 59) {
				lightUpCount++;
				lightDownCount -= 3;
			}
			else {
				lightUpCount -= 8;
				lightDownCount++;
			}
			if (lightDownCount > 12) {
				lightDownCount = 0;
				if (lightDetail > -4)
				{
					SetLightDetail (lightDetail - 1);
				}
				if (fpsMem < 45) {
					lightMax--;
				}
			}
			else
				if (lightDownCount < 0) {
					lightDownCount = 0;
				}
			if (lightUpCount > 120 && lightDetail < lightMax) {
				lightUpCount = 0;
				SetLightDetail (lightDetail + 1);
			}
			else
				if (lightUpCount < 0) {
					lightUpCount = 0;
				}
		}
	}

	public void OpenMenu()
	{
		menuOpen = true;
		if (networkType == NetworkType.Single) {
			PAUSED = true;
			Time.timeScale = 0;
		}
	}

	void DrawMarkers()
	{
		if (HomeMarker.activeSelf)
		{
			Vector2 markerDrawPosition = HomeMarkerPosition;
			if (MoveMarker(ref markerDrawPosition))
			{
				HomeMarker.transform.position = markerDrawPosition;
			}
		}

		foreach (var ally in allies) {
			Vector2 pos = ally.transform.position;
			if (MoveMarker(ref pos))
			{
				ally.GUIMarker.SetActive(true);
				ally.GUIMarker.transform.position = new Vector3(pos.x, pos.y, -0.2f);
			}
			else {
				ally.GUIMarker.SetActive(false);
			}

		}
	}

	bool MoveMarker(ref Vector2 pos)
	{
		Vector2 screenPos = Camera.main.WorldToViewportPoint(pos);
		
		float newX = screenPos.x;
		float newY = screenPos.y;
		bool changed = false;

		if (newX < 0.02)
		{
			float ratio = 0.48f / Mathf.Abs(0.5f - newX);
			newY = ratio * (newY - 0.5f) +0.5f;
			newX = 0.02f;
			changed = true;
		}
		else if (newX > 0.98)
		{
			float ratio = 0.48f / Mathf.Abs(0.5f - newX);
			newY = ratio * (newY - 0.5f) +0.5f;
			newX = 0.98f;
			changed = true;
		}
		
		if (newY < 0.02)
		{
			float ratio = 0.48f / Mathf.Abs(0.5f - newY);
			newX = ratio * (newX - 0.5f) +0.5f;
			newY = 0.02f;
			changed = true;
		}
		else if (newY > 0.98)
		{
			float ratio = 0.48f / Mathf.Abs(0.5f - newY);
			newX = ratio * (newX - 0.5f) +0.5f;
			newY = 0.98f;
			changed = true;
		}
		
		pos = Camera.main.ViewportToWorldPoint(new Vector2(newX, newY));
		return changed;
	}
	
	// Update is called once per frame
	void Update () {
		if (Application.loadedLevel != 1) {
			return;
		}


		time.Enqueue (Time.deltaTime);
		if (time.Count == 101) {
			time.Dequeue();
		}

		CachedTime.Clear();
		float totalTime = 0;
		foreach (var f in time) {
			totalTime += f;
			CachedTime.Add(totalTime);
		}


		if (Input.GetKey (KeyCode.O)) {

			int layerMask = (1 << 17 | 1 << 9);
			Collider2D col = Physics2D.OverlapCircle(Camera.main.ScreenToWorldPoint (Input.mousePosition),0.3f,layerMask);
			
			//Debug.Log (pickups.Count);
			
			if (col != null && col.transform != null) {
				SurvivorBehaviour s = col.transform.gameObject.GetComponent<SurvivorBehaviour> ();
				if (s != selectedSurv) {
					if (selectedSurv != null)
					selectedSurv.debugEnable = false;
					selectedSurv = s;
					if (s != null)
					s.debugEnable = true;
				}


			}
			else {
				if (selectedSurv != null)
				selectedSurv.debugEnable = false;
				selectedSurv = null;

		
			}


		}


		AutoLightAndFPS ();



		if (cInput.GetButtonDown("Interact")) {
			if (interactHovered != null)
			{
				if (interactHovered.Interact(GlobalData.instance.player))
				{
					SetOptionActivated(interactHovered as IOptioned);
					if (optionedActivated != null)
					{
						GlobalData.instance.player.PrimaryInputs = false;
						Screen.lockCursor = false;
						//Screen.showCursor = true;
					}
				}
			}
			else {
				CloseOpenMenus(true);
			}
		}

		if (Input.GetKeyUp (KeyCode.Escape)) {
			if ( IsOptionMenuOpen())
			{
				CloseOpenMenus(false);
			}
			else 
			{
				menuOpen = !menuOpen;
				Screen.lockCursor = !menuOpen;
			
				PAUSED = !PAUSED;

				if (PAUSED)
				{
					Time.timeScale = 0;
				}
				else {
					Time.timeScale = 1;
				}
			}
		}


		if (PAUSED) {
			return;
		}


		DrawMarkers ();

		if ((Time.frameCount) % 100 == 0) {
			foreach (AreaBehaviour area in areas) {
				area.SpreadInfection ();
			}
		}


		float newIntensity = 0.456f * Mathf.Sin ((currentHours - 0.5f)/24f * 2 *Mathf.PI - Mathf.PI/2f) + 0.45f;

		if (newIntensity > 0.9f) {
			newIntensity = 0.9f;
		}

		if (newIntensity < 0) {
			newIntensity = 0;
		}

		if (Mathf.Abs (newIntensity - DayLight.Intensity) > 0.01f) {
			DayLight.Intensity = newIntensity;

			AreaBehaviour.infestationSpawnRate = 0.08f/(newIntensity + 1f) - 0.02f;

			Light2D.LightDetailSetting lsDetailDayLight = Light2D.LightDetailSetting.Rays_100;
			float dayLightSpread = newIntensity +0.2f> 1? 1 : newIntensity+0.2f;

			//DayLight.LightRadius = (dayLightSpread)* 30;
			//DayLight.CastDistance = DayLight.LightRadius*0.74f;

			if (DayLight.CastDistance > 18)
			{
				DayLight.CastDistance = 18;
			}


			GlobalData.instance.player.TorchRadius =  (1.4f - newIntensity)*10;

			GlobalData.instance.player.Torch.CastDistance = GlobalData.instance.player.TorchRadius*0.9f;
			if (GlobalData.instance.player.Torch.CastDistance > 18)
			{
				GlobalData.instance.player.Torch.CastDistance = 18;
			}

			foreach (AllyBehaviour ally in allies) {
				ally.TorchRadius = (1.4f - newIntensity)*10;
				ally.Torch.CastDistance = GlobalData.instance.player.TorchRadius*0.9f;
				if (ally.Torch.CastDistance > 18)
				{
					ally.Torch.CastDistance = 18;
				}
			}
		//	Debug.Log ("TIME: " + DailyHour + " " + DayLight.LightDetail + " " + newIntensity + " " + lsDetailDayLight);

		}


	
		if (!GameOver && player != null) {
			ToolTipHover ();

			if (optionedActivated != null) {
				optionedActivated.HandleInputs();
			}

			data.player.ManualUpdate();
			if (Time.frameCount % 5 == 0) {
				data.player.ProcessEffects();
			}
		}



		if (networkType != NetworkType.Client) {
			ServerUpdate ();
			foreach (var ally in allies)
			{
				ally.ClientUpdate();
			}
		} else {

			ClientUpdate();

		}

		/*var overflow = 1.0/goalFPS - Time.deltaTime;
		
		if (overflow > 0.0)
			millisecondsToWait++;
		else
			millisecondsToWait--;
		
		Thread.Sleep( (int)Mathf.Clamp(millisecondsToWait, 0.0f, 1.0f/goalFPS * 1000.0f) ); // clamp for sanity*/

		//Thread.Sleep (1f/60f - Time.deltaTime);
	
		//Thread.Sleep (16 - (int)(Time.deltaTime/1000f));

		Thread.Sleep ( (int)( Mathf.Clamp(16 - Time.deltaTime * 1000,0,16)));
	}

	public void SetCurrentTime( float value)
	{
		currentTime = value;
	}

	void CommonUpdate()
	{

	}

	void ClientUpdate()
	{
		//data.player.ManualUpdate ();

		
		/*spawnTime += Time.deltaTime;
		
		if (spawnTime > 60) {
			
			if (SpawnRandomPickup(GetRandomPos()) != null)
			{
				spawnTime = 0;
			}
			
		}*/

		for (int i = 0; i < survivors.Count; i++) {
	//		survivors[i].ClientUpdate();
		}
		
		foreach (var ally in allies)
		{
			ally.ClientUpdate();
		}

	
		
		for (int i = 0; i < zombies.Count; i++) {
	//TAG		zombies[i].ClientUpdate();
		}
		
		for (int i = 0; i < zombiesToRecycle.Count; i++) {
			zombiesToRecycle [i].OnRecycle ();
			//Debug.Log ("Recycle " + zombiesToRecycle [i].networkView.viewID); 
		}
		zombiesToRecycle.Clear ();
	}

	void ServerUpdate()
	{
		currentTime += Time.deltaTime*1.25f;
		
		if (Input.GetKeyDown (KeyCode.T)) {
			currentTime += 60*1f;
		}
		
		
		if (Input.GetKeyUp (KeyCode.F10)) {
			foreach (var area in areas)
			{
				area.infestationGrowth = 10;
			}
		}
		if (Input.GetKeyUp (KeyCode.F9)) {
			AudioSource AS = gameObject.AddComponent<AudioSource>();
			AS.PlayOneShot(data.sounds[(int)Sounds.ak4all]);
			data.player.AddAndEquip(ItemGun.AK47);
			data.player.melee = false;
			//AreaBehaviour.infestationGrowthOverall = 20;0);
			AreaBehaviour.infestationSpawnRate = 20;
			data.player.inventory.AddItem(ItemType.Ammo, 5000);

			
			foreach (var surv in survivors)
			{
				surv.inventory.AddItem(ItemType.Ammo, 5000);
			}
		}
		
		spawnTime += Time.deltaTime;
		
		if (spawnTime > 60) {			
			if (SpawnRandomPickup(GetRandomPos()) != null)
			{
				spawnTime = 0;
			}			
		}		
	
		if (!data.player.healthComponent.IsDead ()) {
			score = (killCount * 5 + HoursPlayed * 25);
		}
		
		AnimateAndTickleZombies (); //tickle zombies nearby players
		TickleRandomZombies (); //tickle random zombies
		ProcessSurvivorsAndTickleZombies (); //process survivors and tickle nearby zombies
		ProcessZombies (); //process all tickled zombies

		if (survivors.Count < 12) {
			TrySpawnSurvivor(GetRandomPos());
		}
	}

	void TickleRandomZombies()
	{
		if (zombies.Count > 0 && zombiesToProcess.Count < 50 && Time.frameCount % (zombiesToProcess.Count+1) == 0) {
			int index = UnityEngine.Random.Range(0, zombies.Count); 
			
			if (!zombiesToProcess.ContainsKey (zombies[index].ID)) {
				zombiesToProcess [zombies[index].ID] = new KeyValuePair<ZombieBeing, int> (zombies [index], 60 * 7); 
			}
		}
	}

	void ProcessSurvivorsAndTickleZombies()
	{
		foreach (SurvivorBeing survivor in survivors) {
			survivor.UpdateAnimation();
			if (survivor.ManualUpdate()) {				
				if (survivor.ID % 3 == Time.frameCount % 3) {
					survivor.ReflexUpdate();
				}

				if (survivor.ID % 5 == Time.frameCount % 5) {
					survivor.ProcessEffects();
				}

				for (int j = 0; j < survivor.localAffected.Count; j++) {
					ZombieBeing zomb = survivor.localAffected[j] as ZombieBeing;
					
					if (!zombiesToProcess.ContainsKey(zomb.ID)) {
						zombiesToProcess[zomb.ID] = new KeyValuePair<ZombieBeing, int>(zomb, 60*4); 
					}
				}
			}
		}
	}

	void AnimateAndTickleZombies()
	{
		for (int i = 0; i < GlobalData.instance.player.localAffected.Count; i++) {
			ZombieBeing zomb = GlobalData.instance.player.localAffected[i] as ZombieBeing;

			zomb.UpdateAnimation();
			if (!zombiesToProcess.ContainsKey(zomb.ID)) {
				zombiesToProcess[zomb.ID] = new KeyValuePair<ZombieBeing, int>(zomb, 60*20); 
			}
			
		}
	}

	void ProcessZombies()
	{
		Dictionary<int,KeyValuePair<ZombieBeing,int> > zombiesToProcessTemp = new Dictionary<int,KeyValuePair<ZombieBeing,int> > ();
		
		foreach (KeyValuePair<ZombieBeing,int> zombLinger in zombiesToProcess.Values) {
			ZombieBeing zomb = zombLinger.Key;
			
			if (!zomb.gameObject.activeSelf) {
				continue;
			}
			int lingerVal = zombLinger.Value -1;
			if (zomb.ManualUpdate()) {
				if (zomb.TickID == Time.frameCount % ZombieBeing.FRAME_TICK) {
					//TAG    zomb.CasualUpdate();
					zomb.ReflexUpdate();
				}

				if (zomb.ID % 5 == Time.frameCount % 5) {
					zomb.ProcessEffects();
				}
				
				if (lingerVal > 0) {
					zombiesToProcessTemp[zomb.ID] = new KeyValuePair<ZombieBeing, int>(zomb, lingerVal);
				}
			}
			
		}
		
		zombiesToProcess = zombiesToProcessTemp;

		for (int i = 0; i < zombiesToRecycle.Count; i++) {
			zombiesToRecycle [i].OnRecycle ();
		}
		zombiesToRecycle.Clear ();
	}
	

	int GetPoolZombies()
	{
		int total = 0;

		foreach (var area in areas) {
			total += area.ZombiePool;
		}

		return total;
	}
	

	Vector3 GetRandomPos(Bounds bounds)
	{
		float x = UnityEngine.Random.Range(bounds.min.x +0.5f, bounds.max.x -0.5f);
		float y = UnityEngine.Random.Range(bounds.min.y +0.5f, bounds.max.y -0.5f);
		Vector3 pos = new Vector3(x, y);


		return pos;
	}

	Vector3 GetRandomPos()
	{
		float x = UnityEngine.Random.Range(data.WorldLeft, data.WorldRight);
		float y = UnityEngine.Random.Range(data.WorldDown, data.WorldTop);
		Vector3 pos = new Vector3(x, y);

		while ((pos - data.player.transform.position).sqrMagnitude < 120) {
			
			x = UnityEngine.Random.Range(data.WorldLeft, data.WorldRight);
			y = UnityEngine.Random.Range(data.WorldDown, data.WorldTop);
			pos = new Vector3(x, y);
		
		}

		return pos;
	}


	

	void SpawnPickups(int count)
	{
		for (int i = 0; i < count*0.5f; i++) {
			TrySpawnPickup(GetRandomPos(), data.ammoBoxTemplate);
		}	

		for (int i = 0; i < count*0.2f; i++) {
			TrySpawnPickup(GetRandomPos(), data.healthBoxTemplate);
		}	

		for (int i = 0; i < count*0.3f; i++) {
			TrySpawnPickup(GetRandomPos(), data.foodCrate);
		}
	}

	public static void Save()
	{
		BinaryFormatter bf = new BinaryFormatter ();
		FileStream file = File.Create (Application.persistentDataPath + "/save.dat");
		SaveData data = new SaveData (instance.areas.Count, instance.zombies.Count, instance.pickups.Count, ItemHolder.itemHolders.Count, ChestBehaviour.chests.Count);
		HeroBeing hero = GlobalData.instance.player;
		//data.heroSave = hero.GetSaveObject ();
		data.killCount = instance.killCount;
		data.score = instance.score;
		data.currentTime = instance.currentTime;

		for (int i = 0; i < instance.areas.Count; i++) {
			data.areaSaves[i] = instance.areas[i].GetSaveObject();
		}

		for (int i = 0; i < instance.zombies.Count; i++) {
			//TAG	data.zombieSaves[i] = instance.zombies[i].GetSaveObject();
		}

		for (int i = 0; i < instance.pickups.Count; i++) {
			data.pickupSaves[i] = instance.pickups[i].GetComponent<PickupItem>().GetSaveObject();
		}

		for (int i = 0; i < ItemHolder.itemHolders.Count; i++) {
			data.itemHolderSaves[i] = ItemHolder.itemHolders[i].GetSaveObject();
		}

		for (int i = 0; i < ChestBehaviour.chests.Count; i++) {
			data.containerSaves[i] = ChestBehaviour.chests[i].inventory.GetSaveObject();
		}

		bf.Serialize (file, data);
		file.Close ();
	}

	public static bool HasSave()
	{
		return File.Exists (Application.persistentDataPath + "/save.dat");
	}

	void ClearAllInitialSpawns()
	{
		foreach (Transform child in initialPickupsHolder.transform) {
			Destroy(child.gameObject);
		}
	}

	public static void Load()
	{
		if (HasSave()) {

			BinaryFormatter bf = new BinaryFormatter();
			FileStream file = File.Open (Application.persistentDataPath + "/save.dat", FileMode.Open);
			SaveData data = (SaveData)bf.Deserialize(file);
			file.Close();

			//GlobalData.instance.player.LoadFromDataObject(data.heroSave);
			instance.currentTime = data.currentTime;
			instance.score = data.score;
			instance.killCount = data.killCount;

			for (int i = 0; i < data.areaSaves.Length; i++)
			{
				instance.areas[i].LoadFromSaveObject(data.areaSaves[i]);
			}

			for (int i = 0; i < data.zombieSaves.Length; i++)
			{
				ZombieBeing zomb = instance.SpawnZombie(data.zombieSaves[i].pos);
				//TAG	zomb.LoadFromDataObject(data.zombieSaves[i]);
			}

			for (int i = 0; i < data.pickupSaves.Length; i++)
			{
				GameObject template = instance.GetPickupTemplate(data.pickupSaves[i].type);
				PickupItem item = instance.SpawnPickup(data.pickupSaves[i].position, template);
				item.LoadFromSaveObject(data.pickupSaves[i]);
			}

			for (int i = 0; i < data.itemHolderSaves.Length; i++)
			{
				ItemHolder.itemHolders[i].LoadFromSaveObject(data.itemHolderSaves[i]);
			}

			for (int i = 0; i < data.containerSaves.Length; i++)
			{
				ChestBehaviour.chests[i].inventory.LoadFromSaveObject(data.containerSaves[i]);
			}
		}
	}

	void OnLevelWasLoaded(int level) {
		if (Application.loadedLevel != 1) {
			return;
		}
		GameOver = false;

		int pClass = 0;

		InitialiseHeroPlayer ();

		if (doLoad) {
			levelJustLoaded = false;

			foreach (Transform t in initialPickupsHolder.transform)
			{
				pickups.Remove(t.gameObject);
				Destroy (t.gameObject);
			}

			if (networkType == NetworkType.Single)
			{
				Load ();
			}
		} else {
			//if (networkType != NetworkType.Client)
			//{
				GenerateLevel();
			//}
		}

	}


	public SurvivorBeing TrySpawnSurvivor(Vector2 position)
	{

		Collider2D col = Physics2D.OverlapCircle ((Vector2)position, 0.3f,SPAWN_BLOCK_LAYER);
		
		if (col == null) {
			return SpawnRandomSurvivor(position);
		} 

		return null;
	}

	public SurvivorBeing SpawnRandomSurvivor(Vector2 position)
	{
		int survIndex = UnityEngine.Random.Range (0, data.survivorTypes.Count);
		return SpawnSurvivor (position, survIndex);
	}
	
	
	public SurvivorBeing SpawnSurvivor(Vector2 position, int survIndex)
	{
		GameObject survivor = Instantiate (data.survivorTypes[survIndex]) as GameObject;
		SurvivorBeing surv = survivor.GetComponent<SurvivorBeing> ();
		if (surv.isMale) {
			surv.name = data.GetRandomMaleName();
		} else {
			surv.name = data.GetRandomFemaleName();
		}
		//surv.ThinkID = NewThinkID;
		survivor.transform.position = position;
	
		if (Network.isServer) {
			NetworkScript.instance.NetworkSpawnSurvivor(surv, survIndex, surv.name);
		}
		return surv;
	}

	public ZombieBeing TrySpawnZombie(Vector2 position)
	{
		Collider2D col = Physics2D.OverlapCircle ((Vector2)position, 0.3f,SPAWN_BLOCK_LAYER);

		if (col == null) {
			return SpawnZombie(position);
		} 

		return null;
	}


	public ZombieBeing SpawnZombie(Vector2 position)
	{
		ZombieBeing zombie = GameObjectPool.instance.GetZombie(position);

//		Debug.Log("Spawn " + zombie.networkView.viewID + " " + zombie.networkView.viewID.owner);
		if (Network.isServer) {
			NetworkScript.instance.NetworkSpawnZombie(zombie);
		}

		return zombie;
	}


	PickupItem SpawnRandomPickup(Vector3 position)
	{
		float val = UnityEngine.Random.Range (0, 10);

		if (val < 4) {
			return TrySpawnPickup (position, data.ammoBoxTemplate);
		} else if (val < 8 ) {
			return TrySpawnPickup (position, data.healthBoxTemplate);
		} else {
			return TrySpawnPickup (position, data.foodCrate);
		}

		return null;
	}

	public GameObject GetPickupTemplate(PickupType type)
	{
		switch (type) {
		case PickupType.ammo: return data.ammoBoxTemplate;
		case PickupType.food: return data.foodCrate;
		case PickupType.health: return data.healthBoxTemplate;
		}

		return null;
	}

	public PickupItem TrySpawnPickup(Vector3 position, GameObject itemTemplate)
	{

		Collider2D col = Physics2D.OverlapCircle ((Vector2)position, 0.3f,SPAWN_BLOCK_LAYER);

		if (col == null) {
			return SpawnPickup(position, itemTemplate);
		}

		return null;
	}



	public PickupItem SpawnPickup(Vector3 position, GameObject itemTemplate)
	{
		GameObject item = Instantiate (itemTemplate) as GameObject;
		item.transform.position = position + new Vector3(0,0,0.001f);
		PickupItem pItem = item.GetComponent<PickupItem> ();
		if (Network.isServer) {
			NetworkScript.instance.NetworkSpawnPickup (pItem);
		}
		return pItem;
	}
	
	public PickupItem SpawnPickup(Vector3 position, PickupType type)
	{
		GameObject item = Instantiate (GetPickupTemplate(type)) as GameObject;
		item.transform.position = position;
		Debug.Log ("SPAWN ITEM");
		return (PickupItem)item.GetComponent<PickupItem>();
	}

}

