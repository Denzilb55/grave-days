﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class InventoryGUI : MonoBehaviour {

	public bool MouseOnGui;
	public Vector2 scrollPosition1 = Vector2.zero;

	void Awake()
	{
		enabled = false;
	}

	void OnGUI()
	{
		int xWidth = 400;
		int yWidth = 300;
		int xStart = (int)(Screen.width / 2f - xWidth/2f);
		int yStart = (int)(Screen.height / 2f - yWidth/2f);

		Rect guiRect = new Rect (xStart, yStart, xWidth, yWidth);
		
		//Debug.Log (guiRect.Contains (Event.current.mousePosition) + " " + Event.current.mousePosition);
		if (guiRect.Contains (Event.current.mousePosition)) {
			MouseOnGui = true;
		} else {
			MouseOnGui = false;
		}
		
		GUI.Box (guiRect, GUIContent.none); 
		
		GUIStyle titleStyle = new GUIStyle (GUI.skin.label);
		titleStyle.alignment = TextAnchor.UpperCenter;
		titleStyle.fontSize = 20;
		titleStyle.fontStyle = FontStyle.Bold;
		
		
		GUIStyle subTitleStyle = new GUIStyle (GUI.skin.label);
		subTitleStyle.alignment = TextAnchor.UpperCenter;
		subTitleStyle.fontSize = 16;
		subTitleStyle.fontStyle = FontStyle.Bold;
		
		GUIStyle hintStyle = new GUIStyle (GUI.skin.label);
		hintStyle.alignment = TextAnchor.UpperCenter;
		hintStyle.fontSize = 12;
		hintStyle.fontStyle = FontStyle.Italic;
		
		//TAKE
		
		
		
		//STORE
		int heroFood = GlobalData.instance.player.FoodCount;
		int heroMeds = GlobalData.instance.player.BandagesCount;
		int heroAmmo = GlobalData.instance.player.gunComponent.Ammo;
		
		
		GUILayout.BeginArea (guiRect);
		GUILayout.Label ("Chest", titleStyle);
		GUILayout.Label ("Hold shift to move in stacks", hintStyle);
		GUILayout.BeginHorizontal ();
		GUILayout.BeginVertical (GUILayout.Width (xWidth));
		GUILayout.Label ("DEPOSIT", subTitleStyle);
		scrollPosition1 = GUILayout.BeginScrollView(scrollPosition1,false,true);
		
		foreach(KeyValuePair<ItemType, int> entry in GlobalData.instance.player.inventory.items)
		{
			BaseItem item = entry.Key.GetItemInstance();
			
			if (GUILayout.Button (entry.Value+" x "+item.GetItemText())) 
			{
				if (Input.GetKey(KeyCode.LeftShift))
				{
				//	DepositItem(GlobalData.instance.player, item, item.GetStackSize());
				}
				else {
				//	DepositItem(GlobalData.instance.player, item);
				}
			}
		}

		GUILayout.EndScrollView();
		GUILayout.EndVertical ();
		GUILayout.EndHorizontal ();
		
		
		GUILayout.EndArea ();
	}
}
