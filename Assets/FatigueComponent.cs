﻿using UnityEngine;
using System.Collections;

public class FatigueComponent : MonoBehaviour {

	private float _fatigue;
	public float feverModifier;

	public float baseRecovery;
	
	public float MaxFatigue;
	public float Fatigue
	{
		get 
		{
			return _fatigue;
		}

		set 
		{
			_fatigue = value;

			if (_fatigue < 0)
			{
				_fatigue = 0;
			}
			else if (_fatigue > MaxFatigue)
			{
				_fatigue = MaxFatigue;
			}
		}
	}
	

	void Awake()
	{
		MaxFatigue = 100;
		Fatigue = MaxFatigue;
		feverModifier = 0;
		baseRecovery = 8;
	}

	public bool UpdateFatigue(MovementType moveType, float hunger)
	{

		float moveModifier = 0;
		if (moveType == MovementType.walking) {
			moveModifier = 0.35f;
		} else if (moveType == MovementType.running) {
			moveModifier = 1.4f;
		}

		float hungerModifier = hunger / 100f;
		float totalModifier = -feverModifier + hungerModifier - moveModifier - 1;

		Fatigue += Time.deltaTime * baseRecovery + Time.deltaTime * baseRecovery *  (totalModifier);
	
		if (Fatigue < 1) {
			return true;
		}
		return false;
	}

	/*public bool ChangeFatigue(MovementType moveType, float Hunger)
	{
		float fatigueChange = 0;
		if (moveType == MovementType.none) {
			fatigueChange = Time.deltaTime * Hunger *0.2f;
		}
		else if (moveType == MovementType.walking && Hunger > 0)
		{
			fatigueChange = Time.deltaTime * 0.028f * Hunger;
		} else if (moveType == MovementType.running) {
			fatigueChange = -Time.deltaTime * 5f;
		}

		Fatigue += fatigueChange +  Mathf.Abs(fatigueChange) * recoveryModifier;

		if (Fatigue < 1) {
			return true;
		}
		return false;
	}*/
}
