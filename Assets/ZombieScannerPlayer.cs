using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GraveDays.Entity.Being;
using GraveDays.Entity;

public class ZombieScannerPlayer : MonoBehaviour {

	private ILocalAffector hero;
	
	void Awake()
	{
		hero = GetComponentInParent <HeroBeing> ();
		if (hero == null) {
			hero = GetComponentInParent<AllyBehaviour>();
		}
	}


	
	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.CompareTag("Zombie")) {
			ZombieBeing zomb = other.gameObject.GetComponent<ZombieBeing>();
			//zomb.localLiving.Add(hero);
			zomb.AssignAffected(hero);
			hero.AssignAffected(zomb);
		}
		//Debug.Log ("ADDED: " + transform.tag);
	}
	
	void OnTriggerExit2D(Collider2D other)
	{
		if (other.CompareTag("Zombie")) {
			ZombieBeing zomb = other.gameObject.GetComponent<ZombieBeing>();
			zomb.UnassignAffected(hero);
			hero.UnassignAffected(zomb);
		}
	}
}