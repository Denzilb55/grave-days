﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GraveDays.Entity.Being;

public class ZombieScanner : MonoBehaviour {

	//public List<ZombieBehaviour> localZombies = new List<ZombieBehaviour> ();
	private SurvivorBeing surv;

	void Awake()
	{
		surv = GetComponentInParent <SurvivorBeing> ();

	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.CompareTag("Zombie")) {
			ZombieBeing zomb = other.gameObject.GetComponent<ZombieBeing>();
	//		localZombies.Add(zomb);
			//zomb.localLiving.Add(surv);
			zomb.AssignAffected(surv);
			surv.AssignAffected(zomb);
		}
		//Debug.Log ("ADDED: " + transform.tag);
	}
	
	void OnTriggerExit2D(Collider2D other)
	{
		if (other.CompareTag("Zombie")) {
			ZombieBeing zomb = other.gameObject.GetComponent<ZombieBeing>();
	//		localZombies.Remove(zomb);
			//zomb.localLiving.Remove(surv);
			zomb.UnassignAffected(surv);
			surv.UnassignAffected(zomb);
		}
	}
}
