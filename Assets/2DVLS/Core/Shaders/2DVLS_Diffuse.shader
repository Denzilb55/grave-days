Shader "2DVLS/Diffuse" {
    Properties
    {
        _MainTex ("Base (RGB) Trans. (Alpha)", 2D) = "white" { }
     	_tintColor ("Tint Color", Color) = (1, 1, 1, 1) // color
    }

    Category
    {

        Cull Back
        Lighting Off
		
		
        SubShader
        {
        	Tags { "Queue" = "Geometry+9" }
		
		    
		    Pass 
			{
				ColorMask 0
			}

            Pass
            {
           		Blend DstColor zero
           		//Blend zero DstColor
           		//Blend one DstAlpha
           		//Blend DstColor zero
				//Blend DstColor Zero
		
				//Blend OneMinusDstColor Zero
                SetTexture [_MainTex] 
				{ 
                      ConstantColor [_tintColor]
                      combine texture * constant
                }             
            }
        } 
    }

	Fallback "Diffuse"
}