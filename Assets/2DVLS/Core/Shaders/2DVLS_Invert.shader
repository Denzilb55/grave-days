Shader "2DVLS/Invert" {
    Properties 
	{
        _MainTex ("SelfIllum Color (RGB) Alpha (A)", 2D) = "white" {}
    }

    Category {
	
       Lighting Off
       ZWrite Off
       Fog { Mode Off }

       
       
     // Blend SrcColor One
    //  Blend SrcAlpha one
     // Blend one SrcColor
    // Blend SrcColor one
    //Blend zero one
      // Blend OneMinusDstColor One
       Blend  OneMinusDstAlpha zero 
       
       // Blend SrcColor zero 
       
	   BindChannels {
              Bind "Color", color
              Bind "Vertex", vertex
              Bind "TexCoord", texcoord
       }
       
	   SubShader 
	   {
            
            Pass 
			{
               Tags { "Queue" = "Geometry+3" }
              
               
               SetTexture [_MainTex] 
			   {
                    Combine texture * primary
               }
            }
            
      
        } 
    }
}

