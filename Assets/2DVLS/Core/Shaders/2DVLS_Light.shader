Shader "2DVLS/Light" {
    Properties 
	{
        _MainTex ("SelfIllum Color (RGB) Alpha (A)", 2D) = "white" {}
    }

    Category {
	
       Lighting Off
       ZWrite Off
       Fog { Mode Off }
       
	   BindChannels {
              Bind "Color", color
              Bind "Vertex", vertex
              Bind "TexCoord", texcoord
       }
       
	   SubShader 
	   {
            Tags { "Queue" = "Geometry+5" }
            Pass 
			{
               
               Blend DstColor zero
               SetTexture [_MainTex] 
			   {
                    Combine texture * primary
               }
            }
            
            
        } 
    }
}

