Shader "2DVLS/LightFlash" {
    Properties 
	{
      //  _MainTex ("SelfIllum Color (RGB) Alpha (A)", 2D) = "white" {}
        _MainTex ("Texture 0", 2D) = "white" {}
    }

    Category {
	
       Lighting Off
       ZWrite Off
       Fog { Mode Off }
      
       
	   BindChannels {
              Bind "Color", color
              Bind "Vertex", vertex
              Bind "TexCoord", texcoord
       }
       
	   SubShader 
	   {
            Tags { "Queue" = "Geometry+7" }
 			
 			                                          
            Pass 
			{

               //Blend DstAlpha zero
               Blend DstAlpha DstAlpha
               //Blend one one
               SetTexture [_MainTex] 
			   {
                    Combine texture * primary
               }
               
      
            }
            
  
           
        } 
    }
}

