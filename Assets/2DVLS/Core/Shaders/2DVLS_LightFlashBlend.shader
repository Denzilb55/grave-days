Shader "2DVLS/LightFlashBlend" {
    Properties 
	{
        _MainTex ("SelfIllum Color (RGB) Alpha (A)", 2D) = "white" {}
    }

    Category {
	
       Lighting Off
       ZWrite Off
       Fog { Mode Off }

       
       
    //  Blend OneMinusDstColor One
    //  Blend SrcAlpha one
     // Blend one SrcColor
    // Blend SrcColor one
    //Blend zero one
     //  Blend OneMinusSrcColor One
       
       //ColorMask 0
		//ZWrite On
       
	   BindChannels {
              Bind "Color", color
              Bind "Vertex", vertex
              Bind "TexCoord", texcoord
       }
       
	   SubShader 
	   {
            
            Pass 
			{
               
               Tags { "Queue" = "Geometry+2" }
              // Blend one one
               
               Blend SrcAlpha one
           
               
               SetTexture [_MainTex] 
			   {
                    Combine texture 
               }
            }
            
           
        } 
    }
}

