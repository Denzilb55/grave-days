Shader "2DVLS/Cutout" 
{
    Properties
    {
        _MainTex ("Base (RGB) Trans. (Alpha)", 2D) = "white" { }
		_Cutoff ("Alpha cutoff", Range (0,1)) = 1
		_tintColor ("Tint Color", Color) = (1, 1, 1, 1) // color
	}

    Category
    {
        ZWrite On
        Cull Back
        Lighting Off

        SubShader
        {
		    Tags { "Queue" = "Geometry+9" }
		    Pass {
				ColorMask 0

				AlphaTest GEqual [_Cutoff]
				SetTexture [_MainTex] { }            
			}

            Pass
            {
           	
           		Blend DstColor  Zero   
           		//Blend OneMinusDstColor Zero
          
           		AlphaTest GEqual [_Cutoff]
               // SetTexture [_MainTex] { }  
               SetTexture [_MainTex] 
				{ 
                      ConstantColor [_tintColor]
                      combine texture * constant
                }            
            }
        } 
    }
}