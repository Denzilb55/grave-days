Shader "2DVLS/CutoutBright" {
	Properties 
	{
		_MainTex ("SelfIllum Color (RGB) Alpha (A)", 2D) = "white" {}
		_Cutoff ("Alpha cutoff", Range (0,1)) = 1
		_tintColor ("Tint Color", Color) = (1, 1, 1, 1) // color
	}

	Category{
		Lighting Off
		ZWrite Off
		Fog { Mode Off }

		BindChannels {
		      Bind "Color", color
		      Bind "Vertex", vertex
		      Bind "TexCoord", texcoord
		}

		
		 SubShader
        {
		    Tags { "Queue" = "Geometry+12" }
		    Pass {
				ColorMask 0

				AlphaTest GEqual [_Cutoff]
				SetTexture [_MainTex] { }            
			}

            Pass
            {
           	
           		Blend DstAlpha  Zero   
           		//Blend OneMinusDstColor Zero
          
           		AlphaTest GEqual [_Cutoff]
               // SetTexture [_MainTex] { }  
               SetTexture [_MainTex] 
				{ 
                      ConstantColor [_tintColor]
                      combine texture * constant
                }            
            }
        } 
	}

}

